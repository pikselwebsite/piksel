<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class NewPostTranslation extends Model
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = ['title','locale', 'slug', 'content', 'excerpt','image','imageomedium', 'imagethumb','blog_id', 'user_id', 'category'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
