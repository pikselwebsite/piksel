<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Blog extends Model
{
    use Translatable;

    public $translatedAttributes = ['title', 'slug', 'content'];
    protected $table = 'blogs';
    protected $fillable = [
        'id',
        'name',
        'description',
        'user_id',
        'slug',
        'created_at',
        'updated_at'
    ];

}

