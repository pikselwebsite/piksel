<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class NewBlog extends Model
{
    use Translatable;

    public $translatedAttributes = ['name', 'slug', 'description'];
    protected $table = 'new_blogs';
    protected $fillable = [
        'id',
        'name',
        'description',
        'user_id',
        'created_at',
        'updated_at'
    ];

}

