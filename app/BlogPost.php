<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogPost extends Model
{
    use SoftDeletes;
    protected $table = 'blog_posts';
    protected $fillable = [

        'blog_id',
        'title',
        'slug',
        'category',
        'content',
        'excerpt',
        'tag',
        'image',
        'imageomedium',
        'imagethumb',
        'user_id',
        'language',
        'workflow_id',
        'created_at',
        'updated_at',
    ];
    protected $dates = ['deleted_at'];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function cat()
    {
        return $this->belongsTo('App\Category', 'category');
    }
}
