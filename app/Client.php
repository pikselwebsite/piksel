<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use Notifiable;
    protected $table = 'client';
    protected $fillable = [

        'title',
        'client',
        'logo',
        'what_we_did',
        'project',
        'mainimage',
        'mainimagemedium',
        'mainimagethumb',
        'custom_field',
        'icon_1',
        'icon_2',
        'icon_3',
        'icon_4',
        'custom_field_2',
        'desktopimg_1',
        'desktopimg_2',
        'description',
        'description_link',
        'description_img',
        'mobileimg_1',
        'mobileimg_2',
        'mobileimg_3',
        'created_at',
        'updated_at',
        'slug',
        'user_id'
    ];
}
