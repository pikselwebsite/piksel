<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class NewPost extends Model
{
    use Translatable;
    use SoftDeletes;

    protected $table = 'new_posts';


    public $translatedAttributes = ['title', 'slug','locale', 'content', 'excerpt','image','imageomedium', 'imagethumb', 'blog_id', 'user_id', 'category'];

    protected $fillable = [

        'blog_id',
        'title',
        'category',
        'content',
        'excerpt',
        'image',
        'locale',
        'imageomedium',
        'imagethumb',
        'user_id',
        'workflow_id',
        'created_at',
        'updated_at',
    ];
    protected $dates = ['deleted_at'];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function cat()
    {
        return $this->belongsTo('App\Category', 'category');
    }
}
