<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'team';
    protected $fillable = [
        'title',
        'main_image',
        'hover_image',
        'user_id',
        'created_at',
        'updated_at'
    ];
}
