<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'slider';
    protected $fillable = [

        'title',
        'client',
        'image',
        'description',
        'weblink',
        'user_id',
        'created_at',
        'updated_at',
        'language'
    ];

}
