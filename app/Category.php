<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends \Baum\Node
{

    protected $table = 'categories';
    protected $fillable = array('name', 'parent_id', 'slug', 'file', 'filename', 'image', 'description');
    public $timestamps = false;


    public static function getTree($categories)
    {

        $lists = '<ul>';
        foreach ($categories as $category)
            $lists .= self::renderNode($category);
        $lists .= "</ul>";
        return $lists;
    }


    public static function renderNode($node)
    {
        $list = '<li><a href="/admin/categories/' . $node->id . '/edit">' . $node->name;
        if ($node->children()->count() > 0) {
            $list .= '<ul>';
            foreach ($node->children as $child)
                $list .= self::renderNode($child);
            $list .= "</ul>";
        }

        $list .= "</a></li>";
        return $list;
    }
}
