<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
   protected $table = 'about_us';
   protected $fillable =[

        'title',
        'mission_title',
        'mission',
        'custom_field_1',
        'custom_field_2',
        'who_we_are_title',
        'who_we_are',
        'custom_field_3',
        'custom_field_4',
        'about_image',
        'about_image_medium',
        'about_image_thumb',
        'user_id'

   ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function createdby()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function workflow()
    {
        return $this->belongsTo('App\Workflow', 'user_id');
    }
}
