<?php
namespace App\Http\Controllers;

use App\AboutUs;
use App\Team;
use Illuminate\Http\Request;
use App\Settings;
use App\Slide;
use App\Blog;
use App\BlogPost;
use App\Category;
use App\Client;
use Validator;
use Input;
use Session;
use Mail;
    
    class LanguageLocalizationController extends Controller
    {
        public function index(Request $request){
            $settings = Settings::first();
            $images = Slide::all();
            $clients = Client::all();
             
            if($request->lang <> ''){
                app()->setLocale($request->lang);
            }
            echo __('message.desc');
            $data = ["settings" => $settings, "images" => $images, "clients" => $clients];
            return view('main.homepage')->with($data);
        }
    }
    ?>
    