<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Input;
use Session;
use Intervention\Image\ImageManagerStatic as Image;

class ClientController extends Controller


{

    public function __construct()
    {
        $this->middleware('auth');
    }


    private function store_image($theInput, Request $request)
    {


        if ($request->hasFile($theInput)) {

            $image = $request->file($theInput);
            $path = public_path() . '/assets/img/client-images/';
            $title = $request->input('title').'-'.$theInput;
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Client::where($theInput,'LIKE', '%'.$name.'%')->get();

            if(count($checkimage)> 0 ){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $image = $request->imagethumb = $imageName;

            return $image;



        }
    }

    private function update_client_image($theInput, Request $request, $id)
    {
        $client = Client::FindOrFail($id);

        if ($request->hasFile($theInput)) {

            $image = $request->file($theInput);
            $path = public_path() . '/assets/img/client-images/';
            $title = $request->input('title').'-'.$theInput;
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Client::where($theInput,'LIKE', '%'.$name.'%')->get();

            if(count($checkimage)> 0 ){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $image = $request->imagethumb = $imageName;

            return $image;



        } else {
            $image = $client->$theInput;
            return $image;

        }
    }


    //INDEX METHOD:GET
    public function index()
    {

        $clients = Client::all()->sortBy('created_at');
        $data = ['clients' => $clients];
        return view('admin.clients.index')->with($data);

    }

    //SHOW METHOD:GET

    public function show()
    {

        $clients = Client::all()->sortBy('created_at');
        $data = ['clients' => $clients];
        return view('admin.clients.index')->with($data);

    }

    //CREATE METHOD:GET

    public function create()
    {
        return view('admin.clients.create');
    }

    //SAVE METHOD:POST

    public function store(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'client' => 'required|max:255',
            'what_we_did' => 'required',
            'project' => 'required',
            'mainimage' => 'required',
            'description' => 'required',
            'description_link' => 'required',
            'description_img' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $input = $request->all();


        if ($request->hasFile('mainimage')) {

            $image = $request->file('mainimage');
            $path = public_path() . '/assets/img/client-images/';
            $pathThumb = public_path() . '/assets/img/client-images/thumbnails/';
            $pathMedium = public_path() . '/assets/img/client-images/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Client::where('mainimage','LIKE', '%'.$name.'%')->get();

            if(count($checkimage)> 0 ){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/client-images/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['mainimage'] = $image;
            $input['mainimagemedium'] = $imagemedium;
            $input['mainimagethumb'] = $imagethumb;

        }

        $input['logo'] =            $this->store_image('logo', $request);
        $input['icon_1'] =          $this->store_image('icon_1', $request);
        $input['icon_2'] =          $this->store_image('icon_2', $request);
        $input['icon_3'] =          $this->store_image('icon_3', $request);
        $input['icon_4'] =          $this->store_image('icon_4', $request);
        $input['desktopimg_1'] =    $this->store_image('desktopimg_1', $request);
        $input['desktopimg_2'] =    $this->store_image('desktopimg_2', $request);
        $input['description_img'] = $this->store_image('description_img', $request);
        $input['mobileimg_1'] =     $this->store_image('mobileimg_1', $request);
        $input['mobileimg_2'] =     $this->store_image('mobileimg_2', $request);
        $input['mobileimg_3'] =     $this->store_image('mobileimg_3', $request);


        $title = $request->input('title');
        $slugName = str_slug($title, '-');
        $checkSlug = Client::where('slug','LIKE', '%'.$slugName.'%')->get();
        $slug = str_slug($title, '-');


        if(count($checkSlug)> 0 ){
            $input['slug'] = $slug. '-' . count($checkSlug);
        } else{
            $input['slug'] = $slug;
        }


        Client::create($input);
        Session::flash('flash_message', 'Client successfully created!');

        return redirect()->action('ClientController@index');


    }

    // EDIT VIEW METHOD:GET

    public function edit($id)
    {
        $clientDetails = Client::FindOrFail($id);
        $data = ['client' => $clientDetails];

        return view('admin.clients.edit')->with($data);

    }


    // UPDATE BLOG METHOD:PUT

    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'client' => 'required|max:255',
            'what_we_did' => 'required',
            'project' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $client = Client::FindOrFail($id);
        $input = $request->all();


        if ($request->hasFile('mainimage') ) {

            $image = $request->file('mainimage');
            $path = public_path() . '/assets/img/client-images/';
            $pathThumb = public_path() . '/assets/img/client-images/thumbnails/';
            $pathMedium = public_path() . '/assets/img/client-images/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.' . $image->getClientOriginalExtension();

            $checkimage = Client::where('mainimage', 'LIKE', '%' . $name . '%')->get();

            if (count($checkimage) > 0) {
                $imageName = $name . '-' . count($checkimage) . $ext;
            } else {
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/client-images/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['mainimage'] = $image;
            $input['mainimagemedium'] = $imagemedium;
            $input['mainimagethumb'] = $imagethumb;

        } else {
            $input['mainimage'] = $client->mainimage;
            $input['mainimagemedium'] = $client->mainimagemedium;
            $input['mainimagethumb'] = $client->mainimagethumb;
        }

        $input['logo'] =         $this->update_client_image('logo', $request, $id);
        $input['icon_1'] = $this->update_client_image('icon_1', $request, $id);
        $input['icon_2'] = $this->update_client_image('icon_2', $request, $id);
        $input['icon_3'] = $this->update_client_image('icon_3', $request, $id);
        $input['icon_4'] = $this->update_client_image('icon_4', $request, $id);
        $input['desktopimg_1'] = $this->update_client_image('desktopimg_1', $request, $id);
        $input['desktopimg_2'] = $this->update_client_image('desktopimg_2', $request, $id);
        $input['description_img'] = $this->update_client_image('description_img', $request, $id);
        $input['mobileimg_1'] = $this->update_client_image('mobileimg_1', $request, $id);
        $input['mobileimg_2'] = $this->update_client_image('mobileimg_2', $request, $id);
        $input['mobileimg_3'] = $this->update_client_image('mobileimg_3', $request, $id);

        $title = $request->input('title');
        $slugName = str_slug($title, '-');
        $checkSlug = Client::where('slug', 'LIKE', '%' . $slugName . '%')->get();
        $slug = str_slug($title, '-');


        if (count($checkSlug) > 0) {
            $input['slug'] = $slug . '-' . count($checkSlug);
        } else {
            $input['slug'] = $slug;
        }

        $client->fill($input)->save();
        Session::flash('flash_message', 'Client successfully Edited!');

        return redirect()->action('ClientController@index');
    }

    //DELETE BLOG METHOD:DELETE

    public function destroy($id)
    {
        $client = Client::FindOrFail($id);
        $client->delete();

        Session::flash('flash_message', 'Client successfully deleted!');
        return redirect()->action('ClientController@index');
    }


}
