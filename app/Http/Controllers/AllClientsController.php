<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\Team;
use Illuminate\Http\Request;
use App\Settings;
use App\Slide;
use App\Blog;
use App\BlogPost;
use App\Category;
use App\Client;
use Validator;
use Input;
use Session;
use Mail;

class AllClientsController extends Controller
{
     public function bimilk()
    {
        $settings = Settings::first();
        $data = [  "settings" => $settings];
        return view('clients.bimilk')->with($data);
    }
      public function linea()
    {
        $settings = Settings::first();
        $data = [  "settings" => $settings];
        return view('clients.linea')->with($data);
    }
      public function balans()
    {
        $settings = Settings::first();
        $data = [  "settings" => $settings];
        return view('clients.balans')->with($data);
    }
      public function zito()
    {
        $settings = Settings::first();
        $data = [  "settings" => $settings];
        return view('clients.zito')->with($data);
    }
       public function cineplexx()
    {
        $settings = Settings::first();
        $data = [  "settings" => $settings];
        return view('clients.cineplexx')->with($data);
    }
       public function forza()
    {
        $settings = Settings::first();
        $data = [  "settings" => $settings];
        return view('clients.forza')->with($data);
    }



}