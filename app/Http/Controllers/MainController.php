<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\Team;
use Illuminate\Http\Request;
use App\Settings;
use App\Slide;
use App\NewBlog;
use App\NewPost;
use App\User;
use App\NewPostTranslation;
use App\NewBlogTranslation;
use App\Category;
use App\Client;
use Validator;
use Illuminate\Support\Facades\Input;
use Session;
use Mail;
use App;

class MainController extends Controller
{
    public function index()
    {
//        dd(App::getLocale());
        $settings = Settings::first();
        $clients = Client::all();
        $images = Slide::where('language', '=', App::getLocale())->get();
        $data = ["settings" => $settings, "images" => $images, "clients" => $clients];
        return view('main.homepage')->with($data);
    }
    public function blogs()
    {
        $settings = Settings::first();
        $blogs = NewBlog::all()->sortByDesc('id');
        $posts = NewPostTranslation::where('locale', '=', App::getLocale())->get()->sortByDesc('id');
        $blogCategories = Category::all();
        $data = ["settings" => $settings, "blogs" => $blogs, "posts" => $posts, 'blogCategories' => $blogCategories];
        return view('main.blog')->with($data);
    }

    public function post($slug)
    {
        // $post = NewPostTranslation::whereTranslate('slug', $slug)->firstOrFail();
       
        $post = NewPostTranslation::where('slug','=',$slug)->first();
        //  if ($post->translate()->where('slug', $slug)->first()->locale != app()->getLocale()) {
        //     return redirect()->route('post.show', $post->translate()->slug);
        // }
        $blog = NewBlogTranslation::where('id', '=', $post->blog_id)->first();
        $user = User::where('id','=',$post->user_id)->first();
        $randomPosts = NewPostTranslation::inRandomOrder()->where('id', '!=', $post->new_post_id)->take(3)->get();
        $blogCategories = Category::where('id','=',$post->category)->first();
        $date =NewPost::where('id','=',$post->new_post_id)->first();
        $settings = Settings::first();
        $previous = NewPostTranslation::where('id', '<', $post->new_post_id)->first();
        $next = NewPostTranslation::where('id', '>', $post->new_post_id)->first();


        $data = ["settings" => $settings,  "blog" => $blog,"date" => $date,"user"=>$user, "post" => $post, 'next'=>$next, 'previous' => $previous, 'randomPosts' => $randomPosts, 'blogCategories' => $blogCategories];
        return view('main.posts')->with($data);
    }
    public function portfolio()
    {
        $settings = Settings::first();
        $clients = Client::all()->sortByDesc('id');
        $data = ["clients" => $clients, "settings" => $settings];
        return view('main.portfolio')->with($data);
    }

    public function about()
    {
        $settings = Settings::first();
        $about = AboutUs::first();
        $team = Team::all();
        $words = explode(" ",$about->mission_title);

        $mission = "";
        $vision = "";

        foreach($words as $index => $word)
        {
            if($index === 0) $mission = $word;
            else   $vision .= " ".$word;
        }

        $whoWords = explode(" ", $about->who_we_are_title);
        $who = "";
        $others = "";
        foreach($whoWords as $index => $word)
        {
            if($index === 0) $who = $word;
            else $others .= " ".$word;
        }

        $data = ["about" => $about, "settings" => $settings, "mission" => $mission, "vision" => $vision, "who" => $who, "others" => $others, "team" => $team];
        return view('main.about')->with($data);
    }

    public function client($slug)
    {
        $client = Client::where("slug", "=", $slug)->first();
        $clients = Client::all()->sortByDesc('id');
        $settings = Settings::first();
        $data = ["client" => $client, "clients" => $clients, "settings" => $settings];
        return view('main.client')->with($data);
    }

    public function contact()
    {
        $settings = Settings::first();
        $images = Slide::all();
        $clients = Client::all();
        $data = ["settings" => $settings, "images" => $images, "clients" => $clients];
        return view('main.contact')->with($data);
    }
    public function career_page()
    {
        $settings = Settings::first();
        $clients = Client::all();
        $data = ["settings" => $settings,  "clients" => $clients];
        return view('main.career')->with($data);
    }

    public function postcontact(Request $request){
       
        $errors = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'bodyMessage' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $data = array(
            'name' => $request->name,
            'company' => $request->company,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'web' => $request->web,
            'bodyMessage' => $request->bodyMessage
        );

        Mail::send('emails.contact', $data, function($message) use($data){
            $message->from($data['email']);
            $message->to('contact@piksel.mk');
            $message->replyTo($data['email']);
            $message->subject('Нова порака од piksel.mk');
        });

        Session::flash('flash_message', 'Thank You. The message has been sent');

        return redirect()->back();
    }

    public function career(Request $request){
        $errors = Validator::make($request->all(), [
            'c_fullname' => 'required',
            'c_email' => 'required|email',
            'c_phone' => 'required',
            'cv' => 'required|mimes:pdf,ppt,docx,doc,xls',
            'c_message' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $data = array(
            'c_fullname' => $request->c_fullname,
            'c_email' => $request->c_email,
            'c_phone' => $request->c_phone,
            'position'=>$request->position,
            'c_message' => $request->c_message,
            'cv' => $request->cv
        );
        Mail::send('emails.careers', $data, function($message) use($data){
            $message->from($data['c_email']);
            $message->to('career@piksel.mk');
            $message->replyTo($data['c_email']);
            $message->attach($data['cv']->getRealPath(), array(
                    'as' => $data['c_fullname'] . '-cv.' . $data['cv']->getClientOriginalExtension(),
                    'mime' => $data['cv']->getMimeType())
            );
            $message->subject($data['position'].'-Кариера - piksel.mk');
        });

        Session::flash('flash_message', 'Thank You. The message has been sent');

        return redirect()->back();
    }

    public function homecontact(Request $request){
        $errors = Validator::make($request->all(), [
            'user' => 'required',
            'email' => 'required|email',
            'bodymessage' => 'required',
            'g-recaptcha-response' => 'required|captcha',

        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $data = array(
            'user' => $request->user,
            'email' => $request->email,
            'bodymessage' => $request->bodymessage,
        );

        Mail::send('emails.homemail', $data, function($message) use($data){
            $message->from($data['email']);
            $message->to('contact@piksel.mk');
            $message->replyTo($data['email']);
            $message->subject('Нова порака од piksel.mk');

        });

        Session::flash('flash_message', 'Thank You. The message has been sent');

        return redirect()->back();
    }

    public function error(){
        $settings = Settings::first();
        $clients = Client::all();
        $data = ["settings" => $settings, "clients" => $clients];
        return view('main.404')->with($data);
    }

}
