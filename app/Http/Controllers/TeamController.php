<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Http\Requests;
use App\Workflow;
use App\User as User;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Input;
use Session;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $members = Team::all();
        $data = [ "members" => $members];

            return view('admin.team.index')->with($data);

    }


    public function store(Request $request) {

        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'main_image' => 'required',
            'hover_image' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $input = $request->all();

        //The image

        if ($request->hasFile('main_image')) {

            $image = $request->file('main_image');
            $path = public_path() . '/assets/img/team-images';
            $pathMedium = public_path() . '/assets/img/team-images/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Team::where('main_image','LIKE', '%'.$name.'%')->get();


            if(count($checkimage) > 0){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/team-images/' . $imageName;


            $imagemedium = Image::make($findimage)->resize(1180, null, function ($constraint) {
                $constraint->aspectRatio();
            });


            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;


            $input['main_image'] = $image;


        }

        //the hover

        if ($request->hasFile('hover_image')) {

            $image = $request->file('hover_image');
            $path = public_path() . '/assets/img/team-images';
            $pathMedium = public_path() . '/assets/img/team-images/medium/';
            $title = $request->input('title').'-hover';
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Team::where('hover_image','LIKE', '%'.$name.'%')->get();


            if(count($checkimage) > 0){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/team-images/' . $imageName;


            $imagemedium = Image::make($findimage)->resize(1180, null, function ($constraint) {
                $constraint->aspectRatio();
            });


            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;


            $input['hover_image'] = $image;


        }


        Team::create($input);

        Session::flash('flash_message', 'Team member successfully created!');

        return redirect()->back();

    }



    public function edit($id)
    {
        $members = Team::all();
        $member = Team::FindOrFail($id);
        $data = [ "members" => $members, 'member' => $member];

        return view('admin.team.edit')->with($data);

    }



    public function update(Request $request, $id) {

        $errors = Validator::make($request->all(), [

            'title' => 'required|max:255',

        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $member = Team::FindOrFail($id);
        $input = $request->all();


        //The image

        if ($request->hasFile('main_image')) {

            $image = $request->file('main_image');
            $path = public_path() . '/assets/img/team-images';
            $pathMedium = public_path() . '/assets/img/team-images/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Team::where('main_image','LIKE', '%'.$name.'%')->get();


            if(count($checkimage) > 0){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/team-images/' . $imageName;


            $imagemedium = Image::make($findimage)->resize(1180, null, function ($constraint) {
                $constraint->aspectRatio();
            });


            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;


            $input['main_image'] = $image;


        }

        //the hover

        if ($request->hasFile('hover_image')) {

            $image = $request->file('hover_image');
            $path = public_path() . '/assets/img/team-images';
            $pathMedium = public_path() . '/assets/img/team-images/medium/';
            $title = $request->input('title').'-hover';
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Team::where('hover_image','LIKE', '%'.$name.'%')->get();


            if(count($checkimage) > 0){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/team-images/' . $imageName;


            $imagemedium = Image::make($findimage)->resize(1180, null, function ($constraint) {
                $constraint->aspectRatio();
            });


            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;


            $input['hover_image'] = $image;


        }

        else
        {
            $input['hover_image'] = $member->hover_image;
            $input['main_image'] = $member->main_image;

        }


        $member->fill($input)->save();
        Session::flash('flash_message', 'Team member successfully updated!');

        return redirect()->route('team.index');

    }




    //DELETE MEMBER

    public function destroy($id)
    {
        $member = Team::FindOrFail($id);
        $member->delete();

        Session::flash('flash_message', 'Team member successfully deleted!');

        return redirect()->route('team.index');


    }







}
