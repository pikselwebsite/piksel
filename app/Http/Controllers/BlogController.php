<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogPost;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Input;
use Session;
use Intervention\Image\ImageManagerStatic as Image;
use App\Client;



class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //INDEX METHOD:GET
    public function index()
    {
        dd($blogs);
        $blogs = Blog::all()->sortBy('created_at');
        $posts = BlogPost::all();
        $blogCategories = Category::all();
        $trashed = BlogPost::onlyTrashed()->get();
        $data = ['blogs' => $blogs, 'posts' => $posts, 'trashed' => $trashed, 'blogCategories' => $blogCategories];
        return view('admin.blog.index')->with($data);

    }

    //SHOW METHOD:GET

    public function show()
    {

        $blogs = Blog::all()->sortBy('created_at');
        $posts = BlogPost::all();
        $data = ['blogs' => $blogs, 'posts', $posts];
        return view('admin.blog.index')->with($data);

    }

    //CREATE METHOD:GET

    public function create()
    {
        return view('admin.blog.create');
    }


    //SAVE METHOD:POST

    public function store(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $request['slug'] = str_slug($request['name']);
        $slug = Blog::where('name', $request['name'])->get();
        if (count($slug) > 0) {
            $request['slug'] = $request['slug'] . '-' . count($slug);
        } else {
            $request['slug'] = $request['slug'];
        }

        $input = $request->all();

        Blog::create($input);

        Session::flash('flash_message', 'Blog successfully created!');

        return redirect()->route('blog.index');
    }

    // EDIT VIEW METHOD:GET

    public function edit($id)
    {
        $blogDetails = Blog::FindOrFail($id);
        $data = ['blog' => $blogDetails];

        return view('admin.blog.edit')->with($data);
    }

    // UPDATE BLOG METHOD:PUT

    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $blog = Blog::FindOrFail($id);
        $request['slug'] = str_slug($request['name']);
        $slug = Blog::where('name', $request['name'])->get();

        $input = $request->all();

        (int)$count = count($slug);

        if ($count > 0) $request['slug'] = $request['slug'] . '-' . $count;

        $blog->fill($input)->save();

        Session::flash('flash_message', 'Blog successfully edited!');

        return redirect('/admin/blog');


    }

    //DELETE BLOG METHOD:DELETE

    public function destroy($id)
    {
        $blog = Blog::FindOrFail($id);
        $blog->delete();

        Session::flash('flash_message', 'Blog successfully deleted!');
        return redirect('/admin/blog');
    }

    /*
    *************************************************************
    *************************************************************
    **************    BLOG POSTS CONTROLLERS       **************
    *************************************************************
    *************************************************************

    */

    public function postsindex($id)
    {
        $blog = Blog::FindOrFail($id);
        $posts = BlogPost::all()->sortBy('created_at')->where('blog_id', $id);
        $trashed = BlogPost::onlyTrashed()->get();
        $blogCategory = Category::Find($id);
        $data = ['posts' => $posts, 'blog' => $blog , 'trashed' => $trashed, 'blogCategory' => $blogCategory];

        return view('admin.blog.posts')->with($data);


    }

    public function trash($id)
    {
        $blog = Blog::FindOrFail($id);
        $trashed = BlogPost::onlyTrashed()->get();
        $posts = BlogPost::all()->sortBy('created_at')->where('blog_id', $id);

        $data = ['posts' => $posts, 'blog' => $blog , 'trashed' => $trashed];

        return view('admin.blog.post.trashed')->with($data);


    }

    //Create post

    public function createpost($id)
    {
        $blog = Blog::FindOrFail($id);
        $categories = Category::all();
        $data = ['blog' => $blog, 'categories' => $categories];

        return view('admin.blog.post.create')->with($data);
    }

    public function storepost(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'content' => 'required',
            'category' => 'required',
            'image' => 'required',
            'excerpt' => 'required',
            'language' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $input = $request->all();


        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/posts-images/';
            $pathThumb = public_path() . '/assets/img/posts-images/thumbnails/';
            $pathMedium = public_path() . '/assets/img/posts-images/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = BlogPost::where('image','LIKE', '%'.$name.'%')->get();

            if(count($checkimage)> 0 ){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/posts-images/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['image'] = $image;
            $input['imageomedium'] = $imagemedium;
            $input['imagethumb'] = $imagethumb;

        }

        $checkSlug = BlogPost::where('slug','LIKE', '%'.$name.'%')->get();
        $slug = str_slug($title, '-');

        if(count($checkSlug)> 0 ){
            $input['slug'] = $slug. '-' . count($checkSlug);
        } else{
            $input['slug'] = $slug;
        }


        BlogPost::create($input);

        Session::flash('flash_message', 'Post successfully created!');

        return redirect()->action('BlogController@index');
    }


    public function editpost($id, $post_id)
    {
            $blog = Blog::FindOrFail($id);
            $categories = Category::all();
            $post = BlogPost::FindOrFail($post_id);

            $data = ['blog' => $blog, 'categories' => $categories, 'post' => $post];

        return view('admin.blog.post.edit')->with($data);

    }

    //UPDATING POST

    public function updatepost(Request $request, $post_id)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'content' => 'required',
            'category' => 'required',
            'excerpt' => 'required'
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $post = BlogPost::FindOrFail($post_id);
        $input = $request->all();

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/posts-images/';
            $pathThumb = public_path() . '/assets/img/posts-images/thumbnails/';
            $pathMedium = public_path() . '/assets/img/posts-images/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = BlogPost::where('image','LIKE', '%'.$name.'%')->get();

            if(count($checkimage)> 0 ){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/posts-images/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['image'] = $image;
            $input['imageomedium'] = $imagemedium;
            $input['imagethumb'] = $imagethumb;

        }

        $title = $request->input('title');
        $name = str_slug($title, '-');
        $checkSlug = BlogPost::where('slug','LIKE', '%'.$name.'%')->get();
        $slug = str_slug($title, '-');

        if(count($checkSlug)> 0 ){
            $input['slug'] = $slug. '-' . count($checkSlug);
        } else{
            $input['slug'] = $slug;
        }

        $post->fill($input)->save();

        Session::flash('flash_message', 'Post successfully edited!');

        return redirect()->back();


    }

    // SOFT DELETE BLOG METHOD:DELETE

    public function destroypost($id)
    {
        $blog = BlogPost::FindOrFail($id);
        $blog->delete();

        Session::flash('flash_message', 'Blog Post successfully deleted!');
        return redirect('/admin/blog');
    }

    //PERMANENT RESTORE ITEM

    public function restoreitem($id)
    {

        $post = BlogPost::onlyTrashed()->FindOrFail($id);

        $post->restore();

        Session::flash('flash_message', 'Post restored!');
        return redirect('/admin/blog');

    }

    //EMPTY TRASH

    public function emptytrash()
    {

        $trashed = BlogPost::onlyTrashed();

        $trashed->forceDelete();

        Session::flash('flash_message', 'Trash successfully deleted!');
        return redirect('/admin/blog');

    }

    //PERMANENT DELETE ITEM

    public function permanentdeleteitem($id)
    {

        $post = BlogPost::onlyTrashed()->FindOrFail($id);

        $post->forceDelete();

        Session::flash('flash_message', 'Post permanently deleted!');
        return redirect('/admin/blog');

    }

}
