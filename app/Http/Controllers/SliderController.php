<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Slide as Slide;
use App\Http\Requests;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Input;
use Session;

class SliderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        $slides = Slide::all()->sortBy('created_at');
        $data = ['slides' => $slides];
        return view('admin.slider.index')->with($data);
    }

    public function create(Request $request)
    {
        //     width: 1180px;         height: 775px;

        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'client' => 'required',
            'description' => 'required',
            'weblink' => 'required',
            'image' => 'required',
            'language' => 'required',
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $input = $request->all();

        //The image

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $path = public_path() . '/assets/img/pikselslider';
            $pathMedium = public_path() . '/assets/img/pikselslider/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Slide::where('image','LIKE', '%'.$name.'%')->get();


            if(count($checkimage) > 0){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/pikselslider/' . $imageName;


            $imagemedium = Image::make($findimage)->resize(1180, null, function ($constraint) {
                $constraint->aspectRatio();
            });


            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;


            $input['image'] = $image;


        }


        Slide::create($input);

        Session::flash('flash_message', 'Slide successfully created!');

        return redirect()->back();
    }

    public function edit($id)
    {


        $slide = Slide::FindOrFail($id);
        $data = ['slide' => $slide];


        return view('admin.slider.edit')->with($data);
    }

    public function update(Request $request,$id)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'client' => 'required',
            'description' => 'required',
            'weblink' => 'required',

        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $input = $request->all();
        $settings = Slide::Find($id);

        $settings->fill($input)->save();

        if ($request->hasFile('image')) {


            $image = $request->file('image');
            $path = public_path() . '/assets/img/pikselslider';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $checkimage = Slide::where('image','LIKE', '%'.$name.'%')->get();


            if(count($checkimage)> 0 ){
                $imageName = $name. '-' . count($checkimage) . $ext;
            } else{
                $imageName = $name . $ext;
            }


            $image->move($path, $imageName);
            $image = $request->imagethumb = $imageName;

            $input['image'] = $image;


        }

        $settings->fill($input)->save();


        Session::flash('flash_message', 'Slider successfully edited!');

        return redirect()->back();
    }

    //DELETE SLIDE

    public function delete($id)
    {
         Slide::destroy($id);

        Session::flash('flash_message', 'Slider successfully deleted!');

        return redirect()->route('admin.slider');


    }


}



