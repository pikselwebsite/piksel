<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Workflow;
use App\AboutUs as AboutUs;
use App\User as User;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Input;
use Session;

class AboutUsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $settings = AboutUs::first();
        $data = [ "settings" => $settings];
            if(!empty($settings))
            {
            return view('admin.aboutus.index')->with($data);
            }
            else
            {
                return view('admin.aboutus.create')->with($data);
            }

    }

    public function store(Request $request) {

        $errors = Validator::make($request->all(), [

            'title'             => 'required|max:255',
            'mission_title'     => 'required|max:255',
            'mission'           => 'required',
            'custom_field_1'    => 'required|max:255',
            'custom_field_2'    => 'required|max:255',
            'who_we_are_title'  => 'required|max:255',
            'who_we_are'        => 'required',
            'custom_field_3'    => 'required|max:255',
            'custom_field_4'    => 'required|max:255',
            'about_image'       => 'required',
            'user_id'           => 'required'
            ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }


        $input = $request->all();


        if ($request->hasFile('about_image')) {

            $image = $request->file('about_image');
            $path = public_path() . '/assets/img/posts-images/';
            $pathThumb = public_path() . '/assets/img/posts-images/thumbnails/';
            $pathMedium = public_path() . '/assets/img/posts-images/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

                $imageName = $name . $ext;

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/posts-images/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['about_image'] = $image;
            $input['about_image_medium'] = $imagemedium;
            $input['about_image_thumb'] = $imagethumb;

        }

        AboutUs::create($input);

        Session::flash('flash_message', 'About us page successfully updated!');

        return redirect()->action('AboutUsController@index');

    }

    public function update(Request $request) {

        $errors = Validator::make($request->all(), [

            'title'             => 'required|max:255',
            'mission_title'     => 'required|max:255',
            'mission'           => 'required',
            'custom_field_1'    => 'required|max:255',
            'custom_field_2'    => 'required|max:255',
            'who_we_are_title'  => 'required|max:255',
            'who_we_are'        => 'required',
            'custom_field_3'    => 'required|max:255',
            'custom_field_4'    => 'required|max:255',
            'user_id'           => 'required'
        ]);

        if ($errors->fails()) {
            return redirect()->back()
                ->withErrors($errors)
                ->withInput();
        }

        $about = AboutUs::first();
        $input = $request->all();


        if ($request->hasFile('about_image')) {

            $image = $request->file('about_image');
            $path = public_path() . '/assets/img/posts-images/';
            $pathThumb = public_path() . '/assets/img/posts-images/thumbnails/';
            $pathMedium = public_path() . '/assets/img/posts-images/medium/';
            $title = $request->input('title');
            $name = str_slug($title, '-');
            $ext = '.'.$image->getClientOriginalExtension();

            $imageName = $name . $ext;

            $image->move($path, $imageName);

            $findimage = public_path() . '/assets/img/posts-images/' . $imageName;
            $imagethumb = Image::make($findimage)->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagemedium = Image::make($findimage)->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imagethumb->save($pathThumb . $imageName);
            $imagemedium->save($pathMedium . $imageName);

            $image = $request->imagethumb = $imageName;
            $imagethumb = $request->image = $imageName;
            $imagemedium = $request->image = $imageName;

            $input['about_image'] = $image;
            $input['about_image_medium'] = $imagemedium;
            $input['about_image_thumb'] = $imagethumb;

        }

        else
        {
            $input['about_image'] = $about->about_image;
            $input['about_image_medium'] = $about->about_image_medium;
            $input['about_image_thumb'] = $about->about_image_thumb;

        }


        $about->fill($input)->save();
        Session::flash('flash_message', 'About us page successfully updated!');

        return redirect()->action('AboutUsController@index');

    }

}
