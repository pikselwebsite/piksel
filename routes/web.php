<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Slide;

Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function()
{

/* Front end */
Route::get('/', 'MainController@index');
Route::get(LaravelLocalization::transRoute('routes.blog'), ['as' => 'blog', 'uses' => 'MainController@blogs']);
Route::get(LaravelLocalization::transRoute('routes.career'), ['as' => 'career', 'uses' => 'MainController@career_page']);
Route::get(LaravelLocalization::transRoute('routes.portfolio'), ['as' => 'portfolio', 'uses' => 'MainController@portfolio']);
Route::get(LaravelLocalization::transRoute('routes.about'), ['as' => 'about', 'uses' => 'MainController@about']);
Route::get(LaravelLocalization::transRoute('routes.contact'), ['as' => 'contact', 'uses' => 'MainController@contact']);
Route::get(LaravelLocalization::transRoute('routes.post'), ['as' => 'post', 'uses' => 'MainController@post']);
Route::get(LaravelLocalization::transRoute('routes.notFound'), ['as' => 'notFound', 'uses' => 'MainController@error']);
//Route::get('/blogpost/{slug}', 'MainController@post');
Route::get('/client/{slug}', 'MainController@client');
Route::post('/contact', 'MainController@postcontact');
Route::post('/career', 'MainController@career');
Route::post('/homecontact', 'MainController@homecontact');

// clients
Route::get(LaravelLocalization::transRoute('routes.bimilk'), ['as' => 'bimilk', 'uses' => 'AllClientsController@bimilk']);
Route::get(LaravelLocalization::transRoute('routes.linea'), ['as' => 'linea', 'uses' => 'AllClientsController@linea']);
Route::get(LaravelLocalization::transRoute('routes.balans'), ['as' => 'balans', 'uses' => 'AllClientsController@balans']);
Route::get(LaravelLocalization::transRoute('routes.zito'), ['as' => 'zito', 'uses' => 'AllClientsController@zito']);
Route::get(LaravelLocalization::transRoute('routes.cineplexx'), ['as' => 'cineplexx', 'uses' => 'AllClientsController@cineplexx']);
Route::get(LaravelLocalization::transRoute('routes.forza'), ['as' => 'forza', 'uses' => 'AllClientsController@forza']);
});
//LANGUAGE SWITCHER
// Route::get('lang/{language}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);


Route::group(['middleware' => ['web'], 'prefix' => LaravelLocalization::setLocale().'/admin'], function () {
    Auth::routes();
    Route::get('logout', 'Auth\LoginController@logout');
});

/* Back end */
Route::group(['middleware' => ['web'], 'prefix' => LaravelLocalization::setLocale().'/admin'], function () {


    Route::get('/', 'SettingsController@index');
    Route::get('/settings', 'SettingsController@index')->name('admin.settings.index');
    Route::post('/settings/store', 'SettingsController@store')->name('admin.settings.store');
    Route::get('/settings/{id}/edit', 'SettingsController@edit')->name('admin.settings.edit');
    Route::post('/settings/update', 'SettingsController@update')->name('admin.settings.update');
    Route::resource('/scripts', 'ScriptsController');
    Route::resource('/categories', 'CategoriesController');
    Route::resource('/users', 'UserController');
    Route::get('/oauth', function () {
        return view('admin.oauth.index');
    });
    Route::get('/slider', 'SliderController@index')->name('admin.slider');
    //POST METHOD FOR SLIDER
    Route::post('slider/create','SliderController@create')->name('admin.slider.create');
    Route::get('slider/{id}/edit', 'SliderController@edit')->name('admin.slider.edit');
    Route::put('slider/update/{id}','SliderController@update')->name('admin.slider.update');
    Route::post('slider/delete/{id}','SliderController@delete')->name('admin.slider.delete');

    //Blog

//    Route::resource('/blog', 'BlogController');
    Route::resource('/blog', 'NewBlogController');
    Route::get('/blog/{id}/posts', 'NewBlogController@postsindex')->name('list.blog.posts');
    Route::get('/blog/{id}/create/post', 'NewBlogController@createpost')->name('create.blog.post');
    Route::get('/blog/{id}/edit/post/{post_id}', 'NewBlogController@editpost')->name('edit.blog.post');
    Route::post('/blog/store/post','NewBlogController@storepost')->name('admin.blogpost.create');
    Route::patch('blog/update/post/{post_id}','NewBlogController@updatepost')->name('admin.blogpost.update');

    Route::get('/blog/{id}/trash', 'NewBlogController@trash');
    Route::post('/blog/delete/post/{id}','NewBlogController@destroypost')->name('admin.blogpost.softdelete');
    Route::post('/blog/empty/trash','NewBlogController@emptytrash')->name('empty.trash');
    Route::post('/blog/permanent/post/{id}','NewBlogController@permanentdeleteitem')->name('admin.blogpost.harddelete');
    Route::post('/blog/restore/post/{id}','NewBlogController@restoreitem')->name('admin.blogpost.restore');

    //Clients
    Route::resource('/clients', 'ClientController');

    //About us
    Route::get('/about', 'AboutUsController@index')->name('admin.about.index');
    Route::get('/about/create', 'AboutUsController@create')->name('admin.about.create');
    Route::post('/about/create', 'AboutUsController@store')->name('admin.about.store');
    Route::patch('/about/update', 'AboutUsController@update')->name('admin.about.update');

    //Team
    Route::resource('/team', 'TeamController');



});