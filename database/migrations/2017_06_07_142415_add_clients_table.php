<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('client');
            $table->string('what_we_did');
            $table->text('project');
            $table->string('mainimage');
            $table->string('mainimagemedium');
            $table->string('mainimagethumb');
            $table->string('custom_field')->nullable();
            $table->string('icon_1')->nullable();
            $table->string('icon_2')->nullable();
            $table->string('icon_3')->nullable();
            $table->string('icon_4')->nullable();
            $table->string('custom_field_2')->nullable();
            $table->string('desktopimg_1')->nullable();
            $table->string('desktopimg_2')->nullable();
            $table->text('description');
            $table->string('description_link');
            $table->string('description_img');
            $table->string('mobileimg_1')->nullable();
            $table->string('mobileimg_2')->nullable();
            $table->string('mobileimg_3')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('client');
    }
}
