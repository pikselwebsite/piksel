<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('instagram');
            $table->string('metaimage');
            $table->string('metatitle');
            $table->string('iosapp');
            $table->string('androidapp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('instagram');
            $table->dropColumn('metaimage');
            $table->dropColumn('metatitle');
            $table->dropColumn('iosapp');
            $table->dropColumn('androidapp');
        });
    }
}
