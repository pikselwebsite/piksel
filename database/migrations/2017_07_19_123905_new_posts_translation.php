<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewPostsTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_post_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('new_post_id')->unsigned();
            $table->string('locale')->index();

            // The actual fields to store the content of your entity. You can add whatever you need.
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('content');
            $table->text('excerpt');
            $table->string('image');
            $table->string('imageomedium');
            $table->string('imagethumb');
            $table->text('category');
            $table->integer('blog_id')->unsigned();
            $table->foreign('blog_id')->references('id')->on('new_blogs');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');


            $table->unique(['new_post_id', 'locale']);
//            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_post_translations');
    }
}
