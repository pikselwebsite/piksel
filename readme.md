<h1>Piksel website</h1>
<h2>Laravel 5.4 Project</h2>

For installation first clone the repo:

<code>
composer install
</code>

<code>
composer require cviebrock/eloquent-sluggable:^4.2.1
</code>

<code>
composer require dimsav/laravel-translatable
</code>

<code>
composer require anhskohbo/no-captcha
</code>

<code>
php artisan migrate
</code>

And you are ready to go