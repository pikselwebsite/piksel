<!doctype html>

<html lang="en" amp>

<head>
    <meta charset="utf-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <title>@yield('title') Piksel</title>
     @yield('metadata')
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="canonical" href="{{url('/')}}">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
    <script src="/js/main.js"></script>
    <link rel="shortcut icon" href="/assets/images/favicon.png" type="image/x-icon">
    
    <style amp-boilerplate>
    body{
        -webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both
        }
        @-webkit-keyframes -amp-start{
            from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
            @keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}
            </style>
            </noscript>
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <!--google analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-46280061-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script type="text/javascript">
_linkedin_data_partner_id = "66150";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=66150&fmt=gif" />
</noscript>
    <!--google analytics end -->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1100857806656160', {
            em: 'insert_email_variable'
        });
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1100857806656160&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>

<body>
<div class='wrap'>
    <div id="arrow-up"><img src="{{url('/')}}/images/belo-strelce.png" alt="gore"></div>
    <header>
        <div class="container-fluid piksel-header">
            <div class='row'>
                <a id='logo-piksel' href='/'><div class="logo col-lg-1 col-md-1 col-xs-2">
                        <!--piksel-logo-->
                        <img src='{{url('/')}}/images/logo-piksel.png'>
                    </div></a>
                <nav class='col-lg-6 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-7 col-sm-offset-1'>
                    <!--menu-->
                    <ul class='nav'>
                        <li>
                            <a {{Request::url() === route('about') ? ' class=act-nav-ab ' : null }} href='{{ route('about') }}'>@lang('menu.about')</a>
                        </li>
                        <li>
                            <a {{Request::url() ===  route('portfolio') ? ' class= act-nav-po ' : null }} href='{{ route('portfolio') }}'>@lang('menu.portfolio')</a>
                        </li>
                        @if(App::getLocale() == 'en')

                        <li>
                            <a {{Request::url() ===  route('blog') ? ' class=act-nav-bl' : null }} href='{{ route('blog') }}'>@lang('menu.blog')</a>
                        </li>
                        @endif
                        <li>
                            <a {{Request::url() ===  route('contact') ? ' class=act-nav-co' : null }} href='{{ route('contact') }}'>@lang('menu.contact')</a>
                        </li>
                        <li>
                            <a {{Request::url() === route('career') ? ' class=act-nav-ca' : null }} href='{{ route('career') }}'>@lang('menu.career')</a>
                        </li>
                    </ul>
                </nav>

                <div class="mobile" >
                    <ul class="mobile-nav" >
                        <li>
                            <a  href='/about'>ABOUT</a>
                        </li>
                        <li>
                            <a  href='/portfolio'>PORTFOLIO</a>
                        </li>
                        <li>
                            <a  href='/blog'>BLOG</a>
                        </li>
                        <li>
                            <a  href='/contact'>CONTACT</a>
                        </li>
                        <li>
                            <a  href='/career'>CAREER</a>
                        </li>
                    </ul>
                </div>              
                <div class="lang col-lg-1 col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-4 col-xs-offset-1">
                 
                    <ul class='lang-ul'>
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li>
                                <a @if(App::getLocale() == $localeCode )  class=active-lang  @endif rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            
                                    {{ strtoupper($localeCode) }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- container -->
    </header>
    <!--header-->

@yield('content')

<!--footer -->
    <footer>

        <div class="copy">
            <p>@lang('footer.piksel')</p>
        </div>
        <a href='/{{App::getLocale()}}'>
            <div class="foot-logo"><img src="{{url('/')}}/images/logo footer.png" alt="Piksel logo"></div>
        </a>
        <div class="social-menu">
            @if($settings->facebook)
                <a id='fb' target="_blank" href='{{ $settings->facebook }}'></a>
            @endif
            @if($settings->twitter)
                <a id='tw' target="_blank" href='{{ $settings->twitter }}'></a>
            @endif
            @if($settings->gplus)
                <a id='go' target="_blank" href='{{ $settings->gplus }}'></a>
            @endif
            @if($settings->instagram)
                <a id='in' target="_blank" href='{{ $settings->instagram }}'></a>
            @endif
            @if($settings->linkedin)
                <a id='li' target="_blank" href='{{ $settings->linkedin }}'></a>
            @endif
        </div>
    </footer>
</div>
<script src="/js/function.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByU20zR0oAthun4ih6yN6Mem_SMCOTmro&callback=initMap">
</script>
@yield('footer')

</body>

</html>