@extends('layouts.main')
@section('title', 'Career | ')
@section('content')

      <div class="contact-title career-part">
                <div class="cut">
                    <h2>@lang('career.title')</h2>
                </div>
            </div><!-- end of title -->

    <div class="contact-career">
        <div class="career-title"><p>@lang('career.paid')</p></div>
        <div class="career-cont">
            <div class="career-first">
                <!--<div data-career ='graph' class="choose-career career-graph">@lang('career.des')</div>-->
                <div data-career ='php' class="choose-career career-php">@lang('career.php')</div>
                <div data-career ='front' class="choose-career career-front">@lang('career.front')</div>
                <div data-career ='soc' class="choose-career career-social">@lang('career.soc')</div>
                <div class="other-career">
                    <p>@lang('career.pos')</p>
                    @lang('career.people')
                    <p class='carrer-touch'>@lang('career.touch')</p><div class='career-arrow'><img src="{{url('/')}}/images/arrow-nadesno.png"  alt="get-in-touch"></div>
                </div>
            </div>
            <div class="career-second">
                <div class="career-cover"></div>
                <div class="carrer-text1">  
                        @lang('career.tekst_des')                  
                </div>
                <div class="carrer-text2">
                    <div class='work graph'>
                             @lang('career.ul_title')
                           <ul >
                             @lang('career.li_des')
                        </ul>
                    </div>
                     <div class='work php'>
                             @lang('career.ul_title')
                           <ul >
                             @lang('career.li_php')
                        </ul>
                    </div>
                     <div class='work front'>
                             @lang('career.ul_title')
                           <ul >
                             @lang('career.li_front')
                        </ul>
                    </div>
                     <div class='work soc'>
                            @lang('career.ul_title2')
                            <ul>
                                @lang('career.li_soc2')
                            </ul>
                             @lang('career.ul_title')
                           <ul >
                             @lang('career.li_soc')
                        </ul>
                    </div>
                   
                    <form class="row" action="{{url('career')}}" method='post' enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-md-6 col-xs-12">
                            <input class='career_form' type="text" placeholder='@lang('career.career_name')' name='c_fullname' value="{{old('c_fullname')}}">
                            @if ($errors->has('c_fullname')) <p
                                    class="alert  alert-danger">Required!</p> @endif
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <input class='career_form' type="tel" placeholder='@lang('career.career_tele')' name='c_phone' value="{{old('c_phone')}}">
                            @if ($errors->has('c_phone')) <p
                                    class="alert  alert-danger">Required!</p> @endif
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <input class='career_form' type="email" placeholder='@lang('career.career_mail')' name='c_email' value="{{old('c_email')}}">
                                @if ($errors->has('c_email')) <p
                                        class="alert  alert-danger">Required!</p> @endif
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <input  type="file" placeholder="No file choosen" name='cv'>
                            @if ($errors->has('cv')) <p
                                    class="alert  alert-danger">Required!</p> @endif
                        </div>
                         <input  class='hidden-form' type="hidden"  name='position' value="No selected">
                        <div class='col-md-6 col-xs-12'style ='margin:3% 0 2% 0;'> 
                                 {!! app('captcha')->display($attributes = [], $lang = null); !!}
                    @if ($errors->has('g-recaptcha-response'))
                    <span class='help-block' style='color:#a94442;'>
                        <strong>The reCaptcha is required</strong>
                    </span>
                    @endif
                            
                              </div>
                        <div class="col-md-12">
                            <textarea class='textarea' type="text" name='c_message'placeholder='@lang('career.career_mes')' rows="8" cols='146'>{!! old('c_message') !!}</textarea>
                            @if ($errors->has('c_message')) <p
                                    class="alert alert-danger">Required!</p> @endif
                        </div>
                         @if(App::getLocale()=='mk')
                     <input id='carrer_submit'type="submit" name='c_submit' value='АПЛИЦИРАЈ'>
                    
                    @endif
                    @if(App::getLocale()=='en')
                         <input id='carrer_submit'type="submit" name='c_submit' value='APPLY NOW'>
                    
                    @endif
                       
                    </form>
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer')
<script>
$(window).on('load',function(){
$top = $('.carrer-touch').position().top;
    $('.career-arrow').css('top',$top);   
})
$(window).on('resize',function(){
$top = $('.carrer-touch').position().top;
    $('.career-arrow').css('top',$top);   
})      
</script>
@endsection
{{--@section('footer')--}}


    {{--<script async defer--}}
            {{--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByU20zR0oAthun4ih6yN6Mem_SMCOTmro&callback=initMap">--}}
    {{--</script>--}}

    {{--@endsection--}}