@extends('layouts.main')
@section('title', 'Contact | ')
@section('content')
    <div class="contact-wrap">
        <div class="contact-title">
            <div class="cut">
                <h2>@lang('contact.contact_title')</h2>
            </div>

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
        </div><!-- end of title -->
        <div class="contact-sec1 row">
            <div class="cont-text col-md-3 col-xs-12">
                <h3>@lang('contact.contact_line')</h3>
                @if ($errors->has('name')) <p
                        class="alert alert-danger">{{ $errors->first('name') }}</p> @endif
                @if ($errors->has('email')) <p
                        class="alert alert-danger">{{ $errors->first('email') }}</p> @endif
                @if ($errors->has('bodyMessage')) <p
                        class="alert alert-danger">{{ $errors->first('bodyMessage') }}</p> @endif
            </div>
            <div class="cont-form col-md-9 col-xs-12">
                <form action="{{url('contact')}}" method='post'>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input class='form-id' type="text" data-holder='@lang('contact.contact_name')'name='name' placeholder='@lang('contact.contact_name')' value="{{old('name')}}"><br>
                    <input class='form-id'  type="text" data-holder='@lang('contact.contact_com')' name='company' placeholder='@lang('contact.contact_com')' value="{{old('company')}}"><br>
                    <input class='form-id'  type="tel" data-holder='@lang('contact.contact_tel')' name='telephone' placeholder='@lang('contact.contact_tel')' value="{{old('telephone')}}"><br>
                    <input class='form-id'  type="email" data-holder='@lang('contact.contact_email')' name='email' placeholder='@lang('contact.contact_email')' value="{{old('email')}}"><br>
                    <input class='form-id'  type="url" data-holder='@lang('contact.contact_web')' name='web' placeholder='@lang('contact.contact_web')' value="{{old('web')}}">
                    {!! app('captcha')->display($attributes = [], $lang = null); !!}
                    @if ($errors->has('g-recaptcha-response'))
                    <span class='help-block' style='color:#a94442;'>
                        <strong>The reCaptcha is required</strong>
                    </span>
                    @endif
                    <textarea class='form-messid'  type="text" name='bodyMessage' data-holder='@lang('contact.contact_mes')' placeholder='@lang('contact.contact_mes')' rows="19" cols='50'>{!! old('bodyMessage') !!}</textarea>
                      @if(App::getLocale()=='mk')
                       <input id='cont-button' class='contact-mk' type="submit" name='submit' value='ИСПРАТИ'>
                    
                    @endif
                    @if(App::getLocale()=='en')
                          <input id='cont-button' type="submit" name='submit' value='SEND'>
                    
                    @endif
                </form>
            </div>
        </div><!-- end of section 1 -->
        <div class="contact-sec2">
            <div class="cont-addres col-md-2 col-xs-12">
                <div class="addres">
                    <h4>@lang('contact.contact_add'): @lang('contact.contact_add_des')</h4>
                    <a href="mailto:{{$settings->email}}">@lang('contact.contact_mail'): @lang('contact.contact_mail_des')</a>
                    <h4>@lang('contact.contact_tele'): @lang('contact.contact_tel_des')</h4>
                </div>
            </div>
            <div class="contact-map col-md-10 col-xs-12">
                <div id='map' class="map" data-lat="{{$settings->lat}}" data-lng="{{$settings->lng}}">

                </div>
                {{--<script>--}}
                    {{--var mapLat = $('#map').data('lat').val();--}}
                    {{--var mapLng = $('#map').data('lng').val();--}}
                    {{--function initMap() {--}}
                        {{--var uluru = {lat: mapLat, lng: mapLng};--}}
                        {{--var map = new google.maps.Map(document.getElementById('map'), {--}}
                            {{--zoom: 4,--}}
                            {{--center: uluru--}}
                        {{--});--}}
                        {{--var marker = new google.maps.Marker({--}}
                            {{--position: uluru,--}}
                            {{--map: map--}}
                        {{--});--}}
                    {{--}--}}
                {{--</script>--}}

            </div>
        </div><!-- end of section 2 -->
    </div>


@endsection
@section('footer')
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
{{--@section('footer')--}}
    {{--<script async defer--}}
            {{--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByU20zR0oAthun4ih6yN6Mem_SMCOTmro&callback=initMap">--}}
    {{--</script>--}}
    {{--@endsection--}}