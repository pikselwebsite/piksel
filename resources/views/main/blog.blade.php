
@extends('layouts.main')
@section('title')
    Blog |
@endsection

@section('metadata')

    <meta name="description" content="Checkout our blog posts and case studies" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Blog">
    <meta itemprop="description" content="Checkout our blog posts and case studies">
    <meta itemprop="image" content="{{URL::to('/').'/assets/images/blog.jpg'}}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="corp_blog_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="Blog">
    <meta name="twitter:description" content="Checkout our blog posts and case studies">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{URL::to('/').'/assets/images/blog.jpg'}}">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="Blog" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{Request::url()}}" />
    <meta property="og:image" content="{{URL::to('/').'/assets/images/blog.jpg'}}" />
    <meta property="og:description" content="Checkout our blog posts and case studies" />
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')

    <div class="blog-wrap">
        <div class="blog-title">
            <div class='cut'>
                <h2>Blog</h2>
            </div>
        </div>
        <div class="bl bl-filter">
            <a style="text-transform: uppercase; letter-spacing: 2px; font-weight: bold;" class="bl-all" data-rel='blog-all'>ALL</a>

            @foreach($blogs as $blog)
                 <a style="text-transform: uppercase; letter-spacing: 2px; font-weight: bold;" class="bl-{{$blog->slug}}" data-rel='blog-{{$blog->slug}}' >{{$blog->name}}</a>
            @endforeach
        </div>
        <div class="blog-posts">
            <div class="col-posts">          
            @foreach($posts as $post)
                    <div class="posts blog-all blog-{{strtolower($blogCategories->find($post->category)->name)}}">
                        <div class="img-post">
                            <img src="{{URL::to('/')}}/assets/img/posts-images/medium/{{$post->imageomedium}}" alt="{{$post->image}}">
                        </div>
                        <div class="posts-text">
                            <p style="text-transform: uppercase; letter-spacing: 2px; font-weight: bold;" class='tag-{{strtolower($blogCategories->find($post->category)->name)}}'>{{$blogCategories->find($post->category)->name}}</p>
                            <p>{!! $post->title !!}</p>
                            <a class='blog-arrow {{strtolower($blogCategories->find($post->category)->name)}}-back' href='{{route('post', $post->slug)}}'>
                                <span ><img src="../images/arrow-nadesno.png" alt="strelka"></span></a>
                        </div>
                    </div>
                    <!-- blogs -->

            @endforeach
            <!--- second line -->
            </div>
        </div>
    </div>

@endsection