@extends('layouts.main')
@section('title')
    {{$post->title}} |
@endsection
@section('metadata')
    <meta name="description" content="{{$post->excerpt}}" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{$post->title}}">
    <meta itemprop="description" content="{{$post->excerpt}}">
    <meta itemprop="image" content="{{URL::to('/').'/assets/img/posts-images/'.$post->image}}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="post_blog_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="{{$post->title}}">
    <meta name="twitter:description" content="{{$post->excerpt}}">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{URL::to('/').'/assets/img/posts-images/'.$post->image}}">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="{{$post->title}}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{Request::url()}}" />
    <meta property="og:image" content="{{URL::to('/').'/assets/img/posts-images/'.$post->image}}" />
    <meta property="og:description" content="{{$post->excerpt}}" />
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')

    <div class="post-wrap">
        <div class="blog-title"><div class='cut'>
                <h2>{{ $post->title }}</h2>

            </div>
        </div>
        <div class="post-img"
             style="background-image: url('/assets/img/posts-images/{{ $post->image }}');
                     background-size: cover;
                     background-repeat: no-repeat;
                     background-position: 50% 50%;
                     ">
            <!--<img src="" alt="">-->
            <div class="post-tag">{{ $blogCategories->name }}</div>
        </div>


        <div class="post-text">

            <div class="author">
                <p>{{ $date->created_at->format('d-m-Y') }}<span class='post-box'></span>By {{ $user->name }}</p>
            </div>
            <h4>{{ $post->title }}</h4>
            {!! $post->content !!}
        </div>

        <div class="post-info">
            <div class="post-author">
                <div class="author-img"><img src="/assets/img/avatars/thumbnails/{{$user->image}}" style="width: 100%; height: auto;" /></div>
                <div class="author-info">
                    <h5>{{ $user->name }}</h5>
                    {!! $user->about !!}
                    <div class="aut-icons">

                           @if(!empty($user->facebook)) <a href="{{$user->facebook}}"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>@endif
                           @if(!empty($user->instagram))<a href="{{$user->instagram}}"><i class="fa fa-instagram" aria-hidden="true"></i></a>@endif
                           @if(!empty($user->twitter))<a href="#{{$user->twitter}}"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>@endif
                           @if(!empty($user->linkedin))<a href="{{$user->linkedin}}"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>@endif
                    </div>
                </div>
            </div>
            <div class="post-social">
                <h5>SHARE IT:</h5>
                <div class="share-buttons">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}" target="_blank"><div class="post-fb col-md-6 fb-share-button" data-href="{{ Request::url()}}" data-layout="button_count" ><img src="{{url('/')}}/images/facebook-share.png" alt="fb"></div></a>
                    <a href="https://twitter.com/intent/tweet?text={{ $post->title}} {{Request::url()}}"><div class="post-twit col-md-6"><img src="{{url('/')}}/images/twitter-share.png" alt="Twitter"></div></a>
                    <a href='https://plus.google.com/share?url={{Request::url()}}'><div  class="post-link col-md-6"><img src="{{url('/')}}/images/google-share.png" alt="google +"></div>
                    <a href='https://www.linkedin.com/shareArticle?mini=true&url={{Request::url()}}'><div class="post-insta col-md-6"><img src="{{url('/')}}/images/linkedin-share.png" alt="link"></div></a>
                </div>
            </div>
        </div>


        <div class="post-nav">
                @if($previous)
                        <a class="prev-post" href='{{route('post',$previous->slug)}}'>
                            <span id='post-left' class='glyphicon glyphicon-arrow-left'></span>
                            <h5>PREVIOUS POST</h5>
                            <p>{{$previous->title}}</p>
                        </a>
                @endif

                @if($next)
                        <a class="next-post" href='{{route('post',$next->slug)}}'>
                            <span id='post-right' class='glyphicon glyphicon-arrow-right'></span>
                            <h5>NEXT POST</h5>
                            <p>{{$next->title}}</p>
                        </a>
                    @endif
        </div>
        <div class="pop-post">
            <div id='pop-title'><h5>RELATED ARTICLES IN <span class='tag-soc'><!--{{strtoupper($blogCategories->find($post->category)->name)}}--></span></h5></div>
            <div class="posts-row">

                @foreach($randomPosts as $rp)
                    <div class="posts">
                        <img src="/assets/img/posts-images/{{ $rp->image }}" alt="image">
                        <div class="posts-text">
                            <h6 class='tag-{{strtolower($blogCategories->find($rp->category)->name)}}'>{{$rp->title}}</h6>
                            <a class='blog-arrow {{strtolower($blogCategories->find($rp->category)->name)}}-back' href='{{$rp->slug}}'><span
                                        class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- post -->



@endsection

@section('footer')
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=652953341561450";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        //fb-share    </script>

    <script>window.twttr = (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
                t = window.twttr || {};
            if (d.getElementById(id)) return t;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
            t._e = [];
            t.ready = function(f) {
                t._e.push(f);
            };
            return t;
        }(document, "script", "twitter-wjs"));
        //twitter-share </script>

    <script src="https://apis.google.com/js/platform.js" async defer></script>
    @endsection