@extends('layouts.main')

@section('metadata')

    <meta name="description" content="{!! strip_tags($settings->description)!!}" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{$settings->metatitle}}">
    <meta itemprop="description" content="{!! strip_tags($settings->description)!!}">
    <meta itemprop="image" content="{{URL::to('/').'/assets/images/'.$settings->metaimage}}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="corp_home_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="{{$settings->metatitle}}">
    <meta name="twitter:description" content="{!! strip_tags($settings->description)!!}">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{URL::to('/').'/assets/images/'.$settings->metaimage}}">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="{{$settings->metatitle}}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{$settings->mainurl}}" />
    <meta property="og:image" content="{{$settings->metaimage}}" />
    <meta property="og:description" content="{!! strip_tags($settings->description)!!}" />
    <meta property="og:site_name" content="{{URL::to('/').'/assets/images/'.$settings->metatitle}}" />
@endsection

@section('content')
    <!-- Section -->
    <div class="section-1">
        <div class="row">
            <div class="title">

            </div>
            @if(count($images) > 0 )
            <div class='sec1-img col-md-12'>
                <div @if(App::getLocale()=="mk") id='mk-naslov' @endif class="text-title">
                    <div class='cut'><h2  class='first-title'>@lang('slider.yourGuide')<span id='s'>&nbsp @lang('slider.throughThe') </span></h2></div><div class='cut'><h2 class='second-title'><span id='b'>@lang('slider.digitalWorld')</span></h2></div>

                </div>


                <div class="counter">
                    <p class='num'>01</p>
                </div>
                <!-- slider -->
                <div id="pikselCarousel" class="carousel slide vertical"  data-ride="carousel" >
                    <!-- SLIDER PIKSEL -->
                    <!-- Indicators -->
                    <!--<ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>-->


                    <div id="slider-background" >
                        <div class="sli-back"></div>
                        <div class="new-back"></div>
                    </div>


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">

                        @foreach($images as $index => $image)
                            <div class="item @if($index == 0) active @endif">
                                <!-- First item -->
                                <div class="client col-md-3 col-md-offset-2">
                                    <ul>
                                        <li><span id='clinet-info'>@lang('slider.client')</span>
                                            {{$image->client}}
                                        </li>
                                        
                                        <li><span id='clinet-info'>
                                            @if($image->client =='Forza')
                                            @lang('slider.whatWeDid')
                                            @else
                                             @lang('slider.whatWeDo')                                       
                                             @endif
                                            </span>
                                            <p>{!! $image->description !!}</p>
                                        </li>
                                       
                                    </ul>
                                </div>
                                <div class="crop-img col-md-7">
                                    <a href="{{url('/').'/'. App::getLocale().$image->weblink}}">
                                        <img src="/assets/img/pikselslider/{{ $image->image }}" alt="{{ $image->name }}">
                                    </a>
                                </div>
                            </div>


                        @endforeach
                    </div>

                    <!-- Left and right controls -->
                    <a class="down" id='des-chevron' role="button" data-target="#pikselCarousel" data-slide="prev">
                        <span ><img src="../images/arrows-dolu.png" alt="dolu"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="up" id='des-chevron' role="button" data-target="#pikselCarousel" data-slide="next">
                        <span > <img src="../images/arrows-gore.png" alt="gore"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <a class="right" id='mob-chevron' role="button" data-target="#pikselCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <a class="left" id='mob-chevron' role="button" data-target="#pikselCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                </div>
                <!-- end of carousel -->
                    @endif
            </div>
        </div>

    </div>
    <!-- end of section-1 -->

    <div class="divider">
        <div class="box-1" data-stellar-ratio="1.2" ></div>
        <div class="box-2" data-stellar-ratio="1.1" ></div>
        <div class="box-3" data-stellar-ratio="1.2" ></div>
    </div>

    <!-- end of divider -->
    <div class="section-2">
        <div class="about">
            <!-- about section-->
            <div class='row about-row'>
                <div class="ab-title col-lg-4  col-md-5  col-xs-12">
                    <!-- title -->
                    <div class='cut'><h2>@lang('home.about')</h2></div>
                    <div class='box-decoration ab-box @if(App::getLocale()=="mk") mk-box-1 @endif'>
                        <div class="inner-box">
                            
                        </div>
                    </div>
                </div>
                <div class="ab-desc col-lg-8 col-md-7 col-xs-12">
                    <!-- text -->
                    <p id='ab-text'>@lang('home.about_des')</p>
                    <a href='{{ route('about') }}'>
                        <div id='arrow'><span><img src="../images/arrow-nadesno.png" alt="desno"></span></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end of section-2 -->
    <div class="section-3">

        <div class="services-title">
            <!-- services title-->
            <div class='cut'><h2>@lang('home.services')</h2></div>

            <div class='box-decoration @if(App::getLocale()=="mk") mk-box  @endif sv-box'>
                <div class="inner-box"></div>
            </div>
        </div>
        <div class="sv-icons">
            <!-- icons -->
            <div class="icons ic-1">
                <img class='img-responsive' src="images/icon-dev.jpg" alt="dev">
                <div class="services-text">
                    <p>@lang('home.services_web_title')</p>
                    <p class='services-description'>@lang('home.services_web')
                    </p>
                </div>
            </div>
            <div class="icons ic-3">
                <div class="services-text">
                    <p>@lang('home.services_dig_title')</p>
                    <p class='services-description'>@lang('home.services_dig')</p>
                </div>
                <img src="images/icon-marketing.jpg" alt='marketing'>
            </div>
            <div class="icons ic-2">
                <img src="images/icon-design.png" alt="des">
                <div class="services-text">
                    <p>@lang('home.services_des_title')</p>
                    <p class='services-description'>@lang('home.services_des')</p>
                </div>
            </div>
            <div class="icons ic-7">
                <div class="services-text">
                    <p>@lang('home.services_pho_title')</p>
                    <p class='services-description'>@lang('home.services_pho')</p>
                </div>
                <img src="images/icon-photo.jpg" alt="photo">
            </div>
        </div>
    </div>
    <!-- end of section-3-->
    <div class="section-4">
        <div class="client-title">
            <div class='cut'><h2>@lang('home.clients')</h2></div>
            <div class='box-decoration cl-box'>
                <div class="inner-box"></div>
            </div>
        </div>
        <div class='row '>
            <div class="col-md-12 col-xs-12">
                <div class='clients cl-1 col-md-2 col-xs-4 '>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img1'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4 '>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img2'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4 '>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img3'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4 '>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img4'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img5'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img6'></div></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img7'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img8'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img9'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img10'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img11'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img12'></div></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img13'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img14'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img15'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img16'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img17'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img18'></div></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img19'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img20'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img21'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img22'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img23'></div></div>
                    </div>
                </div>
                <div class='clients cl-1 col-md-2 col-xs-4'>
                    <div class="clients-info">
                        <div class="clients-image"><div class='img img24'></div></div>
                    </div>
                </div>
            </div>
        </div>
        <a id='home-portfolio-button' href='{{route('portfolio')}}'>@lang('home.portf_button')</a>
    </div>
    <!-- end of section-4 row-3-->

    <div class="section-5">
        <div class="cont-title">
            <div class='cut'><h2>@lang('home.contact')</h2></div>
            <div class='box-decoration ct-box'>
                <div class="inner-box"></div>
            </div>
            <h5>@lang('home.contact_moto')</h5>
        </div>
        <div class="contact-form">
            <form action="{{url('homecontact')}}" method='post'>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input class='form-id' type="text" data-holder='@lang('home.holder_name')' name='user' placeholder='@lang('home.holder_name')'>
                @if ($errors->has('user')) <p
                        class="alert  alert-danger">Required!</p> @endif<br>
                <input class='form-id' type="email" data-holder='@lang('home.holder_mail')' name='email' placeholder='@lang('home.holder_mail')'>
                @if ($errors->has('email')) <p
                        class="alert  alert-danger">Required!</p> @endif<br>
                <input class='form-id' type="text" data-holder='@lang('home.holder_message')' name='bodymessage' placeholder='@lang('home.holder_message')'>
                @if ($errors->has('bodymessage')) <p
                        class="alert  alert-danger">Required!</p> @endif<br>
                         {!! app('captcha')->display($attributes = [], $lang = null); !!}
                    @if ($errors->has('g-recaptcha-response'))
                    <span class='help-block' style='color:#a94442;'>
                        <strong>The reCaptcha is required</strong>
                    </span>
                    @endif
                <div class="send-button">
                    <span class='cube'></span>
                    <div class='send-background'></div>
                    @if(App::getLocale()=='mk')
                    <input id='form-button' type="submit" name='submit' value='ИСПРАТИ'>
                    
                    @endif
                    @if(App::getLocale()=='en')
                         <input id='form-button' type="submit" name='submit' value='SEND'>
                    
                    @endif
                </div>
            </form>

        </div>
        <a id='location' @if(App::getLocale()=='mk') class='odvedime'  @endif data-toggle='modal' data-target='#Piksel' href="#"><img src="../images/location.png" alt="location">
            <p>@lang('home.contact_takeme')</p>
        </a>

        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{ Session::get('flash_message') }}
            </div>
        @endif

    </div>
    <!-- end of section 5 -->
    <!-- End Section -->

    <!--modal -->
    <div class="modal fade" id="Piksel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop='false'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="map" class='map'></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal-->
@endsection

@section('footer')
<script>
    
    // $(document).ready(function(){
$(document).ready(function(){
    $countTop=$('.counter').position().top+5;// counter for slider position top
 $countBottom=$('.counter').position().top + $('.counter').outerHeight()-15;// counter slider bootm positon
 $pad = $('.counter').css('padding-left');// counter slider left padding
 $newpad = Number($pad.slice(0,-2));// geting clear number from pad
$countleft =$('.counter').offset().left + $newpad;// counter slider + padding
 $('.up').css('top',$countTop);
  $('.up').css('left',$countleft);// adding new positon to arrows for counter slider
  $('.down').css('left',$countleft);
 $('.down').css('top',$countBottom);
})       
  $(window).on('resize',function(){
    
       $pad = $('.counter').css('padding-left'); // counter slider left padding
    $newpad = Number($pad.slice(0,-2)); // geting clear number from pad
    $countleft =$('.counter').offset().left + $newpad;// counter slider + padding
     $countTop=$('.counter').position().top+5; // counter for slider position top
    $countBottom=$('.counter').position().top + $('.counter').outerHeight()-15;// counter slider bootm positon
     $('.up').css('left',$countleft); // adding new positon for arrows counter slider
    $('.up').css('top',$countTop);
    $('.down').css('top',$countBottom);
      $('.down').css('left',$countleft);
  });

</script>
@endsection

