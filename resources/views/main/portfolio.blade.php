@extends('layouts.main')
@section('title')
    Portfolio |
@endsection

@section('metadata')

    <meta name="description" content="We are proud of our client list" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Portfolio">
    <meta itemprop="description" content="We are proud of our client list">
    <meta itemprop="image" content="{{URL::to('/').'/assets/images/blog.jpg'}}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="corp_blog_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="Portfolio">
    <meta name="twitter:description" content="We are proud of our client list">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{URL::to('/').'/assets/images/portfolio.jpg'}}">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="Portfolio" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{Request::url()}}" />
    <meta property="og:image" content="{{URL::to('/').'/assets/images/blog.jpg'}}" />
    <meta property="og:description" content="We are proud of our client list" />
    <meta property="og:site_name" content="Piksel LTD" />
@endsection

@section('content')
   
<div class="porf-section">
            <div class="portfolio-title">
               <div class='cut'><h2>@lang('portfolio.title')</h2></div>
            </div>
               <div class="bl pr-filter">
                <a class="pr-all" data-rel ='all'  target='_blank'>@lang('portfolio.filter1')</a>
                <a class="pr-web" data-rel='web' target='_blank' >@lang('portfolio.filter2')</a>
                <a class="pr-pho" data-rel ='dig'  target='_blank' >@lang('portfolio.filter3')</a>
                 <a class="pr-soc" data-rel ='gra'  target='_blank' >@lang('portfolio.filter4')</a>
            </div>
            <div class='row '>
             <a class='clients-box' href='{{ route('bimilk') }}'>        
                <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                     <div class='cl-images'><img class='prof-img'src="../images/bimilk.png" /></div>
                    <div class="overlay overlay-bimilk">
                        <div class="text-portf">
                                <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client1')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client1_what')</p>
                                    </div>
                                </div>
                                <a href='{{ route('bimilk') }}'><div class="potfolio-arrow"><span ><img src="../images/arrow-nadesno.png" alt="strelka"></span></div></a></div>

                        </div>
                    </div>
                </div>  
                </a>
                   <!-- items -->
                     <a class='clients-box' href='{{ route('cineplexx') }}'>
                   <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                    <div class='cl-images'><img class='prof-img'src="../images/cine.png" /></div>
                    <div class="overlay overlay-cine">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client2')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client2_what')</p>                   
                                </div>
                                </div>
                                <a href='{{ route('cineplexx') }}'><div class="potfolio-arrow"><span ><img src="../images/arrow-nadesno.png" alt="strelka"></span></div></a></div>
                        </div>
                    </div>
                </div>  
                    </a>
                       <!-- items -->    
            <a class='clients-box' href='{{ route('zito') }}'> 
                <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all dig soc gra'>
                     <div class="portf-cont">
                    <div class='cl-images'><img class='prof-img'src="../images/zito.png" /></div>
                    <div class="overlay overlay-zito">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client3')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client3_what')</p>
                               </div>
                               </div>
                                <a href='{{ route('zito') }}'><div class="potfolio-arrow"><span ><img src="../images/arrow-nadesno.png" alt="strelka"></span></div></a></div>
                        </div>
                    </div>
                </div>  
                </a>
                   <!-- items -->
                  <a class='clients-box' href='{{ route('balans') }}'> 
                   <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web dig soc gra'>
                     <div class="portf-cont">
                    <div class='cl-images'><img class='prof-img'src="../images/balans-logo.png" /></div>
                    <div class="overlay overlay-balans">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client4')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client4_what')</p>
                                </div>
                                </div>
                                <a href='{{ route('balans') }}'><div class="potfolio-arrow"><span ><img src="../images/arrow-nadesno.png" alt="strelka"></span></div></a></div>
                        </div>
                    </div>
                </div>  
                </a>
                <!-- items -->
                  <a class='clients-box' href='{{ route('linea') }}'> 
                   <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web'>
                     <div class="portf-cont">
                    <div class='cl-images'><img class='prof-img'src="../images/linea_logo.png" /></div>
                    <div class="overlay overlay-linea">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client5')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client5_what')</p>
                                </div>
                                </div>
                                <a href='{{ route('linea') }}'><div class="potfolio-arrow"><span ><img src="../images/arrow-nadesno.png" alt="strelka"></span></div></a></div>
                        </div>
                    </div>
                </div>  
                </a>
                <!-- items -->
                        <a class='clients-box' href='clients/forza'>
                   <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig web gra'>
                     <div class="portf-cont">
                    <div class='cl-images'><img class='prof-img'src="../images/forza.png" /></div>
                    <div class="overlay overlay-forza">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client6')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client6_what')</p>
                               </div>
                               </div>
                                <a href='{{ route('forza') }}'><div class="potfolio-arrow"><span ><img src="../images/arrow-nadesno.png" alt="strelka"></span></div></a></div>
                        </div>
                    </div>
                </div>  
                    </a>
                       <!-- items -->
                     <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all dig web soc gra'>
                     <div class="portf-cont">
                    <div class='cl-images'><img class='prof-img'src="../images/pap.png" /></div>
                    <div class="overlay overlay-pap">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                     <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client7')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client7_what')</p>
                                </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>  
                   <!-- items -->
                     <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web'>
                     <div class="portf-cont">
                    <div class='cl-images'><img class='prof-img'src="../images/alec-logo.png" /></div>
                    <div class="overlay overlay-alec">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client8')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client8_what')</p>
                                </div>
                             </div>
                             </div>
                        </div>
                    </div>
                </div>  
                <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web'>
                     <div class="portf-cont">
                    <div class='cl-images'><img class='prof-img'src="../images/etrusca.png" /></div>
                    <div class="overlay overlay-etrusca">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client9')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client9_what')</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/trend.png" /></div>
                    <div class="overlay overlay-trend">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client10')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client10_what')</p>    
                                </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/exide.png" /></div>
                    <div class="overlay overlay-exide">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client11')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client11_what')</p>   
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client11_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/delco.png" /></div>
                    <div class="overlay overlay-delco">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client12')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client12_what')</p>  
                                </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/logo-dime.png" /></div>
                    <div class="overlay overlay-dime">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client13')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client13_what')</p>  
                                </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc gra'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/gojo.png" /></div>
                    <div class="overlay overlay-gojo">
                        <div class="text-portf">
                                <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client14')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client14_what')</p>      
                                    </div>
                                 </div>   
                                    <p class='description-clients'>@lang('portfolio.client14_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/lotel.png" /></div>
                    <div class="overlay overlay-lotel">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client15')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client15_what')</p>      
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client15_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/akademik.png" /></div>
                    <div class="overlay overlay-akademik">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                     <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client16')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client16_what')</p>     
                                </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/telma.png" /></div>
                    <div class="overlay overlay-telma">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                  <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client17')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client17_what')</p>           
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client17_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/monin.png" /></div>
                    <div class="overlay overlay-monin">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                    <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client18')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client18_what')</p>      
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client18_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/portioli.png" /></div>
                    <div class="overlay overlay-portioli">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                     <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client19')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client19_what')</p>     
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client19_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/samantha.png" /></div>
                    <div class="overlay overlay-samantha">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client20')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client20_what')</p>             
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client20_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/chillin.png" /></div>
                    <div class="overlay overlay-chillin">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                  <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client21')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client21_what')</p>         
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client21_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/kimbo.png" /></div>
                    <div class="overlay overlay-kimbo">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                   <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client22')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client22_what')</p>              
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client22_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all soc dig'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/savana.png" /></div>
                    <div class="overlay overlay-savana">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                 <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client23')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client23_what')</p>         
                                </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/bim.png" /></div>
                    <div class="overlay overlay-bim">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                 <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client24')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client24_what')</p>          
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client24_des')</p>
                            </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
                    <div class='clients portfol cl-1 col-lg-3 col-md-4 col-xs-12 all web'>
                     <div class="portf-cont">
                   <div class='cl-images'><img class='prof-img'src="../images/unix.png" /></div>
                    <div class="overlay overlay-unix">
                        <div class="text-portf">
                               <div class="client-newtext">
                                    <div class="pr-line"></div>
                                    <div class="cl-text">
                                 <h5>@lang('portfolio.client')</h5>
                                    <p>@lang('portfolio.client25')</p>
                                    <h5>@lang('portfolio.what')</h5>
                                    <p>@lang('portfolio.client25_what')</p>         
                                </div>
                                </div>
                                <p class='description-clients'>@lang('portfolio.client25_des')</p>
                                </div>
                        </div>
                    </div>
                </div>  
                 <!-- items -->
            </div>
            <div class="porf-box-1" data-stellar-ratio='1.5'></div>
             <div class="porf-box-2" data-stellar-ratio='1.5'></div>
        </div>
        <!-- end of section-4 -->



    </div>
    <!-- end of section-4 -->
@endsection

