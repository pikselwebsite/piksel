@extends('layouts.main')
@section('title')
    {{$client->title}} |
@endsection
@section('content')
    <div class="client-wrap ">
        <a class='pull' href='/portfolio'><span class="glyphicon glyphicon-arrow-left"></span> Back to portfolio</a>
        <div class="client-sec1">
            <h2>{{ $client->title }}</h2>
            <div class="cl">
                <div class='client-txt'>
                    <h5>CLIENT</h5>
                    <p>{{ $client->title }}</p>
                </div>
                <div class='client-txt'>
                    <h5>WHAT WE DID</h5>
                    <p>{{ $client->what_we_did }}</p>
                </div>
                <div class='client-txt'>
                    <h5>PROJECT</h5>
                    <p>{!! $client->project !!}</p>
                </div>
            </div>
            <img class='img-responsive' src="/assets/img/client-images/{{ $client->mainimage }}" alt="{{ $client->title }}">
        </div>

        <!-- end of part 1 -->
        <div class="client-sec2">
            <h4>{{ $client->custom_field }}</h4>
            <div class="row ic-row">
                <div class='col-md-3 col-xs-3'><img class='img-responsive' src="/assets/img/client-images/{{ $client->icon_1 }}" alt="Icons"></div>
                <div class='col-md-3 col-xs-3'><img class='img-responsive'  src="/assets/img/client-images/{{ $client->icon_2 }}" alt="Icons"></div>
                <div class='col-md-3 col-xs-3'><img class='img-responsive'  src="/assets/img/client-images/{{ $client->icon_3 }}" alt="Icons"></div>
                <div class='col-md-3 col-xs-3'><img class='img-responsive'  src="/assets/img/client-images/{{ $client->icon_4 }}" alt="Icons"></div>
            </div>
        </div>
    </div>
    <div class="client-wrap2">
        <div data-stellar-ratio="1.2" class="cl-part1">
            <img data-stellar-ratio="1.2" class='img-responsive'src="/assets/img/client-images/{{ $client->desktopimg_1 }}" alt="proekt1">
        </div>
        <div data-stellar-ratio="1.3" class="cl-part2">
            <h4>{{ $client->custom_field_2 }}</h4>
            <img  class='img-responsive' src="/assets/img/client-images/{{ $client->desktopimg_2 }}" alt="proekti2">
        </div>
    </div>
    <!-- end of part 2 -->
    <div class="client-wrap3">
        <div class="row">
            <div class="col-md-6 col-xs-6 bo">
                <div class="blue-box">
                    <h4>Some more info about the website features</h4>
                    <p>{!! strip_tags($client->description) !!}</p>
                    <a href='{{ $client->description_link }}'>SEE MORE IN CASE STUDY <span class='glyphicon glyphicon-arrow-right'></span></a>
                </div>
            </div>
            <div class="col-md-6 col-xs-6"><img class='img-responsive' src="/assets/img/client-images/{{ $client->description_img }}" alt="detali"></div>
        </div>
    </div>
    <!-- end of part 3 -->
    <div class="client-wrap4">
        <img class='img-responsive' src="/assets/img/client-images/{{ $client->mobileimg_1 }}" alt="mob">
        <img class='img-responsive' src="/assets/img/client-images/{{ $client->mobileimg_2 }}" alt="mob">
        <img class='img-responsive' src="/assets/img/client-images/{{ $client->mobileimg_2 }}" alt="mob">

    </div>
    <!-- end of part 4 -->
    <div class="client-wrap5">
        <a class='up-client' ><span class='glyphicon glyphicon-arrow-up'></span></a>
        <a href='clients.html'><div class='left-client'><span class='glyphicon glyphicon-arrow-left'></span><h2>Forza</h2><p>Social Media, B.I, Media Campaign</p></div></a>
        <a href='clients.html'><div class='right-client'><span class='glyphicon glyphicon-arrow-right'></span><h2>Bimilk</h2><p>Social Media, B.I, Media Campaign</p></div></a>
    </div>
@endsection