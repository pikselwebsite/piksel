@extends('layouts.main')
@section('title')
    @lang('404.title') |
@endsection
@section('metadata')

    <meta name="description" content="@lang('404.siteNotFound')" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="@lang('404.title')">
    <meta itemprop="description" content="@lang('404.siteNotFound')">
    {{--<meta itemprop="image" content="{{url('/')}}/images/forza-fb.png">--}}

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="error_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="@lang('404.title')">
    <meta name="twitter:description" content="@lang('404.siteNotFound')">
    <meta name="twitter:creator" content="@pikselmk">
    {{--<meta name="twitter:image" content="{{url('/')}}/images/forza-fb.png">--}}

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="@lang('404.title')" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{route('notFound')}}" />
    {{--<meta property="og:image" content="{{url('/')}}/images/forza-fb.png" />--}}
    <meta property="og:description" content="@lang('404.siteNotFound')">
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')
    <div class="contact-wrap">
        <div class="contact-title">
            <div class="cut">
                <h2>@lang('404.error')</h2>
            </div>

            @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
        @endif
        </div><!-- end of title -->
        <div class="contact-sec1 row">
            <div class="cont-text col-md-12 col-xs-12">
                <div class="cont-text col-md-6 col-xs-12" align="center">
                    <img class="img-responsive" src="../images/error2.gif" />
                </div>

                <div id="errorImage" class="cont-text col-md-6 col-xs-12">
                <h3 style="text-align: center; font-size: 48px;">@lang('404.siteNotFound')</h3>
                </div>


        </div><!-- end of section 1 -->

        </div>


@endsection
