@extends('layouts.main')
@section('title')
    {{$about->title}} |
    @endsection

@section('metadata')

    <meta name="description" content="{!! strip_tags($about->mission) !!}" />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{$about->title}}">
    <meta itemprop="description" content="{!! strip_tags($about->mission) !!}">
    <meta itemprop="image" content="{{URL::to('/').'/assets/img/posts-images/'.$about->about_image}}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="corp_about_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="{{$about->title}}">
    <meta name="twitter:description" content="{!! strip_tags($about->mission) !!}">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{URL::to('/').'/assets/img/posts-images/'.$about->about_image}}">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="{{$about->title}}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{Request::url()}}" />
    <meta property="og:image" content="{{URL::to('/').'/assets/img/posts-images/'.$about->about_image}}" />
    <meta property="og:description" content="{!! strip_tags($about->mission) !!}" />
    <meta property="og:site_name" content="Piksel LTD" />
@endsection


@section('content')
    <!--header-->
    <div class="ab-wrap">
        <div id="ab-box1" data-stellar-ratio='1.4'></div>
        <div id="ab-box2" data-stellar-ratio='1.4'></div>
        <div id="ab-box3"  data-stellar-ratio='1.3'></div>
        <div id="ab-box4"  data-stellar-ratio='1.3'></div>
        <div class="about-wrap">
            <div class="about-title ">
                <div class='cut'><h2>@lang('about.about_title')</h2></div> <!--{{ $about->title }}-->
            </div>
            <!-- title-->
            <div class="about-sec1">
                <div class="row">                                           <!--{{ $mission }}-->    <!--{{ $vision }}-->
                    <div class="col-md-5 col-xs-12"><h3><span id='ab-border'>@lang('about.mision_title')</span><br><span id='ab-blue'>@lang('about.mision_title2')</span></h3></div>
                    <div class="col-md-7 col-xs-12">
                        <p id='about-text'>@lang('about.mision_des')</p><!-- {!! strip_tags($about->mission) !!}-->
                        <div class="row ab-row">
                            @if(App::getLocale()== 'en')
                            <div class="col-md-6 tag-social">@lang('about.mision_fot_1')<span class='ab-bold'>@lang('about.mision_fot_2')</span></div>
                            <div class="col-md-6"> <span id='ab-bold'>@lang('about.mision_fot_3')</span></div>
                            @endif
                             @if(App::getLocale()== 'mk')
                            <div class="col-md-12"> <span id='ab-bold'class='mk-fonts'>@lang('about.mision_fot_3')</span></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- sec-1 -->
            <div class="about-sec2">
                <div class="ab-sec2-title"> <!--{{ $who }}-->   <!--{{ $others }}-->
                    <h3><span id='ab-border2'>@lang('about.who_1')</span><br><span id='ab-bold2'>@lang('about.who_2')</span></h3>
                    <p id='pi-text'>@lang('about.who_des')</p><!--{{ strip_tags($about->who_we_are) }}-->

                    <div class="row ab-row">
                         @if(App::getLocale()== 'en')
                        <div class="col-md-2" style="color:#c22558">@lang('about.who_fot_1')</div>
                        <div class="col-md-10"> <span id='ab-bold2'>@lang('about.who_fot_2')</span></div>
                         @endif
                           @if(App::getLocale()== 'mk')
                        <div class="col-md-12"> <span id='ab-bold2' class='mk-fonts'>@lang('about.who_fot_2')</span></div>
                         @endif
                    </div>
                </div>
            </div>
            <!-- sec-2 -->

            <!--<div class="about-image">
                {{--@if($team == null)--}}
                    {{--{{'<p>Sorry no team members added at the moment.</p>'}}--}}
                {{--@else--}}
                    @foreach($team as $member)
                        <div class="team-piksel" onmouseover="changeImg(this)" onmouseout="returnImg(this)">
                            <img class='first' data-img1 = '{{$member->hover_image}}' data-img2 ='{{$member->main_image}}' src="/assets/img/team-images/{{$member->main_image}}" alt="team piksel">
                        </div>
                    @endforeach
                {{--@endif--}}
            </div>-->
            <!-- sec-3 -->
        </div>
    </div>


@endsection

@section('footer')
    <script>
        //      function changeImg(element,event){
        //     $(element).find('.first').fadeOut('fast').queue(function(){
        //         $(element).find('.second').fadeIn().dequeue();
        //     })

        // }
        // function returnImg(element){
        //         $(element).find('.second').fadeOut('fast').queue(function(){
        //         $(element).find('.first').fadeIn().dequeue();
        //     })
        //     }
        function changeImg(element){
            var src = $(element).find('.first').attr('data-img1');
            $(element).find('.first').attr('src','/assets/img/team-images/'+src);
        }
        function returnImg(element){
            var src = $(element).find('.first').attr('data-img2');
            $(element).find('.first').attr('src','/assets/img/team-images/'+src);
        }
    </script>
@endsection