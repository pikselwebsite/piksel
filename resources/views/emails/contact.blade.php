<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                <tr>
                    <td align="center" bgcolor="#eee" style="color:#333; display: block;">
                        <div align="center">
                            <br/>
                            <img src="http://i.imgur.com/GDJoMnM.png" alt="Piksel LTD" />
                            <h1>Добивте нова порака</h1>
                        </div>
                    </td>
                </tr>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                        <h2>Детали:</h2>
                        <table width="100%" border="1" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td width="44%" style="padding: 10px 10px 10px 10px">Од:</td>
                                <td width="56%" style="padding: 10px 10px 10px 10px">{{$name}}</td>
                            </tr>
                            <tr>
                                <td width="44%" style="padding: 10px 10px 10px 10px">Емаил:</td>
                                <td width="56%" style="padding: 10px 10px 10px 10px">{{$email}}</td>
                            </tr>
                            <tr>
                                <td width="44%" style="padding: 10px 10px 10px 10px">Компанија: </td>
                                <td width="56%" style="padding: 10px 10px 10px 10px"> {{$company}}</td>
                            </tr>
                            <tr>
                                <td width="44%" style="padding: 10px 10px 10px 10px">Телефон: </td>
                                <td width="56%" style="padding: 10px 10px 10px 10px">{{$telephone}}</td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 10px 10px 10px">Web: </td>
                                <td style="padding: 10px 10px 10px 10px">{{$web}}</td>
                            </tr>

                            <tr>
                                <td style="padding: 10px 10px 10px 10px">Порака: </td>
                                <td style="padding: 10px 10px 10px 10px">{{$bodyMessage}}</td>
                            </tr>

                            </tbody>
                        </table>


                </tr>
                <tr>
                    <td align="center" bgcolor="#eee" style="color:#333; padding: 30px 30px 30px 30px;">
                        НАПОМЕНА: Овој емаил е испратен од контакт форма.<br />
                        ©Piksel LTD 2017</td>

                </tr>
            </table>
        </td>
    </tr>
</table>