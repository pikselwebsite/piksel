@extends('layouts.main')
@section('title')
    Zito |
@endsection
@section('metadata')

    <meta name="description" content="Established in 1988 in Veles, Zito Marketi in the last three decades has grown in one of the biggest domestic market chains in Macedonia." />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Zito">
    <meta itemprop="description" content="Established in 1988 in Veles, Zito Marketi in the last three decades has grown in one of the biggest domestic market chains in Macedonia.">
    <meta itemprop="image" content="{{url('/')}}/images/zito-fb.png">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="zito_client_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="Zito">
    <meta name="twitter:description" content="Established in 1988 in Veles, Zito Marketi in the last three decades has grown in one of the biggest domestic market chains in Macedonia.">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{url('/')}}/images/zito-fb.png">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="Zito" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{route('zito')}}" />
    <meta property="og:image" content="{{url('/')}}/images/zito-fb.png" />
    <meta property="og:description" content="Established in 1988 in Veles, Zito Marketi in the last three decades has grown in one of the biggest domestic market chains in Macedonia.">
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')


        <div class="client-wrap ">
             <div class="arrows-clients">
            <a class='pull' href='{{ route('portfolio') }}'><span class="glyphicon glyphicon-arrow-left"></span>@lang('portfolio.prev')</a>
             <a class='pull-next' href='{{ route('balans') }}'>@lang('portfolio.next') &nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
             </div>
            <div class="client2-sec1 zito">
                <h2>@lang('zito.title')</h2>
                <div class="cl">
                    <div class='client-txt'>
                        <h5>@lang('zito.client_title')</h5>
                        <p>@lang('zito.client')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('zito.what_title')</h5>
                        <p>@lang('zito.what')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('zito.about_title')</h5>
                        <p>@lang('zito.about')</p>
                    </div>
                </div>
                <img class='img-responsive' src="{{url('/')}}/images/zito-fb.png" alt="Linea">
            </div>

            <!-- end of part 1 -->
            <div class="client2-sec2 zito">
                <h4>@lang('zito.social_title')</h4>
                <div class="row soc-row">
                    <p>@lang('zito.social')</p>
                </div>
                <div class="cli-social-icons">
                <a target="_blank" href='https://www.facebook.com/zito.marketi'><img src="{{url('/')}}/images/fb-polno.png" alt="fb"></a>
                <a target="_blank" href='https://www.instagram.com/zito.marketi/'><img src="{{url('/')}}/images/insta polno.png" alt="insta"></a>
                </div>
            </div>
            </div>
        <!-- end of part2-->
        <div class="client2-sec3">
                   <div id="fb-soc">
                 <!-- fb posts -->
                 <div class="mockup-post">
                    <div class="fb-post" data-href="https://www.facebook.com/zito.marketi/posts/1030637613704654:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/zito.marketi/posts/1030637613704654:0" class="fb-xfbml-parse-ignore"><p>&#x41d;&#x430;&#x441;&#x43a;&#x43e;&#x440;&#x43e; &#x43d;&#x435;&#x448;&#x442;&#x43e; &#x441;&#x43e;&#x441;&#x435;&#x43c;&#x430; &#x43d;&#x43e;&#x432;&#x43e; &#x43e;&#x434; &#x416;&#x438;&#x442;&#x43e; &#x41c;&#x430;&#x440;&#x43a;&#x435;&#x442;&#x438;...</p>Posted by <a href="https://www.facebook.com/zito.marketi/">Жито Маркети</a> on&nbsp;<a href="https://www.facebook.com/zito.marketi/posts/1030637613704654:0">Thursday, May 18, 2017</a></blockquote></div>
                    <div class="fb-post" data-href="https://www.facebook.com/zito.marketi/posts/984607558307660:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/zito.marketi/posts/984607558307660:0" class="fb-xfbml-parse-ignore"><p>&#x412;&#x430;&#x448;&#x438;&#x442;&#x435; &#x43e;&#x43c;&#x438;&#x43b;&#x435;&#x43d;&#x438; Elephant &#x43a;&#x440;&#x435;&#x43a;&#x435;&#x440;&#x438;, &#x441;&#x430;&#x43c;&#x43e; &#x432;&#x43e; &#x43f;&#x435;&#x440;&#x438;&#x43e;&#x434;&#x43e;&#x442; &#x43e;&#x434; 31.03 &#x434;&#x43e; 06.04 &#x43f;&#x43e; &#x43e;&#x434;&#x43b;&#x438;&#x447;&#x43d;&#x438; &#x446;&#x435;&#x43d;&#x438; &#x432;&#x43e; &#x441;&#x438;&#x442;&#x435; &#x416;&#x438;&#x442;&#x43e; &#x41c;&#x430;&#x440;&#x43a;&#x435;&#x442;&#x438; &#x1f418;</p>Posted by <a href="https://www.facebook.com/zito.marketi/">Жито Маркети</a> on&nbsp;<a href="https://www.facebook.com/zito.marketi/posts/984607558307660:0">Friday, March 31, 2017</a></blockquote></div>
                    <div class="fb-post" data-href="https://www.facebook.com/zito.marketi/posts/987619528006463:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/zito.marketi/posts/987619528006463:0" class="fb-xfbml-parse-ignore"><p>&#x414;&#x43e;&#x431;&#x440;&#x43e; &#x435; &#x43f;&#x43e;&#x437;&#x43d;&#x430;&#x442;&#x43e; &#x434;&#x435;&#x43a;&#x430; &#x43f;&#x43b;&#x43e;&#x434;&#x43e;&#x432;&#x438;&#x442;&#x435; &#x43d;&#x430; &#x430;&#x440;&#x43e;&#x43d;&#x438;&#x458;&#x430;&#x442;&#x430; &#x441;&#x435; &#x432;&#x438;&#x441;&#x442;&#x438;&#x43d;&#x441;&#x43a;&#x430; &#x432;&#x438;&#x442;&#x430;&#x43c;&#x438;&#x43d;&#x441;&#x43a;&#x430; &#x431;&#x43e;&#x43c;&#x431;&#x430;, &#x430; &#x441;&#x430;&#x43c;&#x43e; &#x432;&#x43e; &#x442;&#x435;&#x43a;&#x43e;&#x442; &#x43d;&#x430; &#x43c;&#x435;&#x441;&#x435;&#x446; &#x430;&#x43f;&#x440;&#x438;&#x43b; &#x437;&#x430;&#x43c;&#x440;&#x437;&#x43d;&#x430;&#x442;&#x430; &#x430;&#x440;&#x43e;&#x43d;...</p>Posted by <a href="https://www.facebook.com/zito.marketi/">Жито Маркети</a> on&nbsp;<a href="https://www.facebook.com/zito.marketi/posts/987619528006463:0">Monday, April 3, 2017</a></blockquote></div>
                 </div>
                 <div class="mockup-post">
                    <div class="fb-post" data-href="https://www.facebook.com/zito.marketi/posts/920572038044546:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/zito.marketi/posts/920572038044546:0" class="fb-xfbml-parse-ignore"><p>&#x421;&#x43e;&#x43d; &#x43c;&#x435;&#x43a; &#x43a;&#x430;&#x43a;&#x43e; &#x43e;&#x431;&#x43b;&#x430;&#x447;&#x435; &#x441;&#x43e; &#x43f;&#x43e;&#x441;&#x442;&#x435;&#x43b;&#x43d;&#x438;&#x43d;&#x430;&#x442;&#x430; &#x43d;&#x430; #TA&#xc7;! 
&#x421;&#x430;&#x43c;&#x43e; &#x437;&#x430; &#x43a;&#x43b;&#x443;&#x431;&#x43e;&#x442; &#x43d;&#x430; &#x432;&#x435;&#x440;&#x43d;&#x438; &#x43a;&#x443;&#x43f;&#x443;&#x432;&#x430;&#x447;&#x438; &#x43d;&#x430; &#x416;&#x438;&#x442;&#x43e; &#x41c;&#x430;&#x440;&#x43a;&#x435;&#x442;&#x438;, &#x43e;&#x434;&#x43b;&#x438;&#x447;&#x43d;&#x438; &#x446;&#x435;&#x43d;&#x438; &#x437;&#x430; &#x441;&#x43e;&#x432;&#x440;&#x448;&#x435;&#x43d;&#x43e; &#x441;&#x43f;&#x438;&#x435;&#x45a;&#x435; :)

&#x41f;&#x440;&#x43e;&#x43c;&#x43e;&#x446;&#x438;&#x458;&#x430;&#x442;&#x430; &#x432;&#x430;&#x436;&#x438; &#x434;&#x43e; 15.01, &#x43f;&#x43e;&#x431;&#x440;&#x437;&#x430;&#x458;&#x442;&#x435; - &#x43a;&#x43e;&#x43b;&#x438;&#x447;&#x438;&#x43d;&#x438;&#x442;&#x435; &#x441;&#x435; &#x43e;&#x433;&#x440;&#x430;&#x43d;&#x438;&#x447;&#x435;&#x43d;&#x438;!</p>Posted by <a href="https://www.facebook.com/zito.marketi/">Жито Маркети</a> on&nbsp;<a href="https://www.facebook.com/zito.marketi/posts/920572038044546:0">Tuesday, January 3, 2017</a></blockquote></div>
         
                 <div class="fb-post" data-href="https://www.facebook.com/zito.marketi/posts/1036284419806640:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/zito.marketi/posts/1036284419806640:0" class="fb-xfbml-parse-ignore"><p>&#x421;&#x430;&#x43c;&#x43e; &#x432;&#x43e; &#x43f;&#x43e;&#x43d;&#x435;&#x434;&#x435;&#x43b;&#x43d;&#x438;&#x43a; (29.05.2017) &#x438; &#x441;&#x430;&#x43c;&#x43e; &#x432;&#x43e; &#x416;&#x438;&#x442;&#x43e; &#x41c;&#x430;&#x440;&#x43a;&#x435;&#x442;&#x438; &#x41d;&#x410;&#x41c;&#x410;&#x41b;&#x415;&#x41d;&#x410; &#x446;&#x435;&#x43d;&#x430; &#x43d;&#x430; &#x411;&#x438;&#x442;&#x43e;&#x43b;&#x441;&#x43a;&#x438; Light &#x458;&#x43e;&#x433;&#x443;&#x440;&#x442; 950&#x433;&#x440;. &#x43d;&#x430; 38 &#x434;&#x435;&#x43d;&#x430;&#x440;&#x438;!</p>Posted by <a href="https://www.facebook.com/zito.marketi/">Жито Маркети</a> on&nbsp;<a href="https://www.facebook.com/zito.marketi/posts/1036284419806640:0">Sunday, May 28, 2017</a></blockquote></div>
                 <div class="fb-post" data-href="https://www.facebook.com/zito.marketi/posts/878995938868823:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/zito.marketi/posts/878995938868823:0" class="fb-xfbml-parse-ignore"><p>&#x414;&#x43e; &#x43a;&#x440;&#x430;&#x458;&#x43e;&#x442; &#x43d;&#x430; &#x43e;&#x432;&#x430;&#x430; 2016-&#x442;&#x430; &#x433;&#x43e;&#x434;&#x438;&#x43d;&#x430; &#x43f;&#x43e;&#x434;&#x433;&#x43e;&#x442;&#x432;&#x438;&#x432;&#x43c;&#x435; &#x43d;&#x435;&#x448;&#x442;&#x43e; &#x437;&#x430; &#x441;&#x438;&#x442;&#x435; &#x459;&#x443;&#x431;&#x438;&#x442;&#x435;&#x43b;&#x438; &#x43d;&#x430; &#x43f;&#x438;&#x432;&#x43e;&#x442;&#x43e; :)

&#x417;&#x430; &#x43a;&#x43e;&#x435; &#x431;&#x438;&#x43b;&#x43e; &#x43a;&#x443;&#x43f;&#x435;&#x43d;&#x43e; &#x43f;&#x438;&#x432;&#x43e; (&#x43e;&#x434; &#x43a;&#x43e;&#x458; &#x431;&#x438;&#x43b;&#x43e; &#x431;...</p>Posted by <a href="https://www.facebook.com/zito.marketi/">Жито Маркети</a> on&nbsp;<a href="https://www.facebook.com/zito.marketi/posts/878995938868823:0">Tuesday, November 15, 2016</a></blockquote></div>
             </div>
             </div>
             <div class="insta-soc">
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:43.09623430962343% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BS86RmHFmO2/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Жито маркети (@zito.marketi)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-04-16T15:44:59+00:00">Apr 16, 2017 at 8:44am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:37.5% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BSTBBN_FMS5/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Жито маркети (@zito.marketi)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-03-31T09:15:49+00:00">Mar 31, 2017 at 2:15am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:62.5% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BUMbMK9Fe-N/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Жито маркети (@zito.marketi)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-05-17T12:52:36+00:00">May 17, 2017 at 5:52am PDT</time></p></div></blockquote>
               <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:33.28703703703704% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BVCpUy_hUXW/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Жито маркети (@zito.marketi)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-06-07T14:15:06+00:00">Jun 7, 2017 at 7:15am PDT</time></p></div></blockquote>
                 </div>
        </div>
        <!-- end of part3-->
        <div class="client2-sec4">
            <div class="client2-pr1">
                <div class="cut zito">
                    <h4>Facebook</h4>
                    </div>
                    <h3><span id='z' class='Count' data-num=112>10</span><span id='z'>k</span><span id='b'>Fans</span></h3>
                    <div class='cut zito'><h4>@lang('zito.print_title')</h4>
                    <p>@lang('zito.print')</p>
                    </div>
            </div>
            <div class="client2-pr4 client-zito" style='display:flex;'>
                <div class="clinet4-img">
                    <img src="{{url('/')}}/images/karticki.jpg" alt="bimilk1">
                     <img src="{{url('/')}}/images/poster.jpg" alt="bimilk3">
                </div>
                <div class="clinet4-img">
                    <img src="{{url('/')}}/images/flyers.jpg" alt="bimilk2">
                    <img src="{{url('/')}}/images/trifold.jpg" alt="bimilk3">
                    </div>
               
            </div>
        </div>
        <!-- end of part 4 -->
        <div class="client-wrap5">
            <!--<a class='up-client' ><img src='images/arrow-golema-nagore.png' alt="gore"></a>-->
            <a href='{{ route('cineplexx') }}'><div class='left-client next-cine'><span><img src="{{url('/')}}/images/arrow-golema-nalevo.png" alt="levo"></span><h2>Cineplexx</h2><p></p></div></a>
            <a href='{{ route('balans') }}'><div class='right-client next-balans'><span><img src="{{url('/')}}/images/arrow-golema-nadesno.png" alt="desno"></span><h2>Balans</h2><p></p></div></a>
        </div>

@endsection
@section('footer')
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
       <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=652953341561450";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection