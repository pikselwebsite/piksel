@extends('layouts.main')
@section('title')
    Balans |
@endsection
@section('metadata')

    <meta name="description" content="With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia." />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Balans">
    <meta itemprop="description" content="With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia.">
    <meta itemprop="image" content="{{url('/')}}/images/balans.png">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="balans_client_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="Balans">
    <meta name="twitter:description" content="With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia.">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{url('/')}}/images/balans.png">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="Balans" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{route('balans')}}" />
    <meta property="og:image" content="{{url('/')}}/images/balans.png" />
    <meta property="og:description" content="With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia.">
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')


        <div class="client-wrap ">
             <div class="arrows-clients">
            <a class='pull' href='{{ route('portfolio') }}'><span class="glyphicon glyphicon-arrow-left"></span>@lang('portfolio.prev')</a>
             <a class='pull-next' href='{{ route('linea') }}'> @lang('portfolio.next') &nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
             </div>
            <div class="client-sec1 balans">
                <h2>Balans Plus</h2>
                <div class="cl">
                    <div class='client-txt'>
                        <h5>@lang('balans.client_title')</h5>
                        <p>@lang('balans.client')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('balans.what_title')</h5>
                        <p>@lang('balans.what')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('balans.about_title')</h5>
                        <p>@lang('balans.about')</p>
                    </div>
                </div>
                <img class='img-responsive' src="{{url('/')}}/images/balans.png" alt="Balans">
            </div>
            </div>
            <!-- end of part 1 -->
<div class='client2-sec2 balans'>
    <h4 style='text-align:center;'>@lang('balans.responsive')</h4>
            <p class='after_img_text'>@lang('balans.responsive_text')</p>
    </div>
        <div class="client-wrap2">
            
            <div class="cl-part1">
                <img  class='img-responsive'src="{{url('/')}}/images/web1.jpg" alt="proekt1"> 
            </div>
           <div  class="cl-part2">
            <img class='img-responsive' src="{{url('/')}}/images/web2.jpg" alt="proekti2">
           </div>
        </div>
        <!-- end of part 2 -->
             <div class="client2-sec2 balans">
                <h4>@lang('balans.social_title')</h4>
                <div class="row soc-row">
                    <p >@lang('balans.social')</p>
                </div>
                <div class="cli-social-icons">
                    <a target="_blank" href='https://www.facebook.com/BalansPlus/'><img src="{{url('/')}}/images/fb-polno.png" alt="fb"></a>
                    <a target="_blank" href='https://twitter.com/BitolskoCupe'><img src="{{url('/')}}/images/tw polno.png" alt="twitter"></a>
                    <a target="_blank" href='https://plus.google.com/100914533980052021575'><img src="{{url('/')}}/images/goog polno.png" alt="googl"></a>
                    <a target="_blank" href='https://www.linkedin.com/company-beta/1540633/'><img src="{{url('/')}}/images/in polno.png" alt="insta"></a>
                </div>
                <div class="social-media-background">
                    <img src="{{url('/')}}/images/balans-fb.png" alt="balans">
                </div>
            </div>
            <div class="client2-sec3">
                  <div id="fb-soc">
                 <!-- fb posts -->
                 <div class="mockup-post">
                   <div class="fb-post" data-href="https://www.facebook.com/BalansPlus/posts/1333835913403051:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/BalansPlus/posts/1333835913403051:0" class="fb-xfbml-parse-ignore"><p>&#x421;&#x43c;&#x443;&#x442;&#x438;&#x442;&#x43e; &#x435; &#x441;&#x435;&#x43a;&#x43e;&#x433;&#x430;&#x448; &#x43f;&#x43e;&#x432;&#x43a;&#x443;&#x441;&#x43d;&#x43e; &#x43a;&#x43e;&#x433;&#x430; e &#x432;&#x43e; &#x43a;&#x43e;&#x43c;&#x431;&#x438;&#x43d;&#x430;&#x446;&#x438;&#x458;&#x430; &#x441;&#x43e; #&#x411;&#x430;&#x43b;&#x430;&#x43d;&#x441;&#x41f;&#x43b;&#x443;&#x441;  #&#x411;&#x430;&#x43b;&#x430;&#x43d;&#x441;&#x421;&#x43c;&#x443;&#x442;&#x438;&#x1f60a;</p>Posted by <a href="https://www.facebook.com/BalansPlus/">Balans +</a> on&nbsp;<a href="https://www.facebook.com/BalansPlus/posts/1333835913403051:0">Thursday, June 8, 2017</a></blockquote></div>
                 <div class="fb-post" data-href="https://www.facebook.com/BalansPlus/posts/1333838253402817:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/BalansPlus/posts/1333838253402817:0" class="fb-xfbml-parse-ignore"><p>#&#x411;&#x430;&#x43b;&#x430;&#x43d;&#x441;&#x421;&#x43c;&#x443;&#x442;&#x438; &#x440;&#x435;&#x446;&#x435;&#x43f;&#x442; &#x43d;&#x430; &#x434;&#x435;&#x43d;&#x43e;&#x442;:
&#x418;&#x437;&#x43b;&#x443;&#x43f;&#x435;&#x442;&#x435; &#x435;&#x434;&#x43d;&#x43e; &#x43a;&#x438;&#x432;&#x438; &#x438; &#x441;&#x442;&#x430;&#x432;&#x435;&#x442;&#x435; &#x433;&#x43e; &#x432;&#x43e; &#x431;&#x43b;&#x435;&#x43d;&#x434;&#x435;&#x440; &#x437;&#x430;&#x435;&#x434;&#x43d;&#x43e; &#x441;&#x43e; &#x448;&#x43e;&#x43b;&#x458;&#x430; #&#x411;&#x430;&#x43b;&#x430;&#x43d;&#x441;&#x41f;&#x43b;&#x443;&#x441;,  &#x43d;&#x435;&#x43a;&#x43e;&#x43b;&#x43a;&#x443; &#x437;&#x440;&#x43d;&#x430; &#x433;&#x440;&#x43e;...</p>Posted by <a href="https://www.facebook.com/BalansPlus/">Balans +</a> on&nbsp;<a href="https://www.facebook.com/BalansPlus/posts/1333838253402817:0">Monday, June 12, 2017</a></blockquote></div>
                  
                <div class="fb-post" data-href="https://www.facebook.com/BalansPlus/posts/1253839148069395:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/BalansPlus/posts/1253839148069395:0" class="fb-xfbml-parse-ignore"><p>&#x412;&#x435;&#x436;&#x431;&#x430;&#x43c;&#x435; &#x441;&#x43e; #&#x411;&#x430;&#x43b;&#x430;&#x43d;&#x441;&#x41f;&#x43b;&#x443;&#x441; :)</p>Posted by <a href="https://www.facebook.com/BalansPlus/">Balans +</a> on&nbsp;<a href="https://www.facebook.com/BalansPlus/posts/1253839148069395:0">Thursday, April 6, 2017</a></blockquote></div>
                 </div>
                 <div class="mockup-post">
             <div class="fb-post" data-href="https://www.facebook.com/BalansPlus/posts/1307824739337502:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/BalansPlus/posts/1307824739337502:0" class="fb-xfbml-parse-ignore"><p>&#x421;&#x435;&#x43a;&#x43e;&#x433;&#x430;&#x448; &#x435; &#x432;&#x438;&#x441;&#x442;&#x438;&#x43d;&#x441;&#x43a;&#x438;&#x43e;&#x442; &#x434;&#x435;&#x43d; &#x434;&#x430; &#x441;&#x435; &#x43f;&#x43e;&#x447;&#x43d;&#x435; &#x441;&#x43e; &#x437;&#x434;&#x440;&#x430;&#x432;&#x438; &#x43d;&#x430;&#x432;&#x438;&#x43a;&#x438; :) &#x41a;&#x43e;&#x458;&#x430; &#x435; &#x432;&#x430;&#x448;&#x430;&#x442;&#x430; &#x43e;&#x43c;&#x438;&#x43b;&#x435;&#x43d;&#x430; &#x444;&#x438;&#x437;&#x438;&#x447;&#x43a;&#x430; &#x430;&#x43a;&#x442;&#x438;&#x432;&#x43d;&#x43e;&#x441;&#x442;?

#BalansRun #BalansLife</p>Posted by <a href="https://www.facebook.com/BalansPlus/">Balans +</a> on&nbsp;<a href="https://www.facebook.com/BalansPlus/posts/1307824739337502:0">Wednesday, May 17, 2017</a></blockquote></div>
                  <div class="fb-post" data-href="https://www.facebook.com/BalansPlus/posts/1142809615839016:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/BalansPlus/posts/1142809615839016:0" class="fb-xfbml-parse-ignore"><p>&#x41f;&#x43e;&#x458;&#x430;&#x434;&#x43e;&#x43a;&#x43e;&#x442; &#x435; &#x43f;&#x43e;&#x441;&#x43b;&#x443;&#x436;&#x435;&#x43d;! :)

&#x421;&#x43e; #&#x411;&#x430;&#x43b;&#x430;&#x43d;&#x441;+ &#x43e;&#x441;&#x442;&#x430;&#x43d;&#x435;&#x442;&#x435; &#x432;&#x43e; &#x444;&#x43e;&#x440;&#x43c;&#x430; &#x438; &#x43e;&#x432;&#x430;&#x430; &#x437;&#x438;&#x43c;&#x430; :)</p>Posted by <a href="https://www.facebook.com/BalansPlus/">Balans +</a> on&nbsp;<a href="https://www.facebook.com/BalansPlus/posts/1142809615839016:0">Saturday, December 17, 2016</a></blockquote></div>
             </div>
            </div>
              <div class="insta-soc">
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:56.666666666666664% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BUjgg2NhAEd/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Bimilk (@bimilk.mk)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-05-26T12:01:39+00:00">May 26, 2017 at 5:01am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:28.14814814814815% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BSEjMkQBQCC/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Bimilk (@bimilk.mk)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-03-25T18:25:51+00:00">Mar 25, 2017 at 11:25am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:31.38888888888889% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BVpsPUmhUcu/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Bimilk (@bimilk.mk)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-06-22T18:10:57+00:00">Jun 22, 2017 at 11:10am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:36.01851851851852% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BV4_QE5hHoM/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Bimilk (@bimilk.mk)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-06-28T16:45:41+00:00">Jun 28, 2017 at 9:45am PDT</time></p></div></blockquote>
                 </div>
        </div>
        <!-- end of part5-->
        <!-- end of part3-->
        <!--<div class="client-wrap3 ">
            <div class="row">
                <div class="col-md-6 col-xs-6 bo">
                    <div class="blue-box balans-plus">
                        <h4>Some more info about the website features</h4>
                        <p>Lorem lpsum simply dummy text of the pinting and typesetting indsutry.Lorem lpsum has been the industry standart dummy text ever since the 1500s, when an unknown pinter took a galley of typ and scrambled it to make a type specimen book, it has survived not only five centureies, but also the lep into electronic typeseetting, emaining essentaily unchanged</p>
                        <a href='#'>SEE MORE IN CASE STUDY <span class='glyphicon glyphicon-arrow-right'></span></a>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6 bal"><img src="{{url('/')}}/images/website-piksel-novo.png" alt="web"><img class='img-responsive' src="{{url('/')}}/images/ikons.png" alt="detali"></div>
            </div>
        </div>-->
        <!-- end of part 4 -->
        <div class='client2-sec2 balans'>
    <h4 style='text-align:center;'>@lang('balans.jademe_title')</h4>
            <p class='after_img_text'>@lang('balans.jademe')</p>
    </div>
        <div class="client-wrap4 bal-back">
            <img class='img-responsive ' src="{{url('/')}}/images/balans-app.jpg" alt="balans">
        </div>
        <!-- end of part 6 -->
        <div class="client-wrap5">
            <a href='{{ route('zito') }}'><div class='left-client next-zito'><span><img src="{{url('/')}}/images/arrow-golema-nalevo.png" alt="levo"></span><h2>Zito Marketi</h2><p></p></div></a>
            <a href='{{ route('linea') }}'><div class='right-client next-linea'><span><img src="{{url('/')}}/images/arrow-golema-nadesno.png" alt="desno"></span><h2>Linea</h2><p></p></div></a>
        </div>


@endsection
@section('footer')
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
     </script>
       <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=652953341561450";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection