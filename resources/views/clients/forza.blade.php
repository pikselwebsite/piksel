@extends('layouts.main')
@section('title')
    Forza |
@endsection
@section('metadata')

    <meta name="description" content="Fast loan for fast solutions." />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Forza">
    <meta itemprop="description" content="Fast loan for fast solutions.">
    <meta itemprop="image" content="{{url('/')}}/images/forza-fb.png">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="forza_client_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="Forza">
    <meta name="twitter:description" content="Fast loan for fast solutions.">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{url('/')}}/images/forza-fb.png">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="Forza" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{route('forza')}}" />
    <meta property="og:image" content="{{url('/')}}/images/forza-fb.png" />
    <meta property="og:description" content="Fast loan for fast solutions.">
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')

 <div class="client-wrap ">
             <div class="arrows-clients">
            <a class='pull' href='{{ route('portfolio') }}'><span class="glyphicon glyphicon-arrow-left"></span>@lang('portfolio.prev')</a>
             <a class='pull-next' href='{{ route('bimilk') }}'>@lang('portfolio.next') &nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
             </div>
            <div class="client2-sec1 forza">
                <h2>@lang('forza.title')</h2>
                <div class="cl">
                    <div class='client-txt'>
                        <h5>@lang('forza.client_title')</h5>
                        <p>@lang('forza.client')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('forza.what_title')</h5>
                        <p>@lang('forza.what')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('forza.about_title')</h5>
                        <p>@lang('forza.about')</p>
                    </div>
                </div>
                <img class='img-responsive' src="{{url('/')}}/images/forza-fb.png" alt="Linea">
            </div>

            <!-- end of part 1 -->
            <div class="client2-sec2 forza">
                <h4>@lang('forza.social_title')</h4>
                <div class="row soc-row">
                    <p>@lang('forza.social')</p>
                </div>
                {{--<div class="cli-social-icons">--}}
                {{--<a target="_blank" href='https://www.facebook.com/forzamkd/'><img src="{{url('/')}}/images/fb-polno.png" alt="fb"></a>--}}
                {{--<a target="_blank" href='https://www.instagram.com/forza_mkd/'><img src="{{url('/')}}/images/insta polno.png" alt="insta"></a>--}}
                 {{--<a target="_blank" href='#'><img src="{{url('/')}}/images/tw polno.png" alt="twitter"></a>--}}
                 {{--<a target="_blank" href='https://www.youtube.com/channel/UC9mTX6kQRWlfWUCfWNSE84A'><img src="{{url('/')}}/images/youtube.png" alt="youtube"></a>--}}
                {{--</div>--}}
            </div>
            </div>
        <!-- end of part2-->
        <div class="client2-sec3">
                   <div id="fb-soc" class='forza-img'>
                    <img src="{{url('/')}}/images/s3.png" alt="s3">
                       <img src="{{url('/')}}/images/s2.png" alt="s2">              
                    <img src="{{url('/')}}/images/s4.png" alt="s4">
                     <img src="{{url('/')}}/images/forza-1.jpg" alt="s3">
                    <img src="{{url('/')}}/images/forza-2.jpg" alt="s1">
                      <img src="{{url('/')}}/images/forza-3.jpg" alt="s2">
                    <img src="{{url('/')}}/images/forza-4.jpg" alt="s4">
                    <!--<img src="{{url('/')}}/images/s1.png" alt="s1"> -->
                    <img src="{{url('/')}}/images/forza-5.png" alt="s4"> 
             </div>
         
        </div>
        <div class="client2-sec2 forza">
        <h4>@lang('forza.priracnik_naslov')</h4>
               <p class='after_img_text'>@lang('forza.priracnik')</p>
          </div>  
        <div class="client2-pr4 forza">
            <img class='img-resposive' src="{{url('/')}}/images/priracnik-za-web.png" alt="forza">
            </div>
        <!-- end of part3-->
        <div class="client2-sec4">
            <div class="client2-pr1">
                <div class="cut forza">
                    <h4>@lang('forza.media')</h4>
                    </div>
                    <div class='clients-images'>
                        <div class="client-background"></div>
                        <img class='img-responsive' src="{{url('/')}}/images/carousel-fb-forza.png" alt="forza">
                    </div>
            </div>
            <div class="client2-pr4">
                    <img class='img-responsive' src="{{url('/')}}/images/forza-baneri.png" alt="bimilk1">
            </div>
        </div>
        <!-- end of part 4 -->
        <div class="client-wrap5">
            <!--<a class='up-client' ><img src='images/arrow-golema-nagore.png' alt="gore"></a>-->
            <a href='{{ route('linea') }}'><div class='left-client next-linea'><span><img src="{{url('/')}}/images/arrow-golema-nalevo.png" alt="levo"></span><h2>Linea</h2><p></p></div></a>
            <a href='{{ route('bimilk') }}'><div class='right-client next-bimilk'><span><img src="{{url('/')}}/images/arrow-golema-nadesno.png" alt="desno"></span><h2>Bimilk</h2><p></p></div></a>
        </div>


@endsection
@section('footer')
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
       <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=652953341561450";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection