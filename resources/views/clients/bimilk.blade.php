@extends('layouts.main')
@section('title')
    Bimilk |
@endsection
@section('metadata')

    <meta name="description" content="With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia." />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Bimilk">
    <meta itemprop="description" content="With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia.">
    <meta itemprop="image" content="{{url('/')}}/images/bimilk-fb.png">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="bimilk_client_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="Bimilk">
    <meta name="twitter:description" content="With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia.">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{url('/')}}/images/bimilk-fb.png">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="Bimilk" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{route('bimilk')}}" />
    <meta property="og:image" content="{{url('/')}}/images/bimilk-fb.png" />
    <meta property="og:description" content="With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia.">
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')
 <div class="client-wrap ">
            <div class="arrows-clients">
            <a class='pull' href='{{ route('portfolio') }}'><span class="glyphicon glyphicon-arrow-left"></span>@lang('portfolio.prev')</a>
             <a class='pull-next' href='{{ route('cineplexx') }}'>@lang('portfolio.next') &nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
             </div>
            <div class="client2-sec1">
                <h2>@lang('bimilk.title')</h2>
                <div class="cl">
                    <div class='client-txt'>
                        <h5>@lang('bimilk.client_title')</h5>
                        <p>@lang('bimilk.client')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('bimilk.what_title')</h5>
                        <p>@lang('bimilk.what')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('bimilk.about_title')</h5>
                        <p>@lang('bimilk.about')</p>
                    </div>
                </div>
                <img class='img-responsive' src="{{url('/')}}/images/bimilk-fb.png" alt="Linea">
            </div>

            <!-- end of part 1 -->
            <div class="client2-sec2">
                <h4>@lang('bimilk.social_title')</h4>
                <div class="row soc-row">
                    <p>@lang('bimilk.social')</p>
                </div>
                <div class="cli-social-icons">
                <a target="_blank" href='https://www.facebook.com/Bimilk/'><img src="{{url('/')}}/images/fb-polno.png" alt="fb"></a>
                <a target="_blank" href='https://twitter.com/BitolskoCupe'><img src="{{url('/')}}/images/tw polno.png" alt="twitter"></a>
                <a target="_blank" href='https://plus.google.com/100914533980052021575'><img src="{{url('/')}}/images/goog polno.png" alt="googl"></a>
                <a target="_blank" href='https://www.instagram.com/bimilk.mk/'><img src="{{url('/')}}/images/insta polno.png" alt="insta"></a>
                </div>
            </div>
            </div>
        <!-- end of part2-->
        <div class="client2-sec3">
             <div id="fb-soc">
                 <!-- fb posts -->
                 <div class="mockup-post">
                   <div class="fb-post" data-href="https://www.facebook.com/Bimilk/posts/1351126068256147:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/Bimilk/posts/1351126068256147:0" class="fb-xfbml-parse-ignore"><p>&#x41e;&#x442;&#x441;&#x435;&#x433;&#x430; &#x43d;&#x430;&#x448;&#x430;&#x442;&#x430; &#x442;&#x440;&#x43f;&#x435;&#x437;&#x430; &#x434;&#x43e;&#x43f;&#x43e;&#x43b;&#x43d;&#x438;&#x442;&#x435;&#x43b;&#x43d;&#x43e; &#x45c;&#x435; &#x458;&#x430; &#x437;&#x431;&#x43e;&#x433;&#x430;&#x442;&#x443;&#x432;&#x430; &#x438; &#x43d;&#x43e;&#x432;&#x438;&#x43e;&#x442; &#x43f;&#x440;&#x43e;&#x438;&#x437;&#x432;&#x43e;&#x434; &#x2013; &#x411;&#x438;&#x442;&#x43e;&#x43b;&#x441;&#x43a;&#x438; &#x43f;&#x443;&#x442;&#x435;&#x440;, &#x441;&#x43a;&#x43e;&#x446;&#x43a;&#x430;&#x43d; &#x437;&#x430; &#x441;&#x435;&#x447;&#x438;&#x458; &#x432;&#x43a;&#x443;&#x441;!</p>Posted by <a href="https://www.facebook.com/Bimilk/">Bimilk</a> on&nbsp;<a href="https://www.facebook.com/Bimilk/posts/1351126068256147:0">Friday, June 9, 2017</a></blockquote></div>
                  <div class="fb-post" data-href="https://www.facebook.com/Bimilk/posts/1356812964354124:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/Bimilk/posts/1356812964354124:0" class="fb-xfbml-parse-ignore"><p>&#x411;&#x438;&#x442;&#x43e;&#x43b;&#x441;&#x43a;&#x430; &#x43f;&#x430;&#x432;&#x43b;&#x430;&#x43a;&#x430; - &#x437;&#x430; &#x443;&#x448;&#x442;&#x435; &#x43f;&#x43e;&#x432;&#x43a;&#x443;&#x441;&#x43d;&#x438; &#x43b;&#x435;&#x442;&#x43d;&#x438; &#x43f;&#x43e;&#x458;&#x430;&#x434;&#x43e;&#x446;&#x438; &#x1f60a;</p>Posted by <a href="https://www.facebook.com/Bimilk/">Bimilk</a> on&nbsp;<a href="https://www.facebook.com/Bimilk/posts/1356812964354124:0">Friday, June 23, 2017</a></blockquote></div>
                  <div class="fb-post" data-href="https://www.facebook.com/Bimilk/posts/1356805341021553:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/Bimilk/posts/1356805341021553:0" class="fb-xfbml-parse-ignore"><p>&#x414;&#x435;&#x43d;&#x435;&#x441; &#x43d;&#x430; &#x434;&#x43e;&#x43c;&#x430;&#x448;&#x43d;&#x43e;&#x442;&#x43e; &#x43c;&#x435;&#x43d;&#x438; - &#x432;&#x43a;&#x443;&#x441;&#x435;&#x43d; &#x440;&#x435;&#x446;&#x435;&#x43f;&#x442; &#x437;&#x430; &#x43e;&#x432;&#x43e;&#x448;&#x43d;&#x438; &#x43c;&#x430;&#x444;&#x438;&#x43d;&#x438; &#x441;&#x43e; &#x411;&#x438;&#x442;&#x43e;&#x43b;&#x441;&#x43a;&#x438; &#x43f;&#x443;&#x442;&#x435;&#x440; &#x1f36a;</p>Posted by <a href="https://www.facebook.com/Bimilk/">Bimilk</a> on&nbsp;<a href="https://www.facebook.com/Bimilk/posts/1356805341021553:0">Saturday, June 17, 2017</a></blockquote></div>                    
                 </div>
                 <div class="mockup-post mockup-right">
                  <div class="fb-post" data-href="https://www.facebook.com/Bimilk/posts/1334774449891309:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/Bimilk/posts/1334774449891309:0" class="fb-xfbml-parse-ignore"><p>&#x41c;&#x430;&#x43b;&#x43a;&#x443; &#x43f;&#x43e;&#x438;&#x43d;&#x430;&#x43a;&#x432;&#x430; &#x443;&#x442;&#x440;&#x438;&#x43d;&#x441;&#x43a;&#x430; #&#x411;&#x438;&#x43c;&#x438;&#x43b;&#x43a; &#x43a;&#x43e;&#x43c;&#x431;&#x438;&#x43d;&#x430;&#x446;&#x438;&#x458;&#x430; &#x1f60a;</p>Posted by <a href="https://www.facebook.com/Bimilk/">Bimilk</a> on&nbsp;<a href="https://www.facebook.com/Bimilk/posts/1334774449891309:0">Friday, May 26, 2017</a></blockquote></div>
                  <div class="fb-post" data-href="https://www.facebook.com/Bimilk/posts/1269476553087766:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/Bimilk/posts/1269476553087766:0" class="fb-xfbml-parse-ignore"><p>#&#x414;&#x430;&#x43b;&#x438;&#x421;&#x442;&#x435;&#x417;&#x43d;&#x430;&#x435;&#x43b;&#x435;&#x414;&#x435;&#x43a;&#x430; &#x441;&#x432;&#x435;&#x436;&#x43e;&#x442;&#x43e; &#x43c;&#x43b;&#x435;&#x43a;&#x43e; &#x438;&#x43c;&#x430; &#x43e;&#x434;&#x440;&#x435;&#x434;&#x435;&#x43d;&#x438; &#x445;&#x440;&#x430;&#x43d;&#x43b;&#x438;&#x432;&#x438; &#x432;&#x440;&#x435;&#x434;&#x43d;&#x43e;&#x441;&#x442;&#x438; &#x43a;&#x43e;&#x438; &#x45d; &#x43f;&#x43e;&#x43c;&#x430;&#x433;&#x430;&#x430;&#x442; &#x43d;&#x430; &#x43a;&#x43e;&#x436;&#x430;&#x442;&#x430; &#x434;&#x430; &#x441;&#x458;&#x430;&#x435;.

                        &#x41f;&#x43e;&#x432;&#x435;&#x45c;&#x435; &#x437;&#x430; &#x43f;&#x440;&#x438;&#x434;&#x43e;&#x431;&#x438;&#x432;&#x43a;&#x438;&#x442;&#x435; &#x43e;&#x434; &#x441;&#x432;&#x435;&#x436;&#x43e;&#x442;&#x43e; &#x43c;&#x43b;&#x435;&#x43a;&#x43e; &#x43c;&#x43e;&#x436;&#x435;&#x442;&#x435; &#x434;&#x430; &#x43d;&#x430;&#x458;&#x434;&#x435;&#x442;&#x435; &#x43d;&#x430; &#x441;&#x43b;&#x435;&#x434;&#x43d;&#x438;&#x43e;&#x442; &#x43b;&#x438;&#x43d;&#x43a; - http://bit.ly/2mIWWLF</p>Posted by <a href="https://www.facebook.com/Bimilk/">Bimilk</a> on&nbsp;<a href="https://www.facebook.com/Bimilk/posts/1269476553087766:0">Friday, March 10, 2017</a></blockquote></div>
                    <div class="fb-post" data-href="https://www.facebook.com/Bimilk/posts/1273746549327433:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/Bimilk/posts/1273746549327433:0" class="fb-xfbml-parse-ignore"><p>#&#x414;&#x430;&#x43b;&#x438;&#x417;&#x43d;&#x430;&#x435;&#x442;&#x435;&#x414;&#x435;&#x43a;&#x430; &#x421;&#x432;&#x435;&#x436;&#x43e;&#x442;&#x43e; &#x43c;&#x43b;&#x435;&#x43a;&#x43e; &#x435; &#x43e;&#x434;&#x43b;&#x438;&#x447;&#x435;&#x43d; &#x438;&#x437;&#x432;&#x43e;&#x440; &#x43d;&#x430; &#x43a;&#x430;&#x43b;&#x446;&#x438;&#x443;&#x43c;, &#x43a;&#x43e;&#x458; &#x435; &#x43e;&#x434; &#x441;&#x443;&#x448;&#x442;&#x438;&#x43d;&#x441;&#x43a;&#x43e; &#x437;&#x43d;&#x430;&#x447;&#x435;&#x45a;&#x435; &#x437;&#x430; &#x437;&#x434;&#x440;&#x430;&#x432;&#x438; &#x43a;&#x43e;&#x441;&#x43a;&#x438; &#x438; &#x432;&#x43e; &#x438;&#x441;&#x442;&#x43e; &#x432;&#x440;&#x435;&#x43c;&#x435; &#x435; &#x434;...</p>Posted by <a href="https://www.facebook.com/Bimilk/">Bimilk</a> on&nbsp;<a href="https://www.facebook.com/Bimilk/posts/1273746549327433:0">Wednesday, March 15, 2017</a></blockquote></div>
                 </div>
             </div>
             <div class="insta-soc">
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:41.62037037037037% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BUv4_UgBABz/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Bimilk (@bimilk.mk)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-05-31T07:26:25+00:00">May 31, 2017 at 12:26am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:33.33333333333333% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BT4Kcq-hXBP/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Bimilk (@bimilk.mk)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-05-09T16:01:30+00:00">May 9, 2017 at 9:01am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:41.08247422680412% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BTZKcF7hwyS/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Bimilk (@bimilk.mk)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-04-27T15:04:58+00:00">Apr 27, 2017 at 8:04am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BS4P0bbBVE6/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Bimilk (@bimilk.mk)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-04-14T20:17:02+00:00">Apr 14, 2017 at 1:17pm PDT</time></p></div></blockquote>
                 </div>
        </div>
        <!-- end of part3-->
        <div class="client2-sec4">
            <div class="client2-pr1">
                <div class="cut">
                    <h4>Facebook</h4>
                    </div>
                    <h3><span id='c' class='Count' data-num=92>40</span><span id='c'>k</span><span id='b'>Fans</span></h3>
                    <div class='cut'><h4>B.I</h4></div>
                    <p>@lang('bimilk.bi')</p>
            </div>
            <div class="client2-pr3">
                <div class="client-img4"><img src="{{url('/')}}/images/twitter.png" alt="twitt"></div>
            </div>
            <div class="clients-twitter">
                <blockquote class="twitter-video" data-lang="en"><p lang="ru" dir="ltr">Ве послушавме ;)  <a href="https://twitter.com/hashtag/bimilk?src=hash">#bimilk</a> <a href="https://twitter.com/hashtag/%D0%B1%D0%B8%D0%BC%D0%B8%D0%BB%D0%BA?src=hash">#бимилк</a> <a href="https://twitter.com/hashtag/%D0%B1%D0%B0%D0%BB%D0%B0%D0%BD%D1%81?src=hash">#баланс</a> <a href="https://twitter.com/hashtag/balans?src=hash">#balans</a> <a href="https://twitter.com/hashtag/%D0%B1%D0%B0%D0%BB%D0%B0%D0%BD%D1%81%D0%BF%D0%BB%D1%83%D1%81?src=hash">#балансплус</a> <a href="https://t.co/JJnGuzY9Lx">pic.twitter.com/JJnGuzY9Lx</a></p>&mdash; Битолско Чупе (@BitolskoCupe) <a href="https://twitter.com/BitolskoCupe/status/794187444082135040">November 3, 2016</a></blockquote>
            </div>
        </div>
        <!-- end of part 4 -->
        <div class="client-wrap5">
            <!--<a class='up-client' ><img src='images/arrow-golema-nagore.png' alt="gore"></a>-->
            <a href='{{ route('forza') }}'><div class='left-client next-forza'><span><img src="{{url('/')}}/images/arrow-golema-nalevo.png" alt="levo"></span><h2>Forza</h2><p></p></div></a>
            <a href='{{ route('cineplexx') }}'><div class='right-client next-cine'><span><img src="{{url('/')}}/images/arrow-golema-nadesno.png" alt="desno"></span><h2>Cineplexx</h2><p></p></div></a>
        </div>




@endsection
@section('footer')
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
     </script>
       <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=652953341561450";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection