@extends('layouts.main')
@section('title')
    Linea |
@endsection
@section('metadata')

    <meta name="description" content="Web site for construction company Lorem lpsum is simply dummy text of the pinting and typesetting industry." />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Linea">
    <meta itemprop="description" content="Web site for construction company Lorem lpsum is simply dummy text of the pinting and typesetting industry.">
    <meta itemprop="image" content="{{url('/')}}/images/linea.png">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="Linea">
    <meta name="twitter:description" content="Web site for construction company Lorem lpsum is simply dummy text of the pinting and typesetting industry.">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{url('/')}}/images/linea.png">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="Linea" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{route('linea')}}" />
    <meta property="og:image" content="{{url('/')}}/images/linea.png" />
    <meta property="og:description" content="Web site for construction company Lorem lpsum is simply dummy text of the pinting and typesetting industry.">
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')


        <div class="client-wrap ">
            <div class="arrows-clients">
            <a class='pull' href='{{ route('portfolio') }}'><span class="glyphicon glyphicon-arrow-left"></span>@lang('portfolio.prev')</a>
             <a class='pull-next' href='{{ route('forza') }}'>@lang('portfolio.next') &nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
             </div>
            <div class="client-sec1">
                <h2>@lang('linea.title')</h2>
                <div class="cl">
                    <div class='client-txt'>
                        <h5>@lang('linea.client_title')</h5>
                        <p>@lang('linea.client')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('linea.what_title')</h5>
                        <p>@lang('linea.what')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('linea.about_title')</h5>
                        <p>@lang('linea.about')</p>
                    </div>
                </div>
                <img class='img-responsive' src="{{url('/')}}/images/linea.png" alt="Linea">
                <p class='after_img_text'>@lang('linea.linea-tekst')</p>
            </div>

            <!-- end of part 1 -->
            <div class="client-sec2">
                <h4>@lang('linea.icons')</h4>
                <div class="row ic-row">
                    <div class='col-md-3 col-xs-3'><img class='img-responsive' src="{{url('/')}}/images/ic1.png" alt="ikoni"></div>
                    <div class='col-md-3 col-xs-3'><img class='img-responsive'  src="{{url('/')}}/images/ic2.png" alt="ikoni"></div>
                    <div class='col-md-3 col-xs-3'><img class='img-responsive'  src="{{url('/')}}/images/ic3.png" alt="ikoni"></div>
                    <div class='col-md-3 col-xs-3'><img class='img-responsive'  src="{{url('/')}}/images/ic4.png" alt="ikoni"></div>
                </div>
            </div>
        </div>
        <div class="client-wrap2">
            <div class="cl-part1">
                <img class='img-responsive'src="{{url('/')}}/images/2proekti.jpg" alt="proekt1"> 
            </div>
           <div class="cl-part2">
               <h4>@lang('linea.responsive')</h4>
            <img class='img-responsive' src="{{url('/')}}/images/2proekti.jpg" alt="proekti2">
           </div>
        </div>
        <!-- end of part 2 -->
        <div class="client-wrap3">
            <div class="row">
                <div class="col-md-6 col-xs-6 bo">
                    <div class="blue-box">
                        <h4>@lang('linea.case')</h4>
                        <p>@lang('linea.case_txt')</p>
                        <!--<a >SEE MORE IN CASE STUDY <span id='arr-right' class='glyphicon glyphicon-arrow-right'></span></a>-->
                    </div>
                </div>
                <div class="col-md-6 col-xs-6"><img class='img-responsive' src="{{url('/')}}/images/details.png" alt="detali"></div>
            </div>
        </div>
        <!-- end of part 3 -->
        <div class="client-wrap4">
            <img class='img-responsive' src="{{url('/')}}/images/mb1.png" alt="mob">
            <img class='img-responsive' src="{{url('/')}}/images/mb2.png" alt="mob">
            <img class='img-responsive' src="{{url('/')}}/images/mb3.png" alt="mob">
            
        </div>
        <!-- end of part 4 -->
        <div class="client-wrap5">
            <a href='{{ route('balans') }}'><div class='left-client next-balans'><span><img src="{{url('/')}}/images/arrow-golema-nalevo.png" alt="levo"></span><h2>Balans</h2><p></p></div></a>
            <a href='{{ route('forza') }}'><div class='right-client next-forza'><span><img src="{{url('/')}}/images/arrow-golema-nadesno.png" alt="desno"></span><h2>Forza</h2><p></p></div></a>
        </div>



@endsection
@section('footer')
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>

       <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=652953341561450";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection