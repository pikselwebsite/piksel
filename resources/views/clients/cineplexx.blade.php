@extends('layouts.main')
@section('title')
    Cineplexx |
@endsection
@section('metadata')

    <meta name="description" content="Cineplexx is a cinema company based in Austria. The company was founded in 1993, and operates mainly multiplex type cinemas. In Macedonia Cineplexx is present from year 2012." />
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Cineplexx">
    <meta itemprop="description" content="Cineplexx is a cinema company based in Austria. The company was founded in 1993, and operates mainly multiplex type cinemas. In Macedonia Cineplexx is present from year 2012.">
    <meta itemprop="image" content="{{url('/')}}/images/cineplexx-fb-1.png">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="cineplexx_client_page">
    <meta name="twitter:site" content="@pikselmk">
    <meta name="twitter:title" content="Cineplexx">
    <meta name="twitter:description" content="Cineplexx is a cinema company based in Austria. The company was founded in 1993, and operates mainly multiplex type cinemas. In Macedonia Cineplexx is present from year 2012.">
    <meta name="twitter:creator" content="@pikselmk">
    <meta name="twitter:image" content="{{url('/')}}/images/cineplexx-fb-1.png">

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="1370373606373353" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="Cineplexx" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{route('cineplexx')}}" />
    <meta property="og:image" content="{{url('/')}}/images/cineplexx-fb-1.png" />
    <meta property="og:description" content="Cineplexx is a cinema company based in Austria. The company was founded in 1993, and operates mainly multiplex type cinemas. In Macedonia Cineplexx is present from year 2012.">
    <meta property="og:site_name" content="Piksel LTD" />
@endsection
@section('content')


        <div class="client-wrap ">
            <div class="arrows-clients">
            <a class='pull' href='{{ route('portfolio') }}'><span class="glyphicon glyphicon-arrow-left"></span>@lang('portfolio.prev')</a>
             <a class='pull-next' href='{{ route('zito') }}'>@lang('portfolio.next') &nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
             </div>
            <div class="client2-sec1 cine">
                <h2>Cineplexx</h2>
                <div class="cl">
                    <div class='client-txt'>
                        <h5>@lang('cineplexx.client_title')</h5>
                        <p>@lang('cineplexx.client')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('cineplexx.what_title')</h5>
                        <p>@lang('cineplexx.what')</p>
                    </div>
                    <div class='client-txt'>
                        <h5>@lang('cineplexx.about_title')</h5>
                        <p>@lang('cineplexx.about')</p>
                    </div>
                </div>
                <img class='img-responsive' src="{{url('/')}}/images/cineplexx-fb-1.png" alt="Linea">
            </div>

            <!-- end of part 1 -->
            <div class="client2-sec2 cine">
                <h4>@lang('cineplexx.social_title')</h4>
                <div class="row soc-row">
                    <p>@lang('cineplexx.social')</p>
                </div>
                <div class="cli-social-icons">
                <a target="_blank" href='https://www.facebook.com/CineplexxMakedonija/'><img src="{{url('/')}}/images/fb-polno.png" alt="fb"></a>
                <a target="_blank" href='https://twitter.com/CineplexxMK'><img src="{{url('/')}}/images/tw polno.png" alt="twitter"></a>
                <a target="_blank" href='https://www.instagram.com/cineplexx_macedonia/'><img src="{{url('/')}}/images/insta polno.png" alt="insta"></a>
                <a target="_blank" href='https://www.youtube.com/user/CineplexxMK'><img src="{{url('/')}}/images/youtube.png" alt="youtube"></a>
                </div>
            </div>
            </div>
        <!-- end of part2-->
        <div class="client2-sec3">
             <div id="fb-soc">
                  <div class="mockup-post">
                        <div class="fb-post" data-href="https://www.facebook.com/CineplexxMakedonija/posts/1526306037400816:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/CineplexxMakedonija/posts/1526306037400816:0" class="fb-xfbml-parse-ignore"><p>Cineplexx &#x444;&#x430;&#x43a;&#x442; #10: 55 &#x440;&#x43e;&#x434;&#x435;&#x43d;&#x434;&#x435;&#x43d; &#x43d;&#x430; &#x421;&#x43f;&#x430;&#x458;&#x434;&#x438;!</p>Posted by <a href="https://www.facebook.com/CineplexxMakedonija/">Cineplexx MK</a> on&nbsp;<a href="https://www.facebook.com/CineplexxMakedonija/posts/1526306037400816:0">Monday, July 3, 2017</a></blockquote></div>
                        <div class="fb-post" data-href="https://www.facebook.com/CineplexxMakedonija/posts/1521700894527997:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/CineplexxMakedonija/posts/1521700894527997:0" class="fb-xfbml-parse-ignore"><p>&#x410;&#x43a;&#x442;&#x435;&#x440;&#x43e;&#x442; Mark Wahlberg &#x435; &#x434;&#x435;&#x43b; &#x43e;&#x434; &#x422;&#x440;&#x430;&#x43d;&#x444;&#x43e;&#x440;&#x43c;&#x435;&#x440;&#x438; &#x432;&#x43e; ____ &#x43f;&#x440;&#x43e;&#x434;&#x43e;&#x43b;&#x436;&#x435;&#x43d;&#x438;&#x458;&#x430;?</p>Posted by <a href="https://www.facebook.com/CineplexxMakedonija/">Cineplexx MK</a> on&nbsp;<a href="https://www.facebook.com/CineplexxMakedonija/posts/1521700894527997:0">Saturday, July 1, 2017</a></blockquote></div>
                  </div>
                   <div class="mockup-post">
                    <div class="fb-post" data-href="https://www.facebook.com/CineplexxMakedonija/posts/1518903131474440:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/CineplexxMakedonija/posts/1518903131474440:0" class="fb-xfbml-parse-ignore"><p>Cineplexx &#x444;&#x430;&#x43a;&#x442; #8 - &#x41a;&#x443;&#x431;&#x430; &#x441;&#x442;&#x430;&#x43d;&#x443;&#x432;&#x430; &#x43e;&#x43c;&#x438;&#x43b;&#x435;&#x43d;&#x43e; &#x43c;&#x435;&#x441;&#x442;&#x43e; &#x437;&#x430; &#x441;&#x43d;&#x438;&#x43c;&#x430;&#x45a;&#x435; &#x445;&#x43e;&#x43b;&#x438;&#x432;&#x443;&#x434;&#x441;&#x43a;&#x438; &#x431;&#x43b;&#x43e;&#x43a;&#x431;&#x430;&#x441;&#x442;&#x435;&#x440;&#x438;</p>Posted by <a href="https://www.facebook.com/CineplexxMakedonija/">Cineplexx MK</a> on&nbsp;<a href="https://www.facebook.com/CineplexxMakedonija/posts/1518903131474440:0">Tuesday, June 27, 2017</a></blockquote></div>
                    <div class="fb-post" data-href="https://www.facebook.com/CineplexxMakedonija/posts/1513339698697450:0" data-width="100%" data-show-text="false"><blockquote cite="https://www.facebook.com/CineplexxMakedonija/posts/1513339698697450:0" class="fb-xfbml-parse-ignore"><p>&#x41a;&#x43e;&#x458; &#x435; &#x442;&#x432;&#x43e;&#x458;&#x43e;&#x442; &#x43e;&#x43c;&#x438;&#x43b;&#x435;&#x43d; &#x436;&#x435;&#x43d;&#x441;&#x43a;&#x438; &#x43b;&#x438;&#x43a; &#x43e;&#x434; &#x434;&#x43e;&#x441;&#x435;&#x433;&#x430;&#x448;&#x43d;&#x438;&#x442;&#x435; #&#x422;&#x440;&#x430;&#x43d;&#x441;&#x444;&#x43e;&#x440;&#x43c;&#x435;&#x440;&#x438;?</p>Posted by <a href="https://www.facebook.com/CineplexxMakedonija/">Cineplexx MK</a> on&nbsp;<a href="https://www.facebook.com/CineplexxMakedonija/posts/1513339698697450:0">Thursday, June 22, 2017</a></blockquote></div>
                  </div>
             </div>
             <div class="insta-soc">
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BWIKFtkFgbx/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Cineplexx Macedonia (@cineplexx_macedonia)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-07-04T14:09:00+00:00">Jul 4, 2017 at 7:09am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:36.041666666666664% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BVplrF7lOcp/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Cineplexx Macedonia (@cineplexx_macedonia)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-06-22T17:13:35+00:00">Jun 22, 2017 at 10:13am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:33.28703703703704% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BUgvh3JgNFh/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Cineplexx Macedonia (@cineplexx_macedonia)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-05-25T10:15:08+00:00">May 25, 2017 at 3:15am PDT</time></p></div></blockquote>
                <blockquote class="instagram-media" data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:358px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/BVUe-2oAEIJ/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by Cineplexx Macedonia (@cineplexx_macedonia)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-06-14T12:31:03+00:00">Jun 14, 2017 at 5:31am PDT</time></p></div></blockquote>
                 </div>
            <div class="twitter-soc">
                <blockquote class="twitter-tweet" data-lang="en"><p lang="sr" dir="ltr">Одбројувањето може да започне! <a href="https://t.co/0LpFzULGP6">pic.twitter.com/0LpFzULGP6</a></p>&mdash; Cineplexx MK (@CineplexxMK) <a href="https://twitter.com/CineplexxMK/status/880439301254807553">June 29, 2017</a></blockquote>
                <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Hellо Friday :D <a href="https://t.co/Pq7mgibhh1">pic.twitter.com/Pq7mgibhh1</a></p>&mdash; Cineplexx MK (@CineplexxMK) <a href="https://twitter.com/CineplexxMK/status/865525380081360897">May 19, 2017</a></blockquote>
                <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">They are coming! 👽 <a href="https://t.co/JTfWDDlaeh">pic.twitter.com/JTfWDDlaeh</a></p>&mdash; Cineplexx MK (@CineplexxMK) <a href="https://twitter.com/CineplexxMK/status/863047953514192897">May 12, 2017</a></blockquote>       
                <blockquote class="twitter-tweet" data-lang="en"><p lang="bg" dir="ltr">Подготвени за галактичката авантура <a href="https://twitter.com/hashtag/%D0%A7%D1%83%D0%B2%D0%B0%D1%80%D0%B8%D0%9D%D0%B0%D0%93%D0%B0%D0%BB%D0%B0%D0%BA%D1%81%D0%B8%D1%98%D0%B0%D1%82%D0%B0%D0%94%D0%B5%D0%BB2?src=hash">#ЧувариНаГалаксијатаДел2</a>?😀🍿<br>Се гледаме и дружиме денес во 18:30 во <a href="https://twitter.com/hashtag/Cineplexx?src=hash">#Cineplexx</a>!<a href="https://t.co/5UtycTKqTP">https://t.co/5UtycTKqTP</a> <a href="https://t.co/qbollQq8ga">pic.twitter.com/qbollQq8ga</a></p>&mdash; Cineplexx MK (@CineplexxMK) <a href="https://twitter.com/CineplexxMK/status/859724805507084288">May 3, 2017</a></blockquote>
            </div>
        </div>
        <!-- end of part3-->
        <div class="client2-sec4">
            <div class="client2-pr1">
                <div class="cut cine">
                    <h4>Facebook</h4>
                    </div>
                    <h3><span id='cine' class='Count' data-num=82>0</span><span id='cine'>K</span><span id='b'>Fans</span></h3>
            </div>
        </div>
        <!-- end of part 4 -->
        <div class="client-wrap5">
            <!--<a class='up-client' ><img src='images/arrow-golema-nagore.png' alt="gore"></a>-->
            <a href='{{ route('bimilk') }}'><div class='left-client next-bimilk'><span><img src="{{url('/')}}/images/arrow-golema-nalevo.png" alt="levo"></span><h2>Bimilk</h2><p></p></div></a>
            <a href='{{ route('zito') }}'><div class='right-client next-zito'><span><img src="{{url('/')}}/images/arrow-golema-nadesno.png" alt="desno"></span><h2>Zito Marketi</h2><p></p></div></a>
        </div>

@endsection
@section('footer')
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>

       <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=652953341561450";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endsection