@extends('layouts.app')


@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit Blog</span>
                    </div>

                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{ Form::model('newblog', array('route' => array('blog.update', $blog->id), 'method' => 'PUT', 'files'=>true)) }}

                            <div class="form-group">
                                <label for="title">Blog name</label>
                                <input type="text" name="name" class="form-control" value="{{$blog->name}}">
                            </div>
                            @if ($errors->has('name')) <p
                                    class="alert alert-danger">{{ $errors->first('name') }}</p> @endif

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="ckeditor" id="elm3" name="description">{!! $blog->description !!}</textarea>
                            </div>
                            @if ($errors->has('description')) <p
                                    class="alert alert-danger">{{ $errors->first('description') }}</p> @endif

                        <!-- Hidden inputs -->

                            {{--<input type="hidden" name="user_id" value="{{ Auth::user()->id  }}">--}}

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Save
                            </button>

                            <!-- Button trigger modal -->
                            <button data-link="{{url('/')}}/admin/blog/{{$blog->id}}" type="button" class="btn btn-labeled shiny btn-warning btn-large deleteSlideModal" data-toggle="modal" data-target="#deleteSlide">
                                <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                            </button>

                            <a href="/admin/blog" class="btn btn-labeled shiny btn-warning btn-large">
                                <i class="btn-label fa fa-chevron-left" aria-hidden="true"></i> Back
                            </a>


                            {!! Form::close() !!}


                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="deleteSlide" tabindex="-1" role="dialog" aria-labelledby="deleteSlide">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="deleteSlideLabel">Are you sure you want to delete this blog?</h4>
                </div>
                <div class="modal-body">
                    {{ Form::model('newblog', array('route' => array('blog.destroy', $blog->id), 'method' => 'DELETE', 'id' => 'blog'))}}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">



                        <button  type="submit" class="btn btn-labeled shiny btn-warning btn-large">
                            <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                        </button>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>


@endsection