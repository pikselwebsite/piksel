@extends('layouts.app')


@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Blog Post</span>
                    </div>


                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{ Form::model('BlogPost', array('route' => array('admin.blogpost.create'), 'method' => 'POST', 'files'=>true,)) }}


                            <div class="input-group{{ $errors->has('image') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse image... <input type="file"
                                                                                                  name="image">
								</span>
							</span>
                                <input type="text" class="form-control" readonly="" value="{{old('image')}}">
                            </div>
                            <br/>
                            @if ($errors->has('image')) <p
                                    class="alert alert-danger">{{ $errors->first('image') }}</p> @endif


                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" value="{{old('title')}}">
                            </div>
                            @if ($errors->has('title')) <p
                                    class="alert alert-danger">{{ $errors->first('title') }}</p> @endif

                            <div class="form-group">
                                <label for="description">Content</label>
                                <textarea class="ckeditor" id="elm3" name="content">{!! old('content') !!}</textarea>
                            </div>
                            @if ($errors->has('content')) <p
                                    class="alert alert-danger">{{ $errors->first('content') }}</p> @endif

                            <div class="form-group">
                                <label for="excerpt"> Excerpt</label>
                                <textarea type="text" name="excerpt" class="form-control">{!! old('excerpt') !!}</textarea>
                            </div>
                            @if ($errors->has('excerpt')) <p
                                    class="alert alert-danger">{{ $errors->first('excerpt') }}</p> @endif

                            @if ($errors->has('tag')) <p
                                    class="alert alert-danger">{{ $errors->first('tag') }}</p> @endif

                            <div class="form-group">
                                <select name='category' >
                                    <option selected="true" disabled="disabled">Select Category</option>
                                    @foreach($categories as $category)
                                    <option title='{{$category->name}}' value='{{$category->id}}'>{{$category->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                            @if ($errors->has('category')) <p
                                    class="alert alert-danger">{{ $errors->first('category') }}</p> @endif


                        <!-- Hidden inputs -->

                            <input type="hidden" name="user_id" value="{{ Auth::user()->id  }}">
                            <input type="hidden" name="blog_id" value="{{$blog->id}}">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Save
                            </button>

                            {!! Form::close() !!}


                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection