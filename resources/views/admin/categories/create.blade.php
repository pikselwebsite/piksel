@extends('layouts.app')

@section('scripts')

@endsection

@section('content')

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                @if($status)
                    {{ Form::model($categories, array('route' => array('categories.store'), 'method' => 'POST', 'files'=>true)) }}
                    {!! csrf_field() !!}
                    <legend>Category</legend>
                    <div class="form-group">
                        <label for="name">Category name</label>
                        <input type="text" class="form-control" id="categoryname" name="name" placeholder="Enter category name">
                    </div>
                    @if ($errors->has('name')) <p class="alert alert-danger">{{ $errors->first('name') }}</p> @endif

                    <div class="form-group">
                        <label for="parent_id">Sub category</label>

                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="null">Main</option>

                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">@for ($i = 0; $i < $category->depth; $i++) - @endfor {{ $category->name }}</option>
                            @endforeach

                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                    {!! Form::close() !!}
                @else

                    {{ Form::model('categories', array('route' => array('categories.store'), 'method' => 'POST', 'files'=>true)) }}
                    {!! csrf_field() !!}
                    <legend>Main Category</legend>
                    <div class="form-group">
                        <label for="name">Main Category</label>
                        <input type="text" class="form-control" id="categoryname" name="name" placeholder="Main Category">
                    </div>
                    @if ($errors->has('name')) <p class="alert alert-danger">{{ $errors->first('name') }}</p> @endif

                    <input type="hidden" name="parent_id" value="null">
                    <button type="submit" class="btn btn-default">Submit</button>
                    {!! Form::close() !!}

                @endif
            </div>
        </div>
    </div>
@endsection