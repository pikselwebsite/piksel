@extends('layouts.app')

@section('content')

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <legend>Edit category: {{  $category->name }}</legend>

                <div style="margin-bottom: 50px"></div>
                {{ Form::model($categories, array('route' => array('categories.update', $category->id), 'method' => 'PUT','files' => true)) }}

                <div class="form-group">
                    <label for="categoryname">Genre title</label>
                    <input type="text" class="form-control" id="categoryname" name="name"
                           placeholder="Enter category name" value="{{  $category->name }}">
                </div>
                @if ($errors->has('name')) <p class="alert alert-danger">{{ $errors->first('name') }}</p> @endif



                <button type="submit" class="btn btn-default">Update</button>
                {!! Form::close() !!}




                <hr/>

                {{ Form::model('category', array('route' => array('categories.destroy', $category->id), 'method' => 'DELETE', 'id' => 'category'))}}

                <button type="submit" class="btn btn-labeled shiny btn-danger delete"><i
                            class="btn-label fa fa-trash"></i> Delete
                </button>
                {{ Form::close() }}


            </div>
        </div>



    </div>



@endsection
