@extends('layouts.app')


@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit members</span>
                    </div>
                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{ Form::model('team', array('route' => array('team.update',$member->id), 'method' => 'PUT', 'files'=>true)) }}


                            <div class="form-group">
                                <label for="title">Name</label>
                                <input type="text" name="title" class="form-control" value="{{$member->title}}">
                            </div>
                            @if ($errors->has('title')) <p
                                    class="alert alert-danger">{{ $errors->first('title') }}</p> @endif


                            <div class="input-group">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Main image... <input readonly type="file"
                                                                                                name="main_image">
								</span>
							</span>
                                <input type="text" class="form-control" value="{{$member->main_image}}">
                            </div>
                            <br/>
                            @if ($errors->has('main_image')) <p
                                    class="alert alert-danger">{{ $errors->first('main_image') }}</p> @endif

                            <div class="input-group">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Hover image... <input readonly type="file"
                                                                                                 name="hover_image" >
								</span>
							</span>
                                <input type="text" class="form-control" value="{{$member->hover_image}}">
                            </div>
                            <br/>
                            @if ($errors->has('hover_image')) <p
                                    class="alert alert-danger">{{ $errors->first('hover_image') }}</p> @endif

                        <!-- Hidden inputs -->
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Save
                            </button>


                            <a href="/admin/team"  class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-chevron-left"></i> Back
                            </a>

                            {!! Form::close() !!}




                        </div>

                    </div>

                </div>




                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Members</span>
                    </div>


                    <div class="widget-body">

                        <table class="table table-hover table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="20">
                            <thead class="bordered-blueberry">
                            <tr>
                                <th width="25%">Name</th>
                                <th width="25%">Image</th>
                                <th width="25%">Hover Image</th>
                                <th width="25%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($members))
                                @foreach($members as $member)
                                    <tr align="center" valign="middle">
                                        <td width="25%">{{$member->title}}</td>
                                        <td width="25%"><img src="/assets/img/team-images/{{$member->main_image}}" width="50px" height="auto" style="margin: auto;"/> </td>
                                        <td width="25%"><img src="/assets/img/team-images/{{$member->hover_image}}" width="50px" height="auto" style="margin: auto;"/> </td>
                                        <td width="25%">
                                            <span style="float: right;">
                                                <a type="button" class="manageSlideBtn" id="{{$member->id}}" href="/admin/team/{{$member->id}}/edit/">
                                                    Edit
                                                </a>

                                                <!-- Button trigger modal -->
                                                <button data-link="{{url('/')}}/admin/team/{{$member->id}}" type="button" class="manageSlideBtn deleteMemberModal" data-toggle="modal" data-target="#deleteMember">
                                                    Delete
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif



                            </tbody>
                        </table>

                    </div>

                </div>








            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="deleteMember" tabindex="-1" role="dialog" aria-labelledby="deleteMember">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="deleteMemberLabel">Are you sure you want to delete this member?</h4>
                </div>
                <div class="modal-body">
                    <form id="memberDelForm" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input name="_method" value="DELETE" type="hidden">



                        <button  type="submit" class="btn btn-labeled shiny btn-warning btn-large">
                            <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                        </button>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection