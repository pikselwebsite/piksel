@extends('layouts.app')


@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Slider</span>
                    </div>


                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{ Form::model('slide', array('route' => array('admin.slider.create'), 'method' => 'POST', 'files'=>true)) }}


                            <div class="form-group">
                                <label>Language: </label>
                                <label>
                                    <input name="language" class="form-control colored-palegreen" value="en" checked="" type="radio">
                                    <span class="text"> English</span>
                                </label>
                                <label>
                                    <input name="language" class="form-control colored-palegreen" value="mk" type="radio">
                                    <span class="text"> Macedonian</span>
                                </label>


                            </div>


                            <div class="input-group{{ $errors->has('image') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse image... <input type="file"
                                                                                                  name="image">
								</span>
							</span>
                                <input type="text" class="form-control" readonly="">
                            </div>
                            <br/>
                            @if ($errors->has('image')) <p
                                    class="alert alert-danger">{{ $errors->first('image') }}</p> @endif


                            <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" class="form-control" value="{{old('title')}}">
                                </div>
                                @if ($errors->has('title')) <p
                                        class="alert alert-danger">{{ $errors->first('title') }}</p> @endif

                            <div class="form-group">
                                <label for="client">Client</label>
                                <input type="text" name="client" class="form-control" value="{{old('client')}}">
                            </div>
                            @if ($errors->has('client')) <p
                            class="alert alert-danger">{{ $errors->first('client') }}</p> @endif


                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="ckeditor" id="elm3" name="description">{!! old('description') !!}</textarea>
                            </div>
                            @if ($errors->has('description')) <p
                                    class="alert alert-danger">{{ $errors->first('description') }}</p> @endif

                            <div class="form-group">
                                <label for="weblink">Web site</label>
                                <input type="text" name="weblink" class="form-control" value="{{old('weblink')}}">
                            </div>
                            @if ($errors->has('weblink')) <p
                            class="alert alert-danger">{{ $errors->first('weblink') }}</p> @endif


                        <!-- Hidden inputs -->

                            <input type="hidden" name="user_id" value="{{ Auth::user()->id  }}">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Save
                            </button>


                            {!! Form::close() !!}


                        </div>

                    </div>

                </div>


                    <div class="widget">
                        <div class="widget-header bordered-bottom bordered-warning">
                            <span class="widget-caption">Slides</span>
                        </div>


                        <div class="widget-body">

                            <table class="table table-hover table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="20">
                                <thead class="bordered-blueberry">
                                <tr>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Client</th>
                                    <th>Description</th>
                                    <th>Link</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($slides))
                                    @foreach($slides as $slide)
                                        <tr align="center" valign="middle">
                                            <td width="14%"><img src="/assets/img/pikselslider/{{$slide->image}}" width="100%" height="auto"/> </td>
                                            <td width="20%">{{$slide->title}}</td>
                                            <td width="12%">{{$slide->client}}</td>
                                            <td width="27%">{!!$slide->description!!}</td>
                                            <td width="17%">{{$slide->weblink}}</td>
                                            <td width="10%">
                                                <a type="button" class="manageSlideBtn" id="{{$slide->id}}" href="{{route('admin.slider.edit', $slide->id)}}">
                                                    Edit
                                                </a>

                                                <!-- Button trigger modal -->
                                                <button data-link="{{route('admin.slider.delete', $slide->id)}}" type="button" class="manageSlideBtn deleteSlideModal" data-toggle="modal" data-target="#deleteSlide">
                                                    Delete
                                                </button>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif



                                </tbody>
                            </table>

                        </div>

                    </div>



























            </div>
        </div>
    </div>




    <!-- Modal -->
    <div class="modal fade" id="deleteSlide" tabindex="-1" role="dialog" aria-labelledby="deleteSlide">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="deleteSlideLabel">Are you sure you want to delete this slide?</h4>
                </div>
                <div class="modal-body">
                    <form id="SlideDelForm" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">



                        <button  type="submit" class="btn btn-labeled shiny btn-warning btn-large">
                            <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span>Delete
                        </button>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection