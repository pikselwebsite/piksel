@extends('layouts.app')


@section('content')

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <p><a class="btn btn-labeled shiny btn-warning btn-large" href="/admin/clients/create"> <i
                                class="btn-label fa fa-plus"></i>Add Client </a></p>
            </div>


            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-themesecondary">
                        <i class="widget-icon fa fa-tags themesecondary"></i>
                        <span class="widget-caption themesecondary">Clients</span>
                    </div><!--Widget Header-->
                    <div class="widget-body  no-padding">
                        <div class="tickets-container">

                            @if($clients->isEmpty())
                                <p>There aren't any clients yet please add <a href="/admin/clients/create"> new</a>.</p>
                            @else

                                <table class="table table-hover table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="20">
                                    <thead class="bordered-blueberry">
                                    <tr>
                                        <th>Title</th>
                                        <th>Name</th>
                                        <th>Manage</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @foreach($clients as $client)
                                        <tr  valign="middle">
                                            <td>{{$client->title}}</td>
                                            <td>{{$client->client}}</td>
                                            <td>
                                                    <span style="float: right;">
                                                    <a type="button" class="manageSlideBtn" id="{{$client->id}}" href="/admin/clients/{{$client->id}}/edit">
                                                        Edit
                                                    </a>
                                                        {{ Form::model('client', array('route' => array('clients.destroy', $client->id), 'method' => 'DELETE', 'id' => 'client','class'=>'inlineForm'))}}
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <button  type="submit" class="manageSlideBtn">
                                                                Delete
                                                            </button>
                                                        {{ Form::close() }}

                                                        </span>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



@endsection
