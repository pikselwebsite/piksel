@extends('layouts.app')


@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit Client</span>
                    </div>

                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{ Form::model('client', array('route' => array('clients.update', $client->id), 'method' => 'PUT', 'files'=>true)) }}

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" value="{{$client->title}}">
                            </div>
                            @if ($errors->has('title')) <p
                                    class="alert alert-danger">{{ $errors->first('title') }}</p> @endif

                            <div class="form-group">
                                <label for="client">Client</label>
                                <input type="text" name="client" class="form-control" value="{{$client->client}}">
                            </div>
                            @if ($errors->has('client')) <p
                                    class="alert alert-danger">{{ $errors->first('client') }}</p> @endif

                            <div class="form-group">
                                <label for="what_we_did">What we did?</label>
                                <input type="text" name="what_we_did" class="form-control" value="{{$client->what_we_did}}">
                            </div>
                            @if ($errors->has('what_we_did')) <p
                                    class="alert alert-danger">{{ $errors->first('what_we_did') }}</p> @endif


                            <div class="form-group">
                                <label for="project">Project Info</label>
                                <textarea class="ckeditor" id="elm3" name="project">{!! $client->project !!}</textarea>
                            </div>
                            @if ($errors->has('project')) <p
                                    class="alert alert-danger">{{ $errors->first('project') }}</p> @endif

                            <div class="input-group{{ $errors->has('logo') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse Logo image... <input type="file"
                                                                                                       name="logo">
								</span>
							</span>
                                <input type="text" class="form-control" readonly="">
                            </div>
                            <br/>
                            @if ($errors->has('logo')) <p
                                    class="alert alert-danger">{{ $errors->first('logo') }}</p> @endif


                            <div class="input-group{{ $errors->has('mainimage') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse main image... <input type="file"
                                                                                                       name="mainimage">
								</span>
							</span>
                                <input value="{{$client->mainimage}}" type="text" class="form-control" readonly="">
                            </div>
                            <br/>
                            @if ($errors->has('mainimage')) <p
                                    class="alert alert-danger">{{ $errors->first('mainimage') }}</p> @endif


                            <div class="form-group">
                                <label for="custom_field">Custom field</label>
                                <input type="text" name="custom_field" class="form-control" value="{{$client->custom_field}}">
                            </div>
                            @if ($errors->has('custom_field')) <p
                                    class="alert alert-danger">{{ $errors->first('custom_field') }}</p> @endif

                            <p>Custom icons</p><hr>

                            <div class="col-md-6 col-sm-12">
                                <div class="input-group{{ $errors->has('icon_1') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse icon... <input type="file"
                                                                                                 name="icon_1">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->icon_1}}">
                                </div>
                                <br/>
                                @if ($errors->has('icon_1')) <p
                                        class="alert alert-danger">{{ $errors->first('icon_1') }}</p> @endif
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <div class="input-group{{ $errors->has('icon_2') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse icon... <input type="file"
                                                                                                 name="icon_2">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->icon_2}}">
                                </div>
                                <br/>
                                @if ($errors->has('icon_2')) <p
                                        class="alert alert-danger">{{ $errors->first('icon_2') }}</p> @endif
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-6 col-sm-12">
                                <div class="input-group{{ $errors->has('icon_3') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse icon... <input type="file"
                                                                                                 name="icon_3">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->icon_3}}">
                                </div>
                                <br/>
                                @if ($errors->has('icon_3')) <p
                                        class="alert alert-danger">{{ $errors->first('icon_3') }}</p> @endif
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <div class="input-group{{ $errors->has('icon_4') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse icon... <input type="file"
                                                                                                 name="icon_4">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->icon_4}}">
                                </div>
                                <br/>
                                @if ($errors->has('icon_4')) <p
                                        class="alert alert-danger">{{ $errors->first('icon_4') }}</p> @endif
                            </div>

                            <div class="form-group">
                                <label for="custom_field_2">Second Custom Field</label>
                                <input type="text" name="custom_field_2" class="form-control" value="{{$client->custom_field_2}}">
                            </div>
                            @if ($errors->has('custom_field_2')) <p
                                    class="alert alert-danger">{{ $errors->first('custom_field_2') }}</p> @endif

                            <p>Desktop images</p><hr>

                            <div class="col-md-6 col-sm-12">
                                <div class="input-group{{ $errors->has('desktopimg_1') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse Desktop 1... <input type="file"
                                                                                                      name="desktopimg_1">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->desktopimg_1}}">
                                </div>
                                <br/>
                                @if ($errors->has('desktopimg_1')) <p
                                        class="alert alert-danger">{{ $errors->first('desktopimg_1') }}</p> @endif
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <div class="input-group{{ $errors->has('desktopimg_2') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse Desktop 2... <input type="file"
                                                                                                      name="desktopimg_2">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->desktopimg_1}}">
                                </div>
                                <br/>
                                @if ($errors->has('desktopimg_2')) <p
                                        class="alert alert-danger"> {{ $errors->first('desktopimg_2') }}</p> @endif
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label for="description">General Description</label>
                                <textarea class="ckeditor" id="elm4" name="description">{!! $client->description !!}</textarea>
                            </div>
                            @if ($errors->has('description')) <p
                                    class="alert alert-danger">{{ $errors->first('description') }}</p> @endif


                            <div class="input-group{{ $errors->has('description_img') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse Description image ... <input type="file" name="description_img">
								</span>
							</span>
                                <input type="text" class="form-control" readonly="" value="{{$client->description_img}}">
                            </div>
                            <br/>
                            @if ($errors->has('description_img')) <p
                                    class="alert alert-danger">{{ $errors->first('description_img') }}</p> @endif

                            <div class="form-group">
                                <label for="custom_field_2">Description Link</label>
                                <input type="text" name="description_link" class="form-control" value="{{$client->description_link}}">
                            </div>
                            @if ($errors->has('description_link')) <p
                                    class="alert alert-danger">{{ $errors->first('description_link') }}</p> @endif

                            <p>Mobile images</p><hr>

                            <div class="col-md-4 col-sm-12">
                                <div class="input-group{{ $errors->has('mobileimg_1') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse Mobile image ... <input type="file"
                                                                                                          name="mobileimg_1">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->mobileimg_1}}">
                                </div>
                                <br/>
                                @if ($errors->has('mobileimg_1')) <p
                                        class="alert alert-danger">{{ $errors->first('mobileimg_1') }}</p> @endif
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <div class="input-group{{ $errors->has('mobileimg_2') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse Mobile image... <input type="file"
                                                                                                         name="mobileimg_2">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->mobileimg_2}}">
                                </div>
                                <br/>
                                @if ($errors->has('mobileimg_2')) <p
                                        class="alert alert-danger">{{ $errors->first('mobileimg_2') }}</p> @endif
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <div class="input-group{{ $errors->has('mobileimg_3') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse Mobile image... <input type="file"
                                                                                                         name="mobileimg_3">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="" value="{{$client->mobileimg_3}}">
                                </div>
                                <br/>
                                @if ($errors->has('mobileimg_3')) <p
                                        class="alert alert-danger">{{ $errors->first('mobileimg_3') }}</p> @endif
                            </div>

                            <div class="clearfix"></div>


                            <!-- Hidden inputs -->

                            <input type="hidden" name="user_id" value="{{ Auth::user()->id  }}">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Save
                            </button>

                            <!-- Button trigger modal -->
                            <button data-link="{{url('/')}}/admin/clients/{{$client->id}}" type="button" class="btn btn-labeled shiny btn-warning btn-large deleteSlideModal" data-toggle="modal" data-target="#deleteClient">
                                <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                            </button>

                            <a href="/admin/clients"  class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-chevron-left"></i> Back
                            </a>


                            {!! Form::close() !!}


                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteClient" tabindex="-1" role="dialog" aria-labelledby="deleteClient">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="deleteSlideLabel">Are you sure you want to delete this Client?</h4>
                </div>
                <div class="modal-body">
                    <form id="SlideDelForm" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input name="_method" value="DELETE" type="hidden">



                        <button  type="submit" class="btn btn-labeled shiny btn-warning btn-large">
                            <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                        </button>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection