@extends('layouts.app')


@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">About us</span>
                    </div>


                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{ Form::model('AboutUs', array('route' => array('admin.about.update'), 'method' => 'PATCH', 'files'=>true)) }}


                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" value="{{ $settings->title }}">
                            </div>
                            @if ($errors->has('title')) <p
                                    class="alert alert-danger">{{ $errors->first('title') }}</p> @endif


                            <div class="form-group">
                                <label for="title">Mission title</label>
                                <input  type="text" name="mission_title" class="form-control" value="{{ $settings->mission_title }}">
                            </div>
                            @if ($errors->has('mission_title')) <p
                                    class="alert alert-danger">{{ $errors->first('mission_title') }}</p> @endif

                            <div class="form-group">
                                <label for="description">Mission content</label>
                                <textarea  class="ckeditor" id="elm2" name="mission">{!! $settings->mission !!}</textarea>
                            </div>
                            @if ($errors->has('mission')) <p
                                    class="alert alert-danger">{{ $errors->first('mission') }}</p> @endif

                            <div class="form-group">
                                <label for="title">Custom filed 1</label>
                                <input  type="text" name="custom_field_1" class="form-control" value="{{ $settings->custom_field_1 }}">
                            </div>
                            @if ($errors->has('custom_field_1')) <p
                                    class="alert alert-danger">{{ $errors->first('custom_field_1') }}</p> @endif

                            <div class="form-group">
                                <label for="title">Custom filed 2</label>
                                <input  type="text" name="custom_field_2" class="form-control" value="{{ $settings->custom_field_2 }}">
                            </div>
                            @if ($errors->has('custom_field_2')) <p
                                    class="alert alert-danger">{{ $errors->first('custom_field_2') }}</p> @endif

                            <div class="form-group">
                                <label for="title">Who we are title</label>
                                <input  type="text" name="who_we_are_title" class="form-control" value="{{ $settings->who_we_are_title }}">
                            </div>
                            @if ($errors->has('who_we_are_title')) <p
                                    class="alert alert-danger">{{ $errors->first('who_we_are_title') }}</p> @endif

                            <div class="form-group">
                                <label for="description">Who we are content</label>
                                <textarea  class="ckeditor" id="elm3" name="who_we_are">{!! $settings->who_we_are !!}</textarea>
                            </div>
                            @if ($errors->has('who_we_are')) <p
                                    class="alert alert-danger">{{ $errors->first('who_we_are') }}</p> @endif

                            <div class="form-group">
                                <label for="title">Custom filed 3</label>
                                <input  type="text" name="custom_field_3" class="form-control" value="{{ $settings->custom_field_3 }}">
                            </div>
                            @if ($errors->has('custom_field_3')) <p
                                    class="alert alert-danger">{{ $errors->first('custom_field_3') }}</p> @endif

                            <div class="form-group">
                                <label for="title">Custom filed 4</label>
                                <input  type="text" name="custom_field_4" class="form-control" value="{{ $settings->custom_field_4 }}">
                            </div>
                            @if ($errors->has('custom_field_4')) <p
                                    class="alert alert-danger">{{ $errors->first('custom_field_4') }}</p> @endif

                            <div class="input-group">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> About us image... <input readonly type="file"
                                                                                                    name="about_image" value="{{ $settings->about_image }}">
								</span>
							</span>
                                <input type="text" class="form-control" value="{{ $settings->about_image }}">
                            </div>
                            <br/>
                            @if ($errors->has('about_image')) <p
                                    class="alert alert-danger">{{ $errors->first('about_image') }}</p> @endif

                        <!-- Hidden inputs -->
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Save
                            </button>

                            {!! Form::close() !!}




                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection