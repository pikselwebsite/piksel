@extends('layouts.app')


@section('content')

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <p><a class="btn btn-labeled shiny btn-warning btn-large" href="/admin/blog/create"> <i
                                class="btn-label fa fa-plus"></i>Add Blog </a></p>
            </div>


            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-themesecondary">
                        <i class="widget-icon fa fa-tags themesecondary"></i>
                        <span class="widget-caption themesecondary">Blogs</span>
                    </div><!--Widget Header-->
                    <div class="widget-body  no-padding">
                        <div class="tickets-container">

                            @if($blogs->isEmpty())
                                <p>There aren't any blogs yet please add <a href="/admin/blog/create"> new</a>.</p>
                            @else

                                <table class="table table-hover table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="20">
                                    <thead class="bordered-blueberry">
                                    <tr>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Number of posts</th>
                                        <th>Manage</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                        @foreach($blogs as $blog)
                                            <tr  valign="middle">
                                                <td>{{$blog->name}}</td>
                                                <td>{{$blog->slug}}</td>
                                                <td>{{count($posts->where('blog_id', $blog->id))}}</td>
                                                <td>
                                                    <span style="float: right;">

                                                        <a class="manageSlideBtn" href="/admin/blog/{{$blog->id}}/create/post"> Add Post</a>

                                                         <a type="button" class="manageSlideBtn" id="{{$blog->id}}" href="/admin/blog/{{$blog->id}}/posts">
                                                            See posts
                                                        </a>

                                                         <a type="button" class="manageSlideBtn" id="{{$blog->id}}" href="/admin/blog/{{$blog->id}}/edit/">
                                                            Edit
                                                        </a>

                                                        @if (count($trashed)> 0)

                                                            <a type="button" class="manageSlideBtn" id="{{$blog->id}}" href="/admin/blog/{{$blog->id}}/trash">
                                                            Trashed ({{count($trashed)}})
                                                             </a>

                                                            @endif
                                                        
                                                        </span>

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
