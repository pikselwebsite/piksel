@extends('layouts.app')


@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Add Blog</span>
                    </div>

                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{ Form::model('blog', array('route' => array('blog.store'), 'method' => 'POST', 'files'=>true)) }}

                            <div class="form-group">
                                <label for="title">Blog name</label>
                                <input type="text" name="name" class="form-control" value="{{old('title')}}">
                            </div>
                            @if ($errors->has('name')) <p
                                    class="alert alert-danger">{{ $errors->first('name') }}</p> @endif

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="ckeditor" id="elm3" name="description"></textarea>
                            </div>
                            @if ($errors->has('description')) <p
                                    class="alert alert-danger">{{ $errors->first('description') }}</p> @endif

                        <!-- Hidden inputs -->

                            <input type="hidden" name="user_id" value="{{ Auth::user()->id  }}">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Save
                            </button>


                            {!! Form::close() !!}


                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection