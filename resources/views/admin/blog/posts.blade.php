@extends('layouts.app')


@section('content')

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <p><a class="btn btn-labeled shiny btn-warning btn-large" href="/admin/blog/{{$blog->id}}/create/post"> <i
                                class="btn-label fa fa-plus"></i>Add Post </a></p>
            </div>

            <div class="col-lg-12">
                Published posts ({{count($posts)}}) |   <a href="/admin/blog/{{$blog->id}}/trash" > Trash ({{count($trashed)}}) </a>
            </div>


            <div class="col-lg-12">
                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-themesecondary">
                        <i class="widget-icon fa fa-tags themesecondary"></i>
                        <span class="widget-caption themesecondary">Posts from {{$blog->name}}</span>
                    </div><!--Widget Header-->
                    <div class="widget-body  no-padding">
                        <div class="tickets-container">

                            @if($posts->isEmpty())
                                <p>There aren't any posts yet please add <a href="/admin/blog/{{$blog->id}}/create/post"> new</a>.</p>
                            @else

                                <table class="table table-hover table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="20">
                                    <thead class="bordered-blueberry">
                                    <tr>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Manage</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @foreach($posts as $post)
                                        <tr  valign="middle">
                                            <td>{{$post->title}}</td>
                                            <td>{{$post->slug}}</td>
                                            <td>
                                                    <span style="float: right;">
                                                    <a type="button" class="manageSlideBtn" id="{{$post->id}}" href="/admin/blog/{{$blog->id}}/edit/post/{{$post->id}}">
                                                        Edit
                                                    </a>

                                                        <form class="inlineForm" action="{{url('/')}}/admin/blog/delete/post/{{$post->id}}"  method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <button  type="submit" class="manageSlideBtn">
                                                                Delete
                                                            </button>
                                                        </form>

                                                        </span>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



@endsection
