@extends('layouts.app')


@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-lg-offset-1 col-sm-offset-1 col-lg-10 col-sm-12 col-xs-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="widget">
                    <div class="widget-header bordered-bottom bordered-warning">
                        <span class="widget-caption">Edit Blog Post</span>
                    </div>


                    <div class="widget-body">
                        <div id="horizontal-form">

                            {{ Form::model('blogpost', array('route' => array('admin.blogpost.update', $post->id), 'method' => 'PATCH', 'files'=>true)) }}

                            <div class="form-group">
                                <label>Language: </label>
                                <label>
                                    <input name="language" class="form-control colored-palegreen" value="en" @if($post->language == 'en') checked="" @endif type="radio">
                                    <span class="text"> English</span>
                                </label>
                                <label>
                                    <input name="language" class="form-control colored-palegreen" @if($post->language == 'mk') checked="" @endif value="mk" type="radio">
                                    <span class="text"> Macedonian</span>
                                </label>


                            </div>

                            <div class="input-group{{ $errors->has('image') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse image... <input type="file"
                                                                                                  name="image">
								</span>
							</span>
                                <input type="text" class="form-control" readonly="" value="{{$post->image}}">
                            </div>
                            <br/>
                            @if ($errors->has('image')) <p
                                    class="alert alert-danger">{{ $errors->first('image') }}</p> @endif


                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" value="{{$post->title}}">
                            </div>
                            @if ($errors->has('title')) <p
                                    class="alert alert-danger">{{ $errors->first('title') }}</p> @endif

                            <div class="form-group">
                                <label for="description">Content</label>
                                <textarea class="ckeditor" id="elm3" name="content">{!! $post->content !!}</textarea>
                            </div>
                            @if ($errors->has('content')) <p
                                    class="alert alert-danger">{{ $errors->first('content') }}</p> @endif

                            <div class="form-group">
                                <label for="excerpt"> Excerpt</label>
                                <textarea type="text" name="excerpt" class="form-control">{!! $post->excerpt !!}</textarea>
                            </div>
                            @if ($errors->has('excerpt')) <p
                                    class="alert alert-danger">{{ $errors->first('excerpt') }}</p> @endif

                            <div class="form-group">
                                <label for="tag">Tag</label>
                                <input type="text" name="tag" class="form-control" value="{{$post->tag}}">
                            </div>
                            @if ($errors->has('tag')) <p
                                    class="alert alert-danger">{{ $errors->first('tag') }}</p> @endif

                            <div class="form-group">
                                <select name='category' >
                                    <option disabled="disabled">Select Category</option>
                                    @foreach($categories as $category)
                                        <option title='{{$category->name}}' value='{{$category->id}}'
                                                @if($category->id == $post->category)
                                                selected="selected"
                                                @endif

                                        >
                                            {{$category->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('category')) <p
                                    class="alert alert-danger">{{ $errors->first('category') }}</p> @endif


                        <!-- Hidden inputs -->

                            <input type="hidden" name="user_id" value="{{ Auth::user()->id  }}">
                            <input type="hidden" name="blog_id" value="{{$blog->id}}">

                            <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                        class="btn-label fa fa-plus"></i> Save
                            </button>

                            <!-- Button trigger modal -->
                            <button data-link="{{url('/')}}/admin/blog/delete/post/{{$post->id}}" type="button" class="btn btn-labeled shiny btn-warning btn-large deleteSlideModal" data-toggle="modal" data-target="#deleteSlide">
                                <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                            </button>

                            <a href="/admin/blog/{{$blog->id}}/posts" class="btn btn-labeled shiny btn-warning btn-large">
                                <i class="btn-label fa fa-chevron-left" aria-hidden="true"></i> Back
                            </a>

                            {!! Form::close() !!}


                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="deleteSlide" tabindex="-1" role="dialog" aria-labelledby="deleteSlide">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="deleteSlideLabel">Are you sure you want to delete this post?</h4>
                </div>
                <div class="modal-body">
                    <form id="SlideDelForm" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">



                        <button  type="submit" class="btn btn-labeled shiny btn-warning btn-large">
                            <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                        </button>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection