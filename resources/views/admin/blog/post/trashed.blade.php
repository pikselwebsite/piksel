@extends('layouts.app')


@section('content')

    <div class="page-body">
        <div class="row">
            <div class="col-lg-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                    <div class="col-lg-12">

                        @if(count($trashed)>0)
                        <p>
                            <form action="{{url('/')}}/admin/blog/empty/trash"  method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button  type="submit" class="btn  shiny btn-warning btn-large">
                                    Empty Trash
                                </button>
                            </form>

                        </p>
                        @endif
                        <a href="/admin/blog/{{$blog->id}}/posts">Published posts ({{count($posts)}})</a> |  Trash ({{count($trashed)}})
                    </div>
                    <div class="col-lg-12">
                        <div class="widget">
                            <div class="widget-header bordered-bottom bordered-themesecondary">

                                <i class="widget-icon fa fa-trash" aria-hidden="true"></i>

                                <span class="widget-caption themesecondary">Trashed items from {{$blog->name}}</span>
                            </div><!--Widget Header-->
                            <div class="widget-body  no-padding">
                                <div class="tickets-container">

                                    @if($trashed->isEmpty())
                                        <p>There aren't any posts in trash.</p>
                                    @else

                                        <table class="table table-hover table-striped table-bordered" width="100%" border="0" cellspacing="0" cellpadding="20">
                                            <thead class="bordered-blueberry">
                                            <tr>
                                                <th>Name</th>
                                                <th>Slug</th>
                                                <th>Manage</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            @foreach($trashed as $post)
                                                <tr  valign="middle">
                                                    <td>{{$post->title}}</td>
                                                    <td>{{$post->slug}}</td>
                                                    <td>
                                                    <span style="float: right;">

                                                        <form class="inlineForm" action="{{url('/')}}/admin/blog/restore/post/{{$post->id}}"  method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <button  type="submit" class="manageSlideBtn">
                                                                Restore Item
                                                            </button>
                                                        </form> |



                                                        <!-- Button trigger modal -->
                                    <button style="float: right; display: inline-block;" data-link="{{url('/')}}/admin/blog/permanent/post/{{$post->id}}" type="button" class="manageSlideBtn deleteSlideModal" data-toggle="modal" data-target="#deleteSlide">
                                       Delete
                                    </button>

                                                        </span>

                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif


                                </div>

                            </div>
                        </div>
                    </div>
        </div>
    </div>


        <!-- Modal -->
        <div class="modal fade" id="deleteSlide" tabindex="-1" role="dialog" aria-labelledby="deleteSlide">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="deleteSlideLabel">Are you sure you want to delete this post?</h4>
                    </div>
                    <div class="modal-body">
                        <form id="SlideDelForm" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">



                            <button  type="submit" class="btn btn-labeled shiny btn-warning btn-large">
                                <span class="btn-label g glyphicon glyphicon-trash" aria-hidden="true"></span>Permanent Delete
                            </button>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


@endsection

