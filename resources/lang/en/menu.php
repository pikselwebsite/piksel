<?php

return [
    'about' => 'ABOUT',
    'portfolio' => 'PORTFOLIO',
    'blog' => 'BLOG',
    'contact'=> 'CONTACT',
    'career' => 'CAREER'
];