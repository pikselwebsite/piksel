<?php
return[
    'client_title'=>'CLIENT',
    'client'=>'Bimilk',
    'what_title'=>'WHAT WE DID',
    'what'=>'Social Media, Digital Advertising, Web Maintenance, Data Analysis, Digital Strategy, Content Production',
    'about_title'=>'ABOUT',
    'about'=>'With more then 65 years of experience, IMB Dairy Bitola is the biggest producer of milk, dairy products and natural fruit juices in Macedonia.',
    'social_title'=>'SOCIAL MEDIA',
    'social'=>'We create and maintain the social media presence of Bimilk on daily basis while constantly looking for new ways and technologies to implement in our work.',
    'bi_title'=>'B.I',
    'bi'=>'We are always ready to monitor, listen and respond to our brand`s fans',
];