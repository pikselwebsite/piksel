<?php
return[
    'about_title'=>'About us',
    'mision_title'=>'MISSION ',
    'mision_title2'=>'AND VISION',
    'mision_des'=>'Our mission is to become a leading digital marketing agency as a one-stop-shop for all our of client’s needs, from developing and delivering innovative design, state-of-the-art web sites and creating a unique digital marketing presence. We are a customer-centric company, and our client’s digital marketing always come first. ',
    'mision_fot_1'=>'CREATIVITY IS A',
    'mision_fot_2'=>' MUST!',
    'mision_fot_3'=>'CLIENTS COME FIRST',
    'who_1'=>'WHO',
    'who_2'=>'WE ARE',
     'who_des'=>'Piksel is team of renowned experts dedicated to helping you create innovative websites and providing you with a full set of digital marketing and creative services. With a sound reputation of being experienced, reliable, methodical and passionate we have become a trusted partner of some of the largest companies in Macedonia and abroad. Why Piksel? Because it`s the smallest element of an image, but an image is worth a thousand words and Pi means an infinity of ideas. It`s not how our name is spelled, it`s all about the experience!',
    'who_fot_1'=>'PIKSEL',
    'who_fot_2'=>'MAKING A DIFFERENCE IN THE WORLD OF DIGITAL',
];