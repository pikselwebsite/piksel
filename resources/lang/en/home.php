<?php

return [
    'about' => 'About',
    'about_des' => 'Your one-stop-shop digital marketing agency for discovering, developing and delivering innovative design, state-of-the-art web sites and creating a unique digital presence across all channels.',
    'services' => 'Services',
    'services_web_title' =>'WEB AND SOFTWARE DEVELOPMENT',
    'services_web'=> "We're using the latest in web and software technologies to develop and deploy anything from a small website to custom-made large websites and online software systems",
    'services_dig_title' =>'DIGITAL MАRKETING AND ADVERTISING',
    'services_dig' => 'Upon doing a thorough analysis of your competition and your current digital marketing efforts, we create a custom made digital marketing strategy which we creatively implement across all channels accompanied by smart advertising optimization ',
    'services_des_title' =>'GRAPHIC AND WEB DESIGN',
    'services_des' => 'The design process is essential to creating a strong brand identity, and we try to understand our client’s needs and implement a unique look and feel on traditional and online channels. UX/UI is our main focus when we create your website.',
    'services_pho_title' =>'PHOTOGRAPHY AND VIDEO',
    'services_pho' => 'Photography and video production is part of the process of creating a unique online presence for our clients in their digital marketing endeavors',
    'clients' => 'Clients',
    'portf_button' => 'SEE PORTFOLIO',
    'contact'=>'Contact',
    'contact_moto' =>'WE ARE HERE FOR YOU',
     'contact_button'=>'SEND',
    'contact_takeme' =>'TAKE ME TO PIKSEL',
    'holder_name'=>'Name',
    'holder_mail'=>'E-Mail',
    'holder_message'=>'How can we help you?',
];