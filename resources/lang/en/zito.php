<?php
return[
    'title'=>'Zito Marketi',
    'client_title'=>'CLIENT',
    'client'=>'Zito Marketi',
    'what_title'=>'WHAT WE DID',
    'what'=>'Graphic Design, Social Media, Digital Advertising, Web Design and Development, Data Analysis, Digital Strategy, Content Production',
    'about_title'=>'ABOUT',
    'about'=>'Established in 1988 in Veles, Zito Marketi in the last three decades has grown in one of the biggest domestic market chains in Macedonia.',
    'social_title'=>'SOCIAL MEDIA AND GRAPHIS SOLUTIONS',
    'social'=>'We create the full circle online and offline presence for one of the fastest growing supermarket chains in Macedonia. ',
    'print_title'=>'PRINT MATERIALS',
    'print'=>'We gave a completely new, fresh and modern look to all printed materials associated with the brand. From the smallest leaflets and posters to the big catalogues, banners and billboards, the new design, with the new logo, is present in all the prints.',
];