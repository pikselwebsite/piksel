<?php
return[
    'contact_title'=>'Contact',
    'contact_line'=>'Drop us a line',
    'contact_name'=>'Name',
    'contact_email'=>'E-Mail',
    'contact_tel'=>'Telephone',
    'contact_com'=>'Company',
    'contact_web'=>'Website',
    'contact_mes'=>'Message',
    'contact_add'=>'ADDRESS',
    'contact_add_des'=>'Kosturski heroi 2/2 Skopje R.Macedonia',
    'contact_mail'=>'EMAIL',
    'contact_mail_des'=>'contact@piksel.mk',
    'contact_tele'=>'PHONE',
    'contact_tel_des'=>'+389 071 253 002',
];