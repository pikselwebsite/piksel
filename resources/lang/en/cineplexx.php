<?php
return[
    'client_title'=>'CLIENT',
    'client'=>'Cineplexx',
    'what_title'=>'WHAT WE DID',
    'what'=>'Social Media, Digital Advertising, Data Analysis, Digital Strategy, Content Production',
    'about_title'=>'ABOUT',
    'about'=>'Cineplexx is a cinema company based in Austria. The company was founded in 1993, and operates mainly multiplex type cinemas. In Macedonia Cineplexx is present from year 2012.',
    'social_title'=>'SOCIAL MEDIA',
    'social'=>'Facebook, Instagram, Twitter and Digital Campaigns, that’s just part of the work we do for the famous Cineplexx cinema chain.',
];