<?php
return[
    'title'=>'Career',
    'paid'=>'Get paid for being awesome!',
    'des'=>'GRAPHIC DESIGNER',
    'php'=>'PHP DEVELOPER',
    'front'=>'FRONT-END DEVELOPER',
    'soc'=>'SOCIAL MEDIA NINJA',
    'pos'=>'DON`T SEE A POSITION THAT FITS?',
    'people'=>'<h4>We`re always looking for talented people</h4>',
    'touch'=>'GET IN TOUCH',
    // design
    'tekst_des'=>'<p>Piksel is a fast growing company with 4 years of experience in the digital marketing world.</p><p style="margin-bottom:30px">We focus on web development and digital marketing while creating successful working teams, sharing knowledge, and promoting team spirit. Thus, we strive to give our team members the best possible grounds for personal and professional growth.</p>',
    'ul_title'=>'<p>Requirements</p>',
    'li_des'=>'<li>Knowledge of PHP + PHP Frameworks (Laravel, Codeigniter)</li><li>High knowledge in Javascript</li><li>Mid to top level knowledge of HTML5, CSS3, JQuery and MySQL</li><li>Knowledge in data bases and structure</li><li>Knowledge of Git, Wordpress and Drupal will be an advantage</li>',
    //php
    'ul_title'=>'<p>Requirements</p>',
    'li_php'=>'<li>Knowledge of PHP + PHP Frameworks (Laravel, Codeigniter)</li><li>Knowledge of Javascript</li><li>Mid to top level knowledge of HTML5, CSS3, JQuery and MySQL</li><li>Knowledge in data bases and structure</li><li>Knowledge of Git, Wordpress and Drupal will be considered an advantage</li>',
    //front
    'ul_title'=>'<p>Requirements</p>',
    'li_front'=>'<li>Knowledge of Javascript</li><li>Knowledge of HTML5, CSS3, JQuery</li><li>Mid level knowledge of PHP </li><li>Knowledge in data bases and structure</li><li>Knowledge of Git, Wordpress and Drupal will be considered an advantage</li>',
    //soc
    'ul_title'=>'<p>Requirements</p>',
    'ul_title2'=>'Education and experience:',
    'li_soc2'=>'<li>1-2 year experience in marketing</li><li>Degree in marketing, communications or similar field
Requirements</li>',
    'li_soc'=>'<li>Development and implementation of digital marketing strategies</li><li>Account management</li><li>Creating and following promotions, campaigns, and other forms of online advertising</li><li>Content marketing</li><li>Strong communicational skills</li><li>Communication with clients</li><li>Reporting and analysis</li><li>Team player</li><li>Knowledge of design (PS and AI) will be considered an advantage</li>',

    //
    'career_name'=>'Name & Surname',
    'career_tele'=>'Telephone',
    'career_mail'=>'E-Mail',
    'career_mes'=>'Message',
];