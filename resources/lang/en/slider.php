<?php

return [
    'client' => 'CLIENT',
    'whatWeDo' => 'WHAT WE DO',
    'whatWeDid' => 'WHAT WE DID',
    'yourGuide' => 'Your guide',
    'throughThe'=> 'through the',
    'digitalWorld' => 'digital world!',
];