<?php
return[
    'client_title'=>'CLIENT',
    'client'=>'Balans',
    'what_title'=>'WHAT WE DO',
    'what'=>'Graphic Design, Social Media, Digital Advertising, Web Design and Development, Data Analysis, Digital Strategy, Content Production',
    'about_title'=>'ABOUT',
    'social_title'=>'SOCIAL MEDIA',
    'social'=>'The unique character of Balans+ is present in the unique communication on the Balans+ Facebook page and all the others Social Media channels.',
    'responsive'=>'RESPONSIVE WEBSITE',
    'responsive_text'=>'Part of the work we did for Balans+ was the new responsive website which fresh and clean design that corresponds with the product itself.',
    'about'=>'Balans+ is the first fermented probiotic dairy product on the Macedonian market and ideal source of minerals and vitamins for a healthy lifestyle.',
    'jademe_title'=>'BALANS+ & JADEME.MK',
    'jademe'=>'One of the collaborations that we are proud of is the one with Jademe.mk, website specialized in food and recipes. Campaign for which we developed a new Facebook application in which the fans were able to create and vote for their favorite daily food combinations.',
];