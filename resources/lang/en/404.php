<?php

return[
    'siteNotFound'=>'It\'s looking like you may have taken a wrong turn.
Don\'t worry... It happens to the best of us.',
    'error' => 'Error 404',
    'title' => 'Page Not Found'

];