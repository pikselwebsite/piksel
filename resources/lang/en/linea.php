<?php
return[
      'title'=>'Linea',
    'client_title'=>'CLIENT',
    'client'=>'Linea',
    'what_title'=>'WHAT WE DO',
    'what'=>'Web Design, Web Development',
    'about_title'=>'ABOUT',
    'about'=>'Founded in 1999 in Skopje, LINEA focuses on projecting and construction of commercial and residential objects in the city center, while using the best available technology and materials.',
    'icons'=>'CUSTOM MADE ICONS',
    'linea-tekst'=>'We redesigned the LINEA website implementing a bunch of new features. They include custom made icons, clickable online projects in full and divided by floors and apartments. Also, we implemented  downloadable PDF of every single building, floor and apartment. ',
    'responsive'=>'RESPONSIVE WEBSITE',
    'case'=>'Custom made icons of apartment plans',
    'case_txt'=>'The icon of the plan for each apartment was created separately so the users can easily shift through floors and desired sizes of apartments while choosing the one that fits them the best.',
];