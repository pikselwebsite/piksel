<?php 
return[
    'prev'=>'Back to portfolio',
    'next'=>'Next project',
    'title'=>'Portfolio',
    'filter1'=>'ALL',
    'filter2'=>'WEB DVELOPMENT',
    'filter3'=>'DIGITAL MARKETING',
    'filter4'=>'GRAPHIC DESIGN',
    'client'=>'CLIENT',
    'what'=>"WHAT WE DO",
    // clients now

    //client1
    'client1'=>'Bimilk',
    'client1_what'=>'Social Media, Digital Advertising, Web Maintenance, Data Analysis, Digital Strategy, Content Production',
     
     //client2
    'client2'=>'Cineplex',
    'client2_what'=>'Social Media, Digital Advertising, Data Analysis, Digital Strategy, Content Production',

    //client3
    'client3'=>'Zito Marketi',
    'client3_what'=>'Graphic Design, Social Media, Digital Advertising, Web Design and Development, Data Analysis, Digital Strategy, Content Production',

    //client4
    'client4'=>'Balans',
    'client4_what'=>'Social Media, Digital Advertising, Web Maintenance, Data Analysis, Digital Strategy, Content Production, Web Development',

    //client5
    'client5'=>'Linea',
    'client5_what'=>'Web Design, Web Development',

    //client6
    'client6'=>'Forza',
    'client6_what'=>'Social Media, Digital Advertising, Web Design, Data Analysis, Digital Strategy, Content Production',

     //client7
    'client7'=>'Papilon',
    'client7_what'=>'Social Media, Digital Advertising, Web Design and Development, Data Analysis, Digital Strategy, Content Production',

     //client8
    'client8'=>'Alexander Bach',
    'client8_what'=>'Social Media, Digital Advertising, Web Maintenance, Data Analysis, Web Development, Web Design',

     //client9
    'client9'=>'Etrusca',
    'client9_what'=>'Web Design, Web Development',

    //client10
    'client10'=>'Trend',
    'client10_what'=>'Social Media, Digital Advertising, Web Design and Development, Data Analysis, Digital Strategy',

    //client11
    'client11'=>'Exide',
    'client11_what'=>'Social Media, Digital Advertising, Data Analysis, Content Production',
    'client11_des'=>'We created a brand-new image on Facebook and Instagram, managed GDN campaigns and increased sales.',

    //client12
    'client12'=>'DelCo',
    'client12_what'=>'Web Design and Development, Social Media, Digital Advertising',

    //client13
    'client13'=>'Furna Dime',
    'client13_what'=>'Web Design and Development, Social Media, Digital Advertising',

    //client14
    'client14'=>'Gojo Dojo',
    'client14_what'=>'Social Media, Logo Design, Digital Advertising, Content Production',
    'client14_des'=>'Created a new logo, positioned the brand as a leader in its market segment, and gave a significant boost in sales',

    //client15
    'client15'=>'Biblioteka',
    'client15_what'=>'Social Media, Digital Advertising, Content Production',
    'client15_des'=>'Created a unique presence on Facebook, and became recognizable online for the constant weekly promotions going on for 4 years now',

    //client16
    'client16'=>'Akademik',
    'client16_what'=>'Web Design and Development, Data Analysis',

    //client17
    'client17'=>'Telma',
    'client17_what'=>'Web Design, Development and Maintenance',
    'client17_des'=>'Switched the old platform of the site to a new state-of-the-art solution, completely redesigned the site and we constantly upgrade it according to the newest trends in the TV industry',

    //client18
    'client18'=>'Monin Macedonia',
    'client18_what'=>'Social Media, Digital Advertising, Content Production',
    'client18_des'=>'Established the local page of the world-famous brand according to the brand guidelines but suited for the Macedonian market',

     //client19
    'client19'=>'Portioli',
    'client19_what'=>'Social Media, Digital Advertising',
    'client19_des'=>'We created a brand-new image on Facebook and Instagram',

     //client20
    'client20'=>'Samantha',
    'client20_what'=>'Web Design, Development and Maintenance',
    'client20_des'=>'Switched the old platform of the site to a new state-of-the-art solution, completely redesigned the site',

     //client21
    'client21'=>'Chilin',
    'client21_what'=>'Social Media, Digital Advertising',
    'client21_des'=>'We created a brand-new image on Facebook and Instagram',

     //client22
    'client22'=>'Kimbo',
    'client22_what'=>'Social Media, Digital Advertising, Content Production',
    'client22_des'=>'We created a brand-new image on Facebook and Instagram',

     //client23
    'client23'=>'Savana',
    'client23_what'=>'Social Media, Digital Advertising, Content Production, Digital marketing campaign & strategy',
    'client23_des'=>'We created a brand-new image on Facebook and Instagram',

     //client24
    'client24'=>'BIM',
    'client24_what'=>'Web Design, Development and Maintenance',
    'client24_des'=>'Switched the old platform of the site to a new state-of-the-art solution, completely redesigned the site',

     //client25
    'client25'=>'UNIXX',
    'client25_what'=>'Web Design, Development and Maintenance',
    'client25_des'=>'Switched the old platform of the site to a new state-of-the-art solution, completely redesigned the site',



];