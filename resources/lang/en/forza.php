<?php
return[
    'title'=>'Forza',
    'client_title'=>'CLIENT',
    'client'=>'Forza',
    'what_title'=>'WHAT WE DID',
    'what'=>'Social Media, Digital Advertising, Data Analysis, Digital Strategy, Content Production',
    'about_title'=>'ABOUT',
    'about'=>'Forza is a financial institution specialized in fast loans that are ideal for fast financial solutions. In Macedonia the brand is present from the fall of 2016.',
    'media'=>'Media Campaign and Web Banners',
    'social_title'=>'SOCIAL MEDIA',
    'social'=>'In the fall of 2016, in collaboration with the team of Digital Finance International, we created the full online brand identity of Forza.mk. Big part of that was creating new social media profiles from scratch and implementing the new communicational and advertising strategy.',
    'priracnik_naslov'=>'<a target ="_blank"href="https://forza.mk/priracnik.pdf">WHITE PAPER</a>',
    'priracnik'=>'Part of the work for Forza was the design of the White Paper in which the whole process was explained in details. ',
];