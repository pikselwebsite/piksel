<?php

return [
    'about' => 'about',
    'portfolio' => 'portfolio',
    'blog' => 'blog',
    'post' => 'blogpost/{slug}',
    'contact'=> 'contact',
    'career' => 'career',
    'bimilk' =>  'clients/bimilk',
    'linea' =>  'clients/linea',
    'zito' =>  'clients/zito',
    'cineplexx' =>  'clients/cineplexx',
    'balans' =>  'clients/balans',
    'forza' =>  'clients/forza',
    'notFound' =>  'site-not-found',
];