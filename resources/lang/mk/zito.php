<?php
return[
      'title'=>'Жито Mаркети',
    'client_title'=>'КЛИЕНТ',
    'client'=>'Жито маркети',
    'what_title'=>'ШTO ПРАВИМЕ ЗА КЛИЕНТОТ',
    'what'=>'Графички дизајн, Социјални медиуми, Дигитален Маркетинг, Веб дизајн и одржување, Анализа на податоци, Дигитална стратегија, Контекстуален маркетинг',
    'about_title'=>'ЗA КЛИЕНТОТ',
    'about'=>'Од отворањето на првиот С.Т.Д. во Велес, во далечната 1988 година, па се’ до денес Жито маркети стигнаа до бројката од над 50 супермаркети распределени на територијата на цела Македонија.',
    'social_title'=>'Социјални медиуми',
    'social'=>'Пиксел - место каде се спојуваат онлајн и офлајн присуството на пазарот на еден од најбрзо растечките маркети во Македонија',
    'print_title'=>'Материјали за печатење',
    'print'=>'Направивме комплетно нов, свеж и модерен дизајн на сите материјали за печатење поврзани со брендот. Од постери и брошури, до каталози, банери и билборди. Новиот дизајн со новото лого е имплементиран во сите материјали.',
];