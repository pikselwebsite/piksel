<?php

return [
    'about' => 'za-nas',
    'portfolio' => 'portfolio',
    'blog' => 'blog',
    'post' => 'blogpost/{slug}',
    'contact'=> 'kontakt',
    'career' => 'kariera',
    'bimilk' =>  'klienti/bimilk',
    'linea' =>  'klienti/linea',
    'zito' =>  'klienti/zito',
    'cineplexx' =>  'klienti/cineplexx',
    'balans' =>  'klienti/balans',
    'forza' =>  'klienti/forza',
];