<?php
return[
    'client_title'=>'КЛИЕНТ',
    'client'=>'Balans+',
    'what_title'=>'ШТО ПРАВИМЕ ЗА КЛИЕНТОТ',
    'what'=>'Графички дизајн, социјални медиуми, дигитален маркетинг, веб дизајн и одржување, анализа на податоци, дигитална стратегија, контекстуален маркетинг',
    'about_title'=>'ЗA КЛИЕНТОТ',
    'social_title'=>'Социјални медиуми',
    'social'=>'Посебноста на самиот производ е пресликана и со уникатна комуникација на Баланс+ на Фејсбук страната, како и на останатите социјални мрежи.',
    'responsive'=>'Респонзивна веб страница',
    'responsive_text'=>'Дел од работата за Balans+, беше и нова респонзивна веб страница, каде што новиот и чист дизајн, кореспондира со самиот производ.',
    'about'=>'Balans+ е првиот пробиотски ферментиран производ на Македонскиот пазар. Овој пробиотски јогурт е одличен функционален и хранлив производ што на вкусен начин Ви помага во динамичното секојдневие.',
    'jademe_title'=>'Balans+ & Јадеме.мк',
    'jademe'=>'Една од соработките на која сме горди е онаа со Јадеме.мк, портал специјализиран за храна и рецепти. За оваа соработка изработивме апликација на Фејсбук, во која обожавателите можеа да изберат и направат за нив најдобрата комбинација на оброци (појадок, ручек и вечера), кои потоа се гласаа.',
];