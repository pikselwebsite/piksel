<?php
return[
    'contact_title'=>'Контакт',
    'contact_line'=>'Тука сме за вас',
    'contact_name'=>'Име',
    'contact_email'=>'Е-Пошта',
    'contact_tel'=>'Телефон',
    'contact_com'=>'Компанија',
    'contact_web'=>'Вебстрана',
    'contact_mes'=>'Текст',
    'contact_add'=>'АДРЕСА',
    'contact_add_des'=>'Костурски херои 2/2 Скопје',
    'contact_mail'=>'ЕМАИЛ',
    'contact_mail_des'=>'contact@piksel.mk',
    'contact_tele'=>'ТЕЛЕФОН',
    'contact_tel_des'=>'071 253 002',
];